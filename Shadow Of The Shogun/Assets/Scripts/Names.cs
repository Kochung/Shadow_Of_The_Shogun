﻿using UnityEngine;
using System.Collections;

public class Names
{
    private static string[] list = {"AKI" 
                                    ,"OROCHI"
                                    ,"AKIHIKO" 
                                    ,"OSAMU"
                                    ,"AKIHIRO" 
                                    ,"RAIDEN"
                                    ,"AKIO"
                                    ,"ROKURO"
                                    ,"AKIRA"
                                    ,"RYO"
                                    ,"AOI" 
                                    ,"RYOTA"
                                    ,"ARATA"
                                    ,"RYUU"
                                    ,"ATSUSHI"
                                    ,"SABURO" 
                                    ,"DAI"
                                    ,"SADAO"
                                    ,"DAICHI"
                                    ,"SATORU" 
                                    ,"DAIKI"
                                    ,"SATOSHI" 
                                    ,"DAISUKE"
                                    ,"SEIICHI" 
                                    ,"EIJI"
                                    ,"SEIJI" 
                                    ,"FUMIO"
                                    ,"SHICHIRO" 
                                    ,"GORO"
                                    ,"SHIG"
                                    ,"GOROU"
                                    ,"SHIGEO" 
                                    ,"HACHIRO"
                                    ,"SHIGERU" 
                                    ,"HAJIME"
                                    ,"SHIN" 
                                    ,"HARU"
                                    ,"SHIN'ICHI" 
                                    ,"HARUO"
                                    ,"SHINJI" 
                                    ,"HIDEAKI"
                                    ,"SHIRO" 
                                    ,"HIDEKI"
                                    ,"SHO" 
                                    ,"HIDEO"
                                    ,"SHOICHI" 
                                    ,"HIKARU"
                                    ,"SHOJI" 
                                    ,"HIRO"
                                    ,"SHOU" 
                                    ,"HIROAKI"
                                    ,"SHUICHI" 
                                    ,"HIROKI"
                                    ,"SHUJI" 
                                    ,"HIROSHI"
                                    ,"SORA" 
                                    ,"HIROYUKI"
                                    ,"SUSUMU" 
                                    ,"HISAO"
                                    ,"TADAO" 
                                    ,"HISASHI"
                                    ,"TADASHI" 
                                    ,"HISOKA"
                                    ,"TAKAHIRO" 
                                    ,"HITOSHI"
                                    ,"TAKAO" 
                                    ,"HOTAKA"
                                    ,"TAKAYUKI" 
                                    ,"ICHIRO"
                                    ,"TAKEHIKO" 
                                    ,"ICHIROU"
                                    ,"TAKEO" 
                                    ,"TAKESHI"
                                    ,"TAKUMI" 
                                    ,"ISAMU"
                                    ,"YOSHIKAZU" 
                                    ,"ISAO"
                                    ,"YOSHIHIRO" 
                                    ,"IWAO"
                                    ,"YOSHIO" 
                                    ,"IZANAGI"
                                    ,"YOSHIRO" 
                                    ,"JIRO"
                                    ,"YOSHITO" 
                                    ,"JIROU"
                                    ,"YOSHIYUKI" 
                                    ,"JUN"
                                    ,"YUICHI" 
                                    ,"JUNICHI"
                                    ,"YUJI" 
                                    ,"JURO"
                                    ,"YUU" 
                                    ,"JUROU"
                                    ,"YUUDAI" 
                                    ,"KAEDE"
                                    ,"KATSU" 
                                    ,"KATSUMI" 
                                    ,"KATSUO" 
                                    ,"KATSURO"
                                    ,"KAZUHIKO" 
                                    ,"KAZUHIRO" 
                                    ,"KAZUKI" 
                                    ,"KAZUO"
                                    ,"KEI" 
                                    ,"KEIICHI" 
                                    ,"KEIJI" 
                                    ,"KEN"  
                                    ,"KEN'ICHI" 
                                    ,"KENJI"
                                    ,"KENSHIN" 
                                    ,"KENTA"  
                                    ,"KICHIRO"
                                    ,"KICHIROU"
                                    ,"KIN"  
                                    ,"KOICHI" 
                                    ,"KOJI" 
                                    ,"KUNIO" 
                                    ,"KURO"
                                    ,"KUROU" 
                                    ,"KYO" 
                                    ,"KYOU" 
                                    ,"MADOKA" 
                                    ,"MAKOTO" 
                                    ,"MAMORU" 
                                    ,"MANABU" 
                                    ,"MASA" 
                                    ,"MASAAKI" 
                                    ,"MASAHIKO" 
                                    ,"MASAHIRO" 
                                    ,"MASAKI" 
                                    ,"MASANORI" 
                                    ,"MASAO" 
                                    ,"MASARU"
                                    ,"MASASHI" 
                                    ,"MASATO" 
                                    ,"MASAYOSHI" 
                                    ,"MASAYUKI" 
                                    ,"MASUMI"
                                    ,"MICHI" 
                                    ,"MICHIO"  
                                    ,"MIKIO"
                                    ,"MINORI"
                                    ,"MITSUO"
                                    ,"MITSURU"
                                    ,"NAO"
                                    ,"NAOKI"
                                    ,"NOBORU" 
                                    ,"NOBU"
                                    ,"NOBUO"
                                    ,"NOBUYUKI" 
                                    ,"NORIO" 
    };
    public Names()
    {
    }
    public string getRandomName()
    {
        int length = list.Length;
        int num = BattleManager.instance.random.Next(0, length);
        string returnString = ""+list[num][0];
        returnString += list[num].Substring(1).ToLower();
        return returnString;
    }
}
