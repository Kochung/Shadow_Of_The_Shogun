﻿using UnityEngine;
using System.Collections;

public class EndingController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (InputHandler.instance.AButton || InputHandler.instance.StartButton)
        {
            MapManager.instance.clearMap();
            GameManager.instance.KillSave();
            GameManager.instance.Load("MainMenu", "Main");
        }
	}
}
