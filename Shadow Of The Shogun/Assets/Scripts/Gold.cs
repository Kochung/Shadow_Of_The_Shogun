﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Gold
{
    public int amount;
    private Text UINumber;
    public Gold()
    {
        amount = 200;
        findText();
        updateNumber();
    }
    public void findText()
    {
        if (GameObject.Find("Gold") != null)
            UINumber = GameObject.Find("Gold").transform.GetChild(0).GetComponent<Text>();
    }
    public void updateNumber()
    {
        if(UINumber != null)
            UINumber.text = "       : " + amount;
    }
    public bool check(int Amount)
    {
        return amount >= Amount;
    }
    public void add(int Amount)
    {
        amount += Amount;
        if (amount < 0)
            amount = 0;
        updateNumber();
    }
}
