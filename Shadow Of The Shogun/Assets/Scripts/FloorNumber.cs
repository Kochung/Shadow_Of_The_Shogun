﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FloorNumber : MonoBehaviour
{
	void Start ()
    {
        transform.GetChild(0).GetComponent<Text>().text = "Floor: " + BattleManager.instance.floorNumber;
    }
	
}
