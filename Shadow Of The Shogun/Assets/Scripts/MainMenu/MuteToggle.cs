﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MuteToggle : MonoBehaviour
{
    public Toggle toggle;
    public Slider slider;

    void Start()
    {
        // register listener once
        toggle.onValueChanged.AddListener(ChangeValue);

        // weird NullValueReference bug?
        if (SoundManager.instance == null) return;

        // get value from game settings
        if (toggle.name.Contains("Music"))
            toggle.isOn = SoundManager.instance.muteMusic;
        else if (toggle.name.Contains("Sound"))
            toggle.isOn = SoundManager.instance.muteSfx;

        // update slider & slider value status
        slider.enabled = !toggle.isOn;
        slider.GetComponent<SliderValue>().enabled = !toggle.isOn;
    }

    void ChangeValue(bool check)
    {
        // disable slider
        slider.enabled = !check;

        // fade text via disbling SliderValue
        slider.GetComponent<SliderValue>().enabled = !check;

        // update game settings
        if (toggle.name.Contains("Music"))
            SoundManager.instance.ToggleMusic(check);
        else if (toggle.name.Contains("Sound"))
            SoundManager.instance.ToggleSfx(check);
    }
}
