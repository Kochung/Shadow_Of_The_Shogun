﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InGameMenu : MonoBehaviour
{
    public GameObject pausePanel;
    public GameObject optionPanel;
    public GameObject confirmationPanel;

    private Canvas ui;
    private bool Active;
    private bool StopedTime;
    private int startTime;

    void Awake()
    {
        ui = GetComponent<Canvas>();
    }

    void Start()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(false);
        ui.enabled = false;
        startTime = -1;
    }

    void Update()
    {
        if (InputHandler.instance.SelectButton)
        {
            if (Active)
                Unpause();
            else
                Pause();
        }
        if (startTime == 0)
        {
            startTime = -1;
            Time.timeScale = 1;
        }
        else if (startTime > 0)
            startTime--;
    }

    void OnEnable()
    {
        ToPauseMenu();
    }

    void Pause()
    {
        StopedTime = Time.timeScale != 0;
        Time.timeScale = 0;
        Active = true;
        ui.enabled = true;
        ToPauseMenu();
 //     GameManager.instance.Pause();
    }

    void Unpause()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(false);
        ui.enabled = false;
        Active = false;
        if (StopedTime)
        {
            startTime = 10;
        }
      //  GameManager.instance.Unpause();
    }

    void ToMainMenu()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(true);
 //      Unpause();
 //      GameManager.instance.reset();
 //      GameManager.instance.Load("MainMenu", "Main");
    }
    void LoadMain()
    {
        Unpause();
        try
        {
            MapManager.instance.clearMap();
            GameManager.instance.KillSave();
        }
        catch
        {

        }
        GameManager.instance.Load("MainMenu", "Main");
    }
    void ToPauseMenu()
    {
        pausePanel.SetActive(true);
        optionPanel.SetActive(false);
        confirmationPanel.SetActive(false);
    }

    void ToOptionMenu()
    {
        pausePanel.SetActive(false);
        optionPanel.SetActive(true);
        confirmationPanel.SetActive(false);
    }
}
