﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectableControl : MonoBehaviour, IPointerEnterHandler, ISelectHandler, IDeselectHandler, IPointerExitHandler
{
    public Color baseSelectedColor = Color.white;
    public int selectSound = 1;

    private Selectable selectable;
    private Color normalButtonImageColor;
    private KeyboardUIControl kbControl;

    void Awake()
    {
        selectable = GetComponent<Selectable>();
        normalButtonImageColor = selectable.image.color;
        kbControl = transform.parent.GetComponent<KeyboardUIControl>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        selectable.image.color = baseSelectedColor;
        kbControl.Deselect();
        SoundManager.instance.PlaySingle("click");
    }

    public void OnSelect(BaseEventData eventData)
    {
        selectable.image.color = baseSelectedColor;
        //        if (kbControl.hasSelected)
        SoundManager.instance.PlaySingle("click");
    }

    public void OnDeselect(BaseEventData eventData)
    {
        selectable.image.color = normalButtonImageColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        selectable.image.color = normalButtonImageColor;
    }
}
