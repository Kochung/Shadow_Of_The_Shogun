﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class JLTS : MonoBehaviour
{
    private bool jlts = true;

    private Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
    }
    
    void FadeOut()
    {
        anim.SetTrigger("fadeOut");
    }

    public void FadeOutAfter(float seconds)
    {
        Invoke("FadeOut", seconds);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        SoundManager.instance.PlayMusic("Main");
    }

    public void WaitUntilEscape()
    {
        StartCoroutine(IWaitUntilEscape());
    }

    public IEnumerator IWaitUntilEscape()
    {
        while (jlts)
        {
            if (Input.anyKeyDown)
                break;
            yield return Time.fixedDeltaTime;
        }
        MainMenu();
    }
}
