﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class ChangeMenu : MonoBehaviour
{
    public Button button;
    public string changeSceneName;
    public string changeSongName;

    void Start()
    {
        if (button != null)
            button.onClick.AddListener(LoadScene);
    }

    public void LoadScene()
    {
        if (changeSceneName == "Exit")
        {
            Application.Quit();
            return;
        }
        LoadSceneImmidiately();
    }

    void LoadSceneImmidiately()
    {
        SceneManager.LoadScene(changeSceneName);
        SoundManager.instance.PlayMusic(changeSongName);
	}
	
}