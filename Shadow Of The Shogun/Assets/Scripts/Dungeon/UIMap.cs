﻿using UnityEngine;
using System.Collections;

public class UIMap : MonoBehaviour
{
    public GameObject Wall;
    public GameObject Floor;
    public GameObject miniPlayer;
    public float imageSize;
    public float trueImageSize;
    int[][] map;
    bool [][] fog;
    public GameObject[] objectList;
    private Vector2 offset;
	void Start () 
    {
        map = MapManager.instance.generator.map;
        fog = MapManager.instance.fogMap;

        int x = (int)(GameManager.instance.playerPos.x / (3 * trueImageSize));
        int y = (int)(GameManager.instance.playerPos.y / (3 * trueImageSize));

        offset = new Vector2(((map.Length / 2)) * imageSize, ((map[0].Length / 2)) * imageSize);

        drawMap(x, y);
	}

	void Update () 
    {
        foreach(GameObject thing in objectList)
        {
            thing.transform.position = new Vector3(thing.transform.position.x, thing.transform.position.y, 0);
        }
    }

    void drawMap(int playerX,int playerY)
    {
        int xMax = map.Length;
        int yMax = map[0].Length;
        objectList = new GameObject[xMax*yMax+1];
        int counter = 0;
        for (int x = 0; x < xMax; x++)
        {
            for (int y = 0; y < yMax; y++)
            {
                GameObject tile = null;
                if (map[x][y] == 0)
                    tile = Wall;
                else
                    tile = Floor;
                objectList[counter]=instantiateTile(tile, x, y);
                counter++;
                if (x == playerX && y == playerY) 
                {
                    tile = miniPlayer;
                    objectList[counter] = instantiateTile(tile, x, y);
                    counter++;
                }
            }
        }
    }
    GameObject instantiateTile(GameObject tile, int x, int y)
    {
        if (tile != null)
        {
            GameObject temp = Instantiate(tile, new Vector3(x * imageSize - offset.x, y * imageSize - offset.y, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = transform;
            if (map[x][y] == -2 && tile!=miniPlayer)
                temp.GetComponent<SpriteRenderer>().color = Color.green;
            if (map[x][y] == -3 && tile != miniPlayer)
                temp.GetComponent<SpriteRenderer>().color = Color.red;
            if (map[x][y] == -4 && tile != miniPlayer)
                temp.GetComponent<SpriteRenderer>().color = Color.yellow;
            if (map[x][y] == -5 && tile != miniPlayer)
                temp.GetComponent<SpriteRenderer>().color = new Color(.5f,.5f,0);
            if (tile != miniPlayer)
            {
                int fogCounter = 0;
                if (fog[x * 3][y * 3])
                    fogCounter++;
                if (fog[x * 3 + 1][y * 3])
                    fogCounter++;
                if (fog[x * 3 + 2][y * 3])
                    fogCounter++;
                if (fog[x * 3][y * 3 + 1])
                    fogCounter++;
                if (fog[x * 3][y * 3 + 2])
                    fogCounter++;
                if (fog[x * 3 + 1][y * 3 + 1])
                    fogCounter++;
                if (fog[x * 3 + 1][y * 3 + 2])
                    fogCounter++;
                if (fog[x * 3 + 2][y * 3 + 1])
                    fogCounter++;
                if (fog[x * 3 + 2][y * 3 + 2])
                    fogCounter++;
                if (fogCounter != 0 && map[x][y] == 0)
                {
                    fogCounter = 9;
                }
                temp.transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(0, 0, 0,(float)(1-fogCounter/9.0));
            }
            return temp;
        }
        return null;
    }
}
