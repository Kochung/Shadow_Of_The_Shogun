﻿using UnityEngine;
using System.Collections;

public class Blink : MonoBehaviour 
{
    public int time;
    public int hold;

    int currentTime;
    SpriteRenderer sprite;

	void Start () 
    {
        currentTime = 0;
        sprite = GetComponent<SpriteRenderer>();
	}
	
	void Update () 
    {
	    if(sprite.enabled)
        {
            if(currentTime == time)
            {
                sprite.enabled = false;
                currentTime = 0;
            }
            else
                currentTime++;
        }
        else
        {
            if (currentTime == hold)
            {
                sprite.enabled = true;
                currentTime = 0;
            }
            else
                currentTime++;
        }
	}
}
