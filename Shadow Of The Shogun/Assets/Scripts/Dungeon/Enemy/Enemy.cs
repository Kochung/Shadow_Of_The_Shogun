﻿using UnityEngine;
using System.Collections;

public class Enemy
{
    public int speed;
    public int hold;
    public float trueImageSize;
    public GameObject enemy;
    public bool reset;


    private Animator animator;
    private AStarSearch search;
    private ItemList list;
    private int itemIndex;
    private int pathCounter;
    private char move;
    private Vector3 target;
    private Vector3 holdPosition;

    private int holdCounter;

    public Enemy()
    {
        reset = false;
    }

    public void setInfo(GameObject EnemyObject ,int [][] map, ItemList List, float imageSize)
    {
        setNewEnemy(EnemyObject);
        trueImageSize = imageSize;
        search = new AStarSearch(map);
        list = List;
    }
    public void setNewEnemy(GameObject EnemyObject)
    {
        enemy = EnemyObject;
        speed = enemy.GetComponent<EnemyScript>().speed;
        hold = enemy.GetComponent<EnemyScript>().hold;
        enemy.GetComponent<EnemyScript>().parent = this;
        animator = enemy.GetComponent<Animator>();
        animator.SetBool("Up", true);
        itemIndex = 0;
        pathCounter = 0;
        holdCounter = hold;
        move = ' ';
    }
    public void Instantiate(GameObject EnemyObject)
    {
        enemy = BattleManager.instance.Instantiate(EnemyObject);
        animator = enemy.GetComponent<Animator>();
        enemy.GetComponent<EnemyScript>().parent = this;
        moveToHold();
        reset = false;
    }
    public void Update()
    {
        if (Time.timeScale == 0)
            return;
        if(move == ' ')
        {
            if (holdCounter == hold)
            {
                roll();
                search.search(scale(enemy.transform.position.x), scale(enemy.transform.position.y), (int)list.list[itemIndex].x, (int)list.list[itemIndex].y);
                pathCounter = 0;
                updateTarget();
                holdCounter = 0;
            }
            else
            {
                holdCounter++;
            }
        }
    }

    public void FixedUpdate()
    {
        if (enemy.transform.position.x == target.x && enemy.transform.position.y == target.y)
        {
            updateTarget();
        } 
        if(move != ' ')
        {
            float step = speed * Time.deltaTime;
            enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, target, step);
        }
    }

    public int scale(float num)
    {
        return (int)(num / (3 * trueImageSize));
    }
    public void roll()
    {
        itemIndex = GameManager.instance.random.Next(0,list.list.Length);
    }
    public void updateTarget()
    {
        int horizontal = 0;
        int vertical = 0;
        if (search.path.Equals("") || pathCounter >= search.path.Length)
            move = ' ';
        else
            move = search.path[pathCounter];
        switch (move)
        {
            case 'U':
                animator.SetBool("Up", true);
                animator.SetBool("Down", false);
                animator.SetBool("Left", false);
                animator.SetBool("Right", false);
                horizontal = 0;
                vertical = 1;
                break;
            case 'D':
                animator.SetBool("Up", false);
                animator.SetBool("Down", true);
                animator.SetBool("Left", false);
                animator.SetBool("Right", false);
                horizontal = 0;
                vertical = -1;
                break;
            case 'L':
                animator.SetBool("Up", false);
                animator.SetBool("Down", false);
                animator.SetBool("Left", true);
                animator.SetBool("Right", false);
                horizontal = -1;
                vertical = 0;
                break;
            case 'R':
                animator.SetBool("Up", false);
                animator.SetBool("Down", false);
                animator.SetBool("Left", false);
                animator.SetBool("Right", true);
                horizontal = 1;
                vertical = 0;
                break;
            case ' ':
                horizontal = 0;
                vertical = 0;
                break;
        }
        if (horizontal == 0 && vertical == 0)
            animator.SetBool("Running", false);
        else
            animator.SetBool("Running", true);
        target = new Vector3(enemy.transform.position.x + (horizontal * 3 * trueImageSize), enemy.transform.position.y + (vertical * 3 * trueImageSize), 0);
        pathCounter++;
    }

    public void Hold()
    {
        holdPosition = enemy.transform.position;
    }
    public void moveToHold()
    {
        enemy.transform.position = holdPosition;
    }
}
