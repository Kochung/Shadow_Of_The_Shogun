﻿using UnityEngine;
using System.Collections;

public class EnemyList 
{
    public int size;
    public Enemy[] list;
    public EnemyList()
    {
    }
    public void Update()
    {
        for (int i = 0; i < size; i++)
        {
            list[i].Update();
        }
    }
    public void FixedUpdate()
    {
        for (int i = 0; i < size; i++)
        {
            list[i].FixedUpdate();
        }
    }
    public void reInitiate(GameObject EnemyObject, MapGenerator generator, ItemList itemList, float imageSize)
    {
        bool oneDied = false;
        for (int i = 0; i < size; i++)
        {
            if (!list[i].reset)
                list[i].Instantiate(EnemyObject);
            else
                oneDied = true;
        }
        if(oneDied)
            Refill(EnemyObject, generator, itemList, imageSize);
    }
    public void Hold()
    {
        for (int i = 0; i < size; i++)
        {
            list[i].Hold();
        }
    }
    public void Refill(GameObject EnemyObject, MapGenerator generator, ItemList itemList, float imageSize)
    {
        for (int i = 0; i < size; i++)
        {
            if (list[i].reset)
            {
                for (int j = 0; j < 200; j++)
                {
                    int xPos = BattleManager.instance.random.Next(0, generator.map.Length);
                    int yPos = BattleManager.instance.random.Next(0, generator.map[xPos].Length);
                    if (generator.map[xPos][yPos] != 0 && generator.map[xPos][yPos] != 0)
                    {
                        GameObject enemy = BattleManager.instance.Instantiate(EnemyObject, new Vector3(((xPos * 3) + 1) * imageSize, ((yPos * 3) + 1) * imageSize, 0));
                        list[i].setNewEnemy(enemy);
                        break;
                    }
                }
            }
        }
    }
    public void Spawn(int Size, GameObject EnemyObject, MapGenerator generator, ItemList itemList, float imageSize)
    {
        size = Size;
        list = new Enemy[size];
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < 200;j++ )
            {
                int xPos = BattleManager.instance.random.Next(0, generator.map.Length);
                int yPos = BattleManager.instance.random.Next(0, generator.map[xPos].Length);
                if (generator.map[xPos][yPos] != 0 && generator.map[xPos][yPos] != 0)
                {
                    GameObject enemy = BattleManager.instance.Instantiate(EnemyObject,new Vector3((( xPos * 3) +1) * imageSize, ((yPos * 3) + 1) * imageSize, 0));
                    list[i] = new Enemy();
                    list[i].setInfo(enemy, generator.map, itemList,imageSize);
                    break;
                }
            }
        }
    }
}
