﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour 
{
    public int speed;
    public int hold;
    public Enemy parent;

    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            GameManager.instance.SavePos();
            parent.reset = true;
            GameManager.instance.LoadBattle();
        }
    }
}
