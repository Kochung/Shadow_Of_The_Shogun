﻿using UnityEngine;
using System.Collections;

public class ItemList 
{
    public Vector2[] list;
    public ItemList(int length, int [][] map)
    {
        list = new Vector2[length];
        int index = 0;
        for (int i = 0; i < map.Length; i++)
        {
            for (int j = 0; j < map[i].Length; j++)
            {
                if (map[i][j] == -3 || map[i][j] == -4 || map[i][j] == -5)
                {
                    list[index] = new Vector2(i, j);
                    index++;
                }
            }
        }
    }
}
