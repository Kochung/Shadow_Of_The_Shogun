﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AStarSearch 
{
    public int[][] map;
    public bool[][] boolMap;
    public string path;

    public AStarSearch(int [][] Map)
    {
        map = Map;
        boolMap = new bool[map.Length][];
        for (int i = 0; i < boolMap.Length; i++)
            boolMap[i] = new bool[map[i].Length];

    }
    public void clear()
    {
        for (int i = 0; i < boolMap.Length; i++)
        {
            for (int j = 0; j < boolMap[i].Length; j++)
            {
                if (map[i][j] == 0 || map[i][j] == -2)
                    boolMap[i][j] = true;
                else
                    boolMap[i][j] = false;
            }
        }

    }
    public void search(int startX, int startY, int endX, int endY)
    {
        ArrayList list = new ArrayList();
        list.Add(new AStarPoint(startX, startY, 0, endX, endY, ""));
        clear();
        int stopCounter = 0;
        bool found = false;
        while (list.Count != 0 && stopCounter < 50*50 && !found)
        {
            stopCounter++;
            list.Sort();
            AStarPoint current = (AStarPoint)list[0];
            list.RemoveAt(0);
            if (current.x == endX && current.y == endY)
            {
                found = true;
                path = current.path;
            }
            boolMap[current.x][current.y] = true;
            if (current.x != 0 && !boolMap[current.x - 1][current.y])
                list.Add(new AStarPoint(current.x - 1, current.y, current.step + 1, endX, endY, current.path + "L"));
            if (current.x != boolMap.Length - 1 && !boolMap[current.x + 1][current.y])
                list.Add(new AStarPoint(current.x + 1, current.y, current.step + 1, endX, endY, current.path + "R"));
            if (current.y != 0 && !boolMap[current.x][current.y - 1])
                list.Add(new AStarPoint(current.x, current.y - 1, current.step + 1, endX, endY, current.path + "D"));
            if (current.y != boolMap[current.x].Length - 1 && !boolMap[current.x][current.y + 1])
                list.Add(new AStarPoint(current.x, current.y + 1, current.step + 1, endX, endY, current.path + "U"));
        }
        if(!found)
            path =  "";
    }
}
