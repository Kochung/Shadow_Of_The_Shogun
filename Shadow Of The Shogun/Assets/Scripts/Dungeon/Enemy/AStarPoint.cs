﻿using UnityEngine;
using System;
using System.Collections;

public class AStarPoint : IComparable
{
    public int x;
    public int y;
    public int step;
    public double weight;
    public string path;
    public const double stepWeight = 0.4;
    public const double distanceWeight = 0.6;
    public AStarPoint(int X, int Y, int Step, int endX, int endY, string Path)
    {
        x = X;
        y = Y;
        step = Step;
        path = Path;
        weight = Math.Sqrt((endX - X) * (endX - x) + (endY - y) * (endY - y));
        weight = (weight * distanceWeight) + (step * stepWeight);
    }
    public int CompareTo(object obj)
    {
        if (weight < ((AStarPoint)obj).weight)
            return -1;
        if (weight > ((AStarPoint)obj).weight)
            return 1;
        return 0;
    }
}
