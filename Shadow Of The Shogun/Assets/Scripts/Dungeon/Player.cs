﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
    public int speed;
    public bool cutscene;

    private Rigidbody2D body;
    private Animator animator;
    private Moveable moveable;
    public float maxSpeed;

    private float endX;
    private float endY;

    public int horizontalInput, 
                verticalInput,
                face;
	void Start () 
    {
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        moveable = new Moveable(body,maxSpeed);
        if (GameManager.instance.savePosision)
        {
            face = GameManager.instance.playerFace;
            animatorChangeFace(5,face);
        }
        else
        {
            face = 8;
            animator.SetBool("Up", true);
        }
	}
	
	void Update ()
    {
        if (Time.timeScale == 0)
            return;
        if (cutscene)
            return;
        horizontalInput = (int)Input.GetAxisRaw("Horizontal");
        verticalInput = (int)Input.GetAxisRaw("Vertical");

        if (InputHandler.instance.StartButton && BattleManager.instance.floorNumber != 4 && BattleManager.instance.floorNumber != 5)
        {
            GameManager.instance.SavePos();
            GameManager.instance.LoadPause();
        }
        if (Input.GetKeyDown(KeyCode.Y) && BattleManager.instance.floorNumber != 4 && BattleManager.instance.floorNumber != 5)
        {
            transform.position = new Vector2(endX, endY);
        }
	}

    void FixedUpdate()
    {
        moveable.move(horizontalInput * speed, verticalInput * speed);
        if (horizontalInput == 0 && verticalInput == 0)
            animator.SetBool("Running", false);
        else
            animator.SetBool("Running", true);
        changeface();

    }
    int getDirection()
    {
        if (verticalInput == 1)
        {
            switch (horizontalInput)
            {
                case 1:
                    return 9;
                case 0:
                    return 8;
                case -1:
                    return 7;
            }
        }
        else if (verticalInput == 0)
        {
            switch (horizontalInput)
            {
                case 1:
                    return 6;
                case 0:
                    return 5;
                case -1:
                    return 4;
            }
        }
        else if (verticalInput == -1)
        {
            switch (horizontalInput)
            {
                case 1:
                    return 3;
                case 0:
                    return 2;
                case -1:
                    return 1;
            }
        }
        return 0;
    }
    void changeface()
    {
        int oldFace = face;
        int newface = getDirection();
        if (newface != 5 && oldFace != newface)
        {
            animatorChangeFace(oldFace,newface);
            face = newface;
        }
    }
    void animatorChangeFace(int oldFace,int newFace)
    {
        switch(oldFace)
        {
            case 9:
                animator.SetBool("UpRight",false);
                break;
            case 8:
                animator.SetBool("Up", false);
                break;
            case 7:
                animator.SetBool("UpLeft", false);
                break;
            case 6:
                animator.SetBool("Right", false);
                break;
            case 4:
                animator.SetBool("Left", false);
                break;
            case 3:
                animator.SetBool("DownRight", false);
                break;
            case 2:
                animator.SetBool("Down", false);
                break;
            case 1:
                animator.SetBool("DownLeft", false);
                break;

        }
        switch(newFace)
        {

            case 9:
                animator.SetBool("UpRight", true);
                break;
            case 8:
                animator.SetBool("Up", true);
                break;
            case 7:
                animator.SetBool("UpLeft", true);
                break;
            case 6:
                animator.SetBool("Right", true);
                break;
            case 4:
                animator.SetBool("Left", true);
                break;
            case 3:
                animator.SetBool("DownRight", true);
                break;
            case 2:
                animator.SetBool("Down", true);
                break;
            case 1:
                animator.SetBool("DownLeft", true);
                break;
        }
    }
    public int getFace()
    {
        return face;
    }
    public void stop()
    {
        moveable.stop();
        animator.SetBool("Running", false);
    }
    public void setEnd(float x, float y)
    {
        endX = x;
        endY = y;
    }
}
