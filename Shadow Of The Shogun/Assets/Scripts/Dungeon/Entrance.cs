﻿using UnityEngine;
using System.Collections;

public class Entrance : MonoBehaviour 
{
    private bool active;
    void Start() {}
    void Update()
    {
        if (Time.timeScale == 0) 
            return;
        if (active && InputHandler.instance.AButton)
        {
            Open();
        }
    }
    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            active = true;
            if (InputHandler.instance.AButton)
            {
                Open();
            }
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            active = false;
    }

    private void Open()
    {
        BattleManager.instance.floorNumber = 1;
        MapManager.instance.clearMap();
        GameManager.instance.KillSave();
        GameManager.instance.LoadTown();
    }
}

