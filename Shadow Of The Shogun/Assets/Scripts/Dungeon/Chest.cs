﻿using UnityEngine;
using System.Collections;

public class Chest : MonoBehaviour {

    private Animator animator;
    private bool forcedOpen = false;
    private bool active;
    private int x;
    private int y;
    void Start()
    {
        animator = GetComponent<Animator>();
        if (forcedOpen)
            animator.SetBool("Open", true);
    }
    void Update()
    {
        if (Time.timeScale == 0) 
            return;
        if (active && InputHandler.instance.AButton && !animator.GetBool("Open"))
        {
            Open();
        }
    }
    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            active = true;
            if (InputHandler.instance.AButton && !animator.GetBool("Open"))
            {
                Open();
            }
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            active = false;
    }

    private void Open()
    {
        animator.SetBool("Open", true);
        MapManager.instance.generator.map[x][y] = -5;
        int goldAmount = getGoldAmount()* 2;
        Vector2 item = rollItem();
        BattleManager.instance.gold.add(goldAmount);
        string[] message = { "Found " + goldAmount + " gold" };
        if ((int)item.x != -1)
        {
            BattleManager.instance.bag.addItem((int)item.x, (int)item.y);
            message[0] += "\nFound "+ (int)item.y + " "+ BattleManager.instance.bag.bag[(int)item.x].name;
        }
        GameObject.Find("MainTextBox").GetComponent<BigTextBox>().SetMessage(message);
    }
    private int getGoldAmount()
    {
        int returnGold = 0;
        int ememyNumber = BattleManager.instance.getEnemyNumber();
        for (int i = 0; i < ememyNumber; i++)
        {
            returnGold += BattleManager.instance.getRandomEnemies().rollGold();
        }
        return returnGold;
    }
    public Vector2 rollItem()
    {
        int itemNumber = BattleManager.instance.random.Next(0, 2);
        if (itemNumber == 1)
            return new Vector2(-1,0);
        itemNumber = BattleManager.instance.random.Next(0, 100);
        int quantity = BattleManager.instance.random.Next(1, 5);
        if (itemNumber < 16)
            return new Vector2(0, quantity);
        if (itemNumber < 28)
            return new Vector2(1, quantity);
        if (itemNumber < 34)
            return new Vector2(2, quantity);
        if (itemNumber < 50)
            return new Vector2(3, quantity);
        if (itemNumber < 62)
            return new Vector2(4, quantity);
        if (itemNumber < 68)
            return new Vector2(5, quantity);
        if (itemNumber < 78)
            return new Vector2(6, quantity);
        if (itemNumber < 82)
            return new Vector2(7, quantity);
        if (itemNumber < 84)
            return new Vector2(8, quantity);
        if (itemNumber < 94)
            return new Vector2(9, quantity);
        else
            return new Vector2(10, quantity);
    }
    public void setPos(int X,int Y)
    {
        x = X;
        y = Y;
    }
    public void forceOpen()
    {
        forcedOpen = true;
    }
}
