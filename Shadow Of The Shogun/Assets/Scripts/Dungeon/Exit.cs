﻿using UnityEngine;
using System.Collections;

public class Exit : MonoBehaviour 
{
    private bool active;
    private int floorNumber;
    void Start()
    {
        floorNumber = BattleManager.instance.floorNumber;
    }
    void Update()
    {
        if (Time.timeScale == 0) 
            return;
        if (active && InputHandler.instance.AButton)
        {
            Open();
        }
    }
    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            active = true;
            if (InputHandler.instance.AButton)
            {
                Open();
            }
        }
    }

    void OnTriggerExit2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
            active = false;
    }

    private void Open()
    {
        BattleManager.instance.floorNumber = floorNumber + 1;
        MapManager.instance.clearMap();
        GameManager.instance.KillSave();
        if(floorNumber == 3)
            GameManager.instance.Load("DungeonFloor5", "");
        else if (floorNumber == 4)
            GameManager.instance.Load("DungeonFloor6", "");
        else
            GameManager.instance.LoadDungeon();
    }
}
