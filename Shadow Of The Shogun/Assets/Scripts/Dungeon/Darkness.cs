﻿using UnityEngine;
using System.Collections;

public class Darkness : MonoBehaviour 
{
    private SpriteRenderer sprite;
    public bool moveUp;
    public bool visible;
	void Start () 
    {
        sprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
        sprite.color = new Color(0, 0, 0, 1);
        visible = false;
	}
	
	public void set() 
    {
        if(!visible)
        {
            if (sprite != null)
            {
                sprite.color = new Color(0, 0, 0, 0);
                if(moveUp)
                    GetComponent<SpriteRenderer>().sortingLayerID = sprite.sortingLayerID;
                visible = true;
            }
        }
	}
}
