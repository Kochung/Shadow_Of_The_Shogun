﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FogOfWar
{
    public bool[][] fogMap;
    public GameObject[][] map;

    private GameObject player;
    private int maxSteps;
    private float imageSize;
    bool loaded;

    private class Tile
    {
        public int x;
        public int y;
        public int stepCount;
        public Tile(int X,int Y, int StepCount)
        {
            x = X;
            y = Y;
            stepCount = StepCount;
        }
    }
    public FogOfWar(int x, int y, GameObject [][] Map, int MaxSteps, float ImageSize) 
    {
        player = GameObject.Find("Player");
        map = Map;
        fogMap = new bool[x][];
        for (int i = 0; i < x; i++)
        {
            fogMap[i]=new bool[y];
        }
        maxSteps = MaxSteps;
        imageSize = ImageSize;
        loaded = false;
	}
    public void reStart(GameObject[][] Map)
    {
        player = GameObject.Find("Player");
        map = Map;
        loaded = true;
    }
    public bool reClear()
    {
        for (int i = 0; i < map.Length; i++)
        {
            for (int j = 0; j < map[i].Length; j++)
            {
                if (MapManager.instance.fogMap[i][j])
                {
                    map[i][j].GetComponent<Darkness>().set();
                    if (!map[i][j].GetComponent<Darkness>().visible)
                        return false;
                }
            }
        }
        return true;
    }
	public void Update () 
    {
        if(loaded)
        {
            if(reClear())
                loaded = false;
        }
        int x = (int)(player.transform.position.x / (imageSize));
        int y = (int)(player.transform.position.y / (imageSize));
        if (map[x][y].transform.name.StartsWith("Wall"))
            return;
        breath(x,y);
	}
    public void clear()
    {
        for (int i = 0; i < fogMap.Length; i++)
        {
            for (int j = 0; j < fogMap[i].Length; j++)
            {
                if (map[i][j].transform.name.StartsWith("Wall"))
                    fogMap[i][j] = true;
                else
                    fogMap[i][j] = false;
            }
        }
    }
    void breath(int playerX, int playerY)
    {
        Queue<Tile> queue = new Queue<Tile>();
        queue.Enqueue(new Tile(playerX,playerY,0));
        clear();
        fogMap[playerX][playerY] = true;
        map[playerX][playerY].GetComponent<Darkness>().set();
        while(queue.Count!=0)
        {
            Tile tile = queue.Dequeue();
            int x = tile.x;
            int y = tile.y;
            if (tile.stepCount != maxSteps)
            {
                map[x + 1][y].GetComponent<Darkness>().set();
                map[x - 1][y].GetComponent<Darkness>().set();
                map[x][y + 1].GetComponent<Darkness>().set();
                map[x][y - 1].GetComponent<Darkness>().set();
                map[x + 1][y + 1].GetComponent<Darkness>().set();
                map[x + 1][y - 1].GetComponent<Darkness>().set();
                map[x - 1][y + 1].GetComponent<Darkness>().set();
                map[x - 1][y - 1].GetComponent<Darkness>().set();
                if (!fogMap[x + 1][y])
                {
                    queue.Enqueue(new Tile(x + 1, y, tile.stepCount + 1));
                    fogMap[x + 1][y] = true;
                }
                if (!fogMap[x][y + 1])
                {
                    queue.Enqueue(new Tile(x, y + 1, tile.stepCount + 1));
                    fogMap[x][y + 1] = true;
                }
                if (!fogMap[x - 1][y])
                {
                    queue.Enqueue(new Tile(x - 1, y, tile.stepCount + 1));
                    fogMap[x - 1][y] = true;
                }
                if (!fogMap[x][y - 1])
                {
                    queue.Enqueue(new Tile(x, y - 1, tile.stepCount + 1));
                    fogMap[x][y - 1] = true;
                }


                if (!fogMap[x + 1][y + 1])
                {
                    queue.Enqueue(new Tile(x + 1, y + 1, tile.stepCount + 1));
                    fogMap[x + 1][y + 1] = true;
                }
                if (!fogMap[x + 1][y - 1])
                {
                    queue.Enqueue(new Tile(x + 1, y - 1, tile.stepCount + 1));
                    fogMap[x + 1][y - 1] = true;
                }
                if (!fogMap[x - 1][y + 1])
                {
                    queue.Enqueue(new Tile(x - 1, y + 1, tile.stepCount + 1));
                    fogMap[x - 1][y + 1] = true;
                }
                if (!fogMap[x - 1][y - 1])
                {
                    queue.Enqueue(new Tile(x - 1, y - 1, tile.stepCount + 1));
                    fogMap[x - 1][y - 1] = true;
                }
            }
        }
    }
}
