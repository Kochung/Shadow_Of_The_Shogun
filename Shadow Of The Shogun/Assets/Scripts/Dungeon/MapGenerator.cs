﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator
{
    private int mapXSize;
    private int mapYSize;
    private int roomMinWidth;
    private int roomMaxWidth;
    private int roomMinHeight;
    private int roomMaxHeight;
    private int maxRooms;
    private int roomAttempts;
    private int chestMin;
    private int chestMax;
    private int startAndEndAttempts;

    public int [][] map;
    private Room[] rooms;
    private System.Random random;

    private class Point
    {
        public int x;
        public int y;
        public Point(int X,int Y)
        {
            x = X;
            y = Y;
        }
    }

    private class Room
    {
        public int x;
        public int y;
        public int width;
        public int height;
        public Room(int X,int Y, int Width, int Height)
        {
            x = X;
            y = Y;
            width = Width;
            height = Height;
        }
        public bool canSpawnItem()
        {
            return xMin() <= xMax() && yMin() <= yMax();
        }
        public int xMin()
        {
            return x + 1;
        }
        public int yMin()
        {
            return y + 1;
        }
        public int xMax()
        {
            return x + width - 1;
        }
        public int yMax()
        {
            return y + height - 1;
        }
    }

	public MapGenerator (Map fullMap) 
    {
        random = new System.Random();
        mapXSize = fullMap.mapXSize;
        mapYSize = fullMap.mapYSize;
        roomMinWidth = fullMap.roomMinWidth;
        roomMaxWidth = fullMap.roomMaxWidth;
        roomMinHeight = fullMap.roomMinHeight;
        roomMaxHeight = fullMap.roomMaxHeight;
        maxRooms = fullMap.maxRooms;
        roomAttempts = fullMap.roomAttempts;
        chestMin = fullMap.chestMin;
        chestMax = fullMap.chestMax;
        startAndEndAttempts = fullMap.startAndEndAttempts;
        setupMap();
	}
	
    void setupMap()
    {
        bool stop = false;
        while(!stop)
        {
            map = new int[mapXSize][];
            for (int i = 0; i < mapXSize; i++)
                map[i] = new int[mapYSize];

            int roomCount = 0;
            rooms = new Room[maxRooms];
            for (int i = 0; i < roomAttempts && roomCount < maxRooms; i++)
            {
                if (makeRoom(roomCount))
                    roomCount++;
            }
            int pathValue = 2;
            for (int i = 0; i < mapXSize * mapYSize; i++)
            {
                int x = random.Next(1, mapXSize - 1);
                int y = random.Next(1, mapYSize - 1);
                if (checkTile(x, y, 0))
                {
                    makeMaze(x, y, pathValue);
                    pathValue++;
                }
            }
            for (int i = 0; i < roomCount; i++)
            {
                connectRooms(rooms[i]);
            }
            fillDeadEnds();
            addItems(roomCount);
            if (checkMap())
                stop = true;
        }
    }
    bool checkMap()
    {
        AStarSearch test = new AStarSearch(map);
        int startX = 0;
        int startY = 0;
        int endX = 0;
        int endY = 0;
        for (int i = 0; i < map.Length; i++)
        {
            for (int j = 0; j < map[i].Length; j++)
            {
                if (map[i][j] == -2)
                {
                    startX = i;
                    startY = j;
                }
                else if (map[i][j] == -3)
                {
                    endX = i;
                    endY = j;
                }
            }
        }
        test.search(startX, startY, endX, endY);
        return !test.path.Equals("");
    }
    bool makeRoom(int roomCount)
    {
        int width = random.Next(roomMinWidth, roomMaxWidth);
        int height = random.Next(roomMinHeight, roomMaxHeight);
        int x = random.Next(1, mapXSize - width);
        int y = random.Next(1, mapYSize - height);
        if(Scan(x,y,width,height))
        {
            fillRoom(x,y,width,height);
            rooms[roomCount] = new Room(x, y, width, height);
            return true;
        }
        return false;
    }
    bool Scan(int x,int y,int width,int height)
    {
        for (int i = x - 1; i < x + width + 1;i++ )
        {
            for (int j = y - 1; j < y + height + 1; j++)
            {
                if(map[i][j] != 0)
                    return false;
            }
        }
        return true;
    }
    void fillRoom(int x, int y, int width, int height)
    {
        for (int i = x; i < x + width; i++)
        {
            for (int j = y; j < y + height; j++)
            {
                map[i][j] = 1;
            }
        }
    }

    void makeMaze(int startX, int startY, int pathValue)
    {
        Stack<Point> stack = new Stack<Point>();
        stack.Push(new Point(startX, startY));
        while(stack.Count!=0)
        {
            Point point = stack.Pop();
            int x = point.x;
            int y = point.y;
            map[x][y] = pathValue;
            Point[] list = new Point[4];
            int index = 0;
            if (checkTile(x + 1, y, 1))
            {
                list[index] = new Point(x + 1, y);
                index++;
            }
            if (checkTile(x - 1, y, 2))
            {
                list[index] = new Point(x - 1, y);
                index++;
            }
            if (checkTile(x, y + 1, 3))
            {
                list[index] = new Point(x, y + 1);
                index++;
            }
            if (checkTile(x, y - 1, 4))
            {
                list[index] = new Point(x, y - 1);
                index++;
            }
            if (index != 0)
            {
                stack.Push(point);
                int nextPoint = random.Next(0, index); 
                stack.Push(list[nextPoint]);
            }
        }
    }
    bool checkTile(int x, int y, int ignore)
    {
        if (x < 1 || y < 1 || x > mapXSize - 2 || y > mapYSize - 2) 
            return false;
        if (map[x][y] != 0)
            return false;
        if (map[x + 1][y] != 0 && ignore != 2)
            return false;
        if (map[x - 1][y] != 0 && ignore != 1)
            return false;
        if (map[x][y + 1] != 0 && ignore != 4)
            return false;
        if (map[x][y - 1] != 0 && ignore != 3)
            return false;
        if (map[x + 1][y + 1] != 0 && ignore != 2 && ignore != 4)
            return false;
        if (map[x + 1][y - 1] != 0 && ignore != 2 && ignore != 3)
            return false;
        if (map[x - 1][y + 1] != 0 && ignore != 1 && ignore != 4)
            return false;
        if (map[x - 1][y - 1] != 0 && ignore != 1 && ignore != 3)
            return false;
        return true;
    }

    void connectRooms(Room room)
    {
        ArrayList paths = getConnectedPaths(room);

        if (paths.Count == 0)
        {
            for (int i = room.x; i < room.x + room.width; i++)
            {
                for (int j = room.y; j < room.y + room.height; j++)
                {
                    map[i][j] = 0;
                }
            }
            return;
        }
        int num = random.Next(0, 2);
        if (num == 0)
            connectTop(room, paths);
        num = random.Next(0, 2);
        if (num == 0)
            connectBottom(room, paths);
        num = random.Next(0, 2);
        if (num == 0)
            connectLeft(room, paths);
        num = random.Next(0, 2);
        if (num == 0)
            connectRight(room, paths);
        for (int i = 0; i < paths.Count; i++)
        {
            connectPathNum(room,((int)paths[i]));
        }

    }
    ArrayList getConnectedPaths(Room room)
    {
        ArrayList paths = new ArrayList();
        for (int x = room.x; x < room.x + room.width; x++)
        {
            int bot = getPath(x, room.y - 1);
            int top = getPath(x, room.y + room.height);
            if (bot != 0 && !paths.Contains(bot))
                paths.Add(bot);
            if (top != 0 && !paths.Contains(top))
                paths.Add(top);
        }
        for (int y = room.y; y < room.y + room.height; y++)
        {
            int left = getPath(room.x - 1, y);
            int right = getPath(room.x + room.width, y);
            if (left != 0 && !paths.Contains(left))
                paths.Add(left);
            if (right != 0 && !paths.Contains(right))
                paths.Add(right);
        }
        return paths;
    }

    void connectTop(Room room, ArrayList paths)
    {
        ArrayList doors = new ArrayList();
        for (int x = room.x; x < room.x + room.width; x++)
        {
            int num = getPath(x, room.y + room.height);
            if (num != 0)
                doors.Add(new Point(x, room.y + room.height));
        }
        if (doors.Count != 0)
        {
            int index = random.Next(0, doors.Count);
            int pathNum = getPath(((Point)doors[index]).x, ((Point)doors[index]).y);
            paths.Remove(pathNum);
            map[((Point)doors[index]).x][((Point)doors[index]).y] = -1;
        }
    }
    void connectBottom(Room room, ArrayList paths) 
    {
        ArrayList doors = new ArrayList();
        for (int x = room.x; x < room.x + room.width; x++)
        {
            int num = getPath(x, room.y - 1);
            if (num != 0)
                doors.Add(new Point(x, room.y - 1));
        }
        if (doors.Count != 0)
        {
            int index = random.Next(0, doors.Count);
            int pathNum = getPath(((Point)doors[index]).x, ((Point)doors[index]).y);
            paths.Remove(pathNum);
            map[((Point)doors[index]).x][((Point)doors[index]).y] = -1;
        }
    }
    void connectLeft(Room room, ArrayList paths)
    {
        ArrayList doors = new ArrayList();
        for (int y = room.y; y < room.y + room.height; y++)
        {
            int num = getPath(room.x - 1, y);
            if (num != 0)
                doors.Add(new Point(room.x - 1, y));
        }
        if (doors.Count != 0)
        {
            int index = random.Next(0, doors.Count);
            int pathNum = getPath(((Point)doors[index]).x, ((Point)doors[index]).y);
            paths.Remove(pathNum);
            map[((Point)doors[index]).x][((Point)doors[index]).y] = -1;
        }
    }
    void connectRight(Room room, ArrayList paths)
    {
        ArrayList doors = new ArrayList();
        for (int y = room.y; y < room.y + room.height; y++)
        {
            int num = getPath(room.x + room.width, y);
            if (num != 0)
                doors.Add(new Point(room.x + room.width, y));
        }
        if (doors.Count != 0)
        {
            int index = random.Next(0, doors.Count);
            int pathNum = getPath(((Point)doors[index]).x, ((Point)doors[index]).y);
            paths.Remove(pathNum);
            map[((Point)doors[index]).x][((Point)doors[index]).y] = -1;
        }
    }
    void connectPathNum(Room room, int pathNum) 
    {
        ArrayList doors = new ArrayList();
        for (int x = room.x; x < room.x + room.width; x++)
        {
            int num = getPath(x, room.y + room.height);
            if (num == pathNum)
                doors.Add(new Point(x, room.y + room.height));
            num = getPath(x, room.y - 1);
            if (num == pathNum)
                doors.Add(new Point(x, room.y - 1));
        }
        for (int y = room.y; y < room.y + room.height; y++)
        {
            int num = getPath(room.x - 1, y);
            if (num == pathNum)
                doors.Add(new Point(room.x - 1, y)); 
            num = getPath(room.x + room.width, y);
            if (num == pathNum)
                doors.Add(new Point(room.x + room.width, y));
        }
        if (doors.Count != 0)
        {
            int index = random.Next(0, doors.Count);
            map[((Point)doors[index]).x][((Point)doors[index]).y] = -1;
        }
    }
    int getPath(int x,int y)
    {
        if (x < 1 || y < 1 || x > mapXSize - 2 || y > mapYSize - 2)
            return 0;
        if (map[x][y] != 0)
            return 0;
        if (map[x+1][y] > 1)
            return map[x+1][y];
        if (map[x-1][y] > 1)
            return map[x-1][y];
        if (map[x][y+1] > 1)
            return map[x][y+1];
        if (map[x][y-1] > 1)
            return map[x][y-1];
        return 0;
    }

    void fillDeadEnds()
    {
        Stack<Point> points = new Stack<Point>();
        for(int i=0;i<mapXSize;i++)
        {
            for(int j=0;j<mapYSize;j++)
            {
                if (checkDeadEnd(i,j))
                    points.Push(new Point(i,j));
            }
        }
        while(points.Count!=0)
        {
            Point point = points.Pop();
            int x = point.x;
            int y = point.y;
            map[x][y] = 0;
            if (checkDeadEnd(x + 1, y))
                points.Push(new Point(x + 1, y));
            if (checkDeadEnd(x - 1, y))
                points.Push(new Point(x - 1, y));
            if (checkDeadEnd(x, y + 1))
                points.Push(new Point(x, y + 1));
            if (checkDeadEnd(x, y - 1))
                points.Push(new Point(x, y - 1));
        }
    }
    bool checkDeadEnd(int x, int y)
    {
        if (map[x][y] == 0)
            return false;
        int counter = 0;
        if (map[x + 1][y] != 0) 
            counter++;
        if (map[x - 1][y] != 0) 
            counter++;
        if (map[x][y + 1] != 0) 
            counter++;
        if (map[x][y - 1] != 0) 
            counter++;
        if (counter > 1)
            return false;
        return true;
    }

    void addItems(int roomCount)
    {
        addItem(roomCount, -2);
        addItem(roomCount, -3);
        int numberOcChests = random.Next(chestMin,chestMax);
        for (int i = 0; i < numberOcChests; i++)
        {
            addItem(roomCount, -4);
        }
    }
    void addItem(int roomCount,int type)
    {
        for (int i = 0; i < startAndEndAttempts; i++)
        {
            int index = random.Next(0, roomCount);
            if (rooms[index] != null && rooms[index].canSpawnItem())
            {
                int x = random.Next(rooms[index].xMin(), rooms[index].xMax());
                int y = random.Next(rooms[index].yMin(), rooms[index].yMax());
                if (map[x][y] == 1)
                {
                    map[x][y] = type;
                    return;
                }
            }
        }
    }

}
