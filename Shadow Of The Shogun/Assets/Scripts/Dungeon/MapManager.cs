﻿using UnityEngine;
using System.Collections;

public class MapManager : MonoBehaviour 
{
    
    public static MapManager instance = null;
    public MapGenerator generator;
    public FogOfWar fog;
    public bool[][] fogMap;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
	void Start () {}
	void Update () {}

    public void clearMap()
    {
        generator = null;
        fog = null;
    }
    public bool hasMap()
    {
        return generator != null;
    }
    public void setMap(MapGenerator newGenerator)
    {
        generator = newGenerator;
    }
    public void setFog(FogOfWar newFog)
    {
        fog = newFog;
    }
    public void setFogMap()
    {
        fogMap = new bool[fog.map.Length][];
        for (int i = 0; i < fog.map.Length; i++)
        {
            fogMap[i] = new bool[fog.map[i].Length];
            for (int j = 0; j < fog.map[i].Length; j++)
            {
                fogMap[i][j] = fog.map[i][j].GetComponent<Darkness>().visible;
            }
        }
    }
}
