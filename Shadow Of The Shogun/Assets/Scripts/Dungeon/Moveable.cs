﻿using UnityEngine;
using System.Collections;

public class Moveable
{
    private Rigidbody2D body;
    private float maxSpeed;

    public Moveable(Rigidbody2D Body, float speed)
    {
        body = Body;
        maxSpeed = speed;
    }
    public void move(float x, float y)
    {
        stop();
        body.AddForce(new Vector2(x, y));
        checkSpeed();
    }
    public void moveX(float x)
    {
        stop();
        body.AddForce(new Vector2(x, 0));
        checkSpeed();
    }
    public void moveY(float y)
    {
        stop();
        body.AddForce(new Vector2(0, y));
        checkSpeed();
    }
    public void stop()
    {
        body.velocity = new Vector2(0,0);
    }
    public void checkSpeed()
    {
        float speedx = body.velocity.x;
        float speedy = body.velocity.y;
        if (speedx > maxSpeed)
            body.velocity = new Vector2(maxSpeed, body.velocity.y);
        if (speedx < -maxSpeed)
            body.velocity = new Vector2(-maxSpeed, body.velocity.y);
        if(speedy > maxSpeed)
            body.velocity = new Vector2(body.velocity.x, maxSpeed);
        if (speedy < -maxSpeed)
            body.velocity = new Vector2(body.velocity.x, -maxSpeed);
    }
}
