﻿using UnityEngine;
using System.Collections;

public class Map : MonoBehaviour 
{

    public GameObject [] Floor;
    public GameObject [] Wall;
    public GameObject Chest;
    public GameObject []Exit;
    public GameObject []Entrance;
    public GameObject camraBlocker;
    public GameObject EnemyObject;

    public int enemyCount;
    public float imageSize;
    public int mapXSize;
    public int mapYSize;
    public int roomMinWidth;
    public int roomMaxWidth;
    public int roomMinHeight;
    public int roomMaxHeight;
    public int maxRooms;
    public int roomAttempts;
    public int chestMin;
    public int chestMax;
    public int startAndEndAttempts;
    public int fogOfWarDistance;

    public MapGenerator generator;
    public FogOfWar fog;
    public GameObject[][] map;
    public ItemList itemList;
	void Start () 
    {
        int startX = 0;
        int startY = 0;
        int endX = 0;
        int endY = 0;
        int itemCount = 0;
        if (!MapManager.instance.hasMap())
        {
            generator = new MapGenerator(this);
        }
        else
        {
            generator = MapManager.instance.generator;
        }
        map = new GameObject[mapXSize * 3][];
        for (int i = 0; i < mapXSize * 3; i++)
            map[i] = new GameObject[mapYSize * 3];
        for (int i = 0; i < mapXSize; i++)
        {
            for (int j = 0; j < mapYSize; j++)
            {
                switch (generator.map[i][j])
                {
                    case 0: 
                        if (j == 0)
                        {
                            if (generator.map[i][j + 1] != 0)
                                setWallTopBot(i * 3, j * 3);
                            else
                                setWallBot(i * 3, j * 3);
                        }
                        else if (j == generator.map[i].Length - 1)
                        {

                            if (generator.map[i][j - 1] != 0)
                                setWallTopBot(i * 3, j * 3);
                            else
                                setWallTop(i * 3, j * 3);
                        }
                        else if (generator.map[i][j + 1] == 0 && generator.map[i][j - 1] == 0)
                            setWallMid(i * 3, j * 3);
                        else if (generator.map[i][j + 1] != 0 && generator.map[i][j - 1] != 0)
                            setWallTopBot(i * 3, j * 3);
                        else if (generator.map[i][j + 1] == 0)
                            setWallBot(i * 3, j * 3);
                        else if (generator.map[i][j - 1] == 0)
                            setWallTop(i * 3, j * 3);
                        break;
                    case -2:
                        startX = (i * 3) + 1;
                        startY = (j * 3) + 1;
                        setStart(i * 3, j * 3);
                        break;
                    case -3:
                        endX = (i * 3) + 1;
                        endY = (j * 3) + 1;
                        setEnd(i * 3, j * 3);
                        itemCount++;
                        break;
                    case -4:
                        setChest(i * 3, j * 3);
                        itemCount++;
                        break;
                    case -5:
                        setOpenChest(i * 3, j * 3);
                        itemCount++;
                        break;
                    default:
                        setFloor(i * 3, j * 3);
                        break;

                }
            }
        }
        setCamraBlockers();
        itemList = new ItemList(itemCount,generator.map);

        if (GameManager.instance.savePosision)
        {
            fog = MapManager.instance.fog;
            fog.reStart(map);
            GameObject.Find("Player").transform.position = GameManager.instance.playerPos;
            GameObject.Find("Player").GetComponent<Player>().setEnd(endX * imageSize, endY * imageSize);
            GameObject.Find("Main Camera").transform.position = GameManager.instance.cameraPos;
            GameManager.instance.enemyList.reInitiate(EnemyObject, generator, itemList, imageSize);
        }
        else
        {

            fog = new FogOfWar(mapXSize * 3, mapYSize * 3, map, fogOfWarDistance, imageSize);
            MapManager.instance.setFog(fog);
            MapManager.instance.setMap(generator);
            GameObject.Find("Player").transform.position = new Vector2(startX * imageSize, startY * imageSize);
            GameObject.Find("Player").GetComponent<Player>().setEnd(endX * imageSize, endY * imageSize);
            Transform camera = GameObject.Find("Main Camera").transform;
            camera.position = new Vector3(startX * imageSize, startY * imageSize, camera.position.z);
            GameManager.instance.enemyList.Spawn(enemyCount, EnemyObject, generator, itemList, imageSize);
        }

	}
	void Update ()
    {
        if (Time.timeScale == 0)
            return;
        GameManager.instance.enemyList.Update();
        fog.Update();
    }
    void FixedUpdate()
    {
        GameManager.instance.enemyList.FixedUpdate();
    }
    void setStart(int x,int y)
    {
        instantiateTile(Floor[0], x, y);
        instantiateTile(Floor[1], x + 1, y);
        instantiateTile(Floor[2], x + 2, y);
        instantiateTile(Floor[3], x, y + 1);
        instantiateTile(Entrance[0], x + 1, y + 1);
        instantiateTile(Entrance[1], x + 2, y + 1);
        instantiateTile(Floor[6], x, y + 2);
        instantiateTile(Floor[7], x + 1, y + 2);
        instantiateTile(Floor[8], x + 2, y + 2);
    }
	void setEnd(int x,int y)
    {
        instantiateTile(Floor[0], x, y);
        instantiateTile(Floor[1], x + 1, y);
        instantiateTile(Floor[2], x + 2, y);
        instantiateTile(Floor[3], x, y + 1);
        instantiateTile(Exit[0], x + 1, y + 1);
        instantiateTile(Exit[1], x + 2, y + 1);
        instantiateTile(Floor[6], x, y + 2);
        instantiateTile(Exit[2], x + 1, y + 2);
        instantiateTile(Exit[3], x + 2, y + 2);
    }
    void setChest(int x, int y)
    {
        instantiateTile(Floor[0], x, y);
        instantiateTile(Floor[1], x + 1, y);
        instantiateTile(Floor[2], x + 2, y);
        instantiateTile(Floor[3], x, y + 1);
        instantiateChest(x + 1, y + 1);
        instantiateTile(Floor[5], x + 2, y + 1);
        instantiateTile(Floor[6], x, y + 2);
        instantiateTile(Floor[7], x + 1, y + 2);
        instantiateTile(Floor[8], x + 2, y + 2);
    }

    void setOpenChest(int x, int y)
    {
        instantiateTile(Floor[0], x, y);
        instantiateTile(Floor[1], x + 1, y);
        instantiateTile(Floor[2], x + 2, y);
        instantiateTile(Floor[3], x, y + 1);
        instantiateOpenChest(x + 1, y + 1);
        instantiateTile(Floor[5], x + 2, y + 1);
        instantiateTile(Floor[6], x, y + 2);
        instantiateTile(Floor[7], x + 1, y + 2);
        instantiateTile(Floor[8], x + 2, y + 2);
    }

    void setWallTop(int x,int y)
    {
        if ((x / 3) != 0 && generator.map[(x / 3) - 1][y / 3] == 0)
            instantiateTile(Wall[9], x, y);
        else
            instantiateTile(Wall[3], x, y);
        instantiateTile(Wall[4], x + 1, y);
        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[10], x + 2, y);
        else
            instantiateTile(Wall[5], x + 2, y);
        
        if ((x / 3) != 0 && generator.map[(x / 3) - 1][y / 3] == 0)
            instantiateTile(Wall[9], x, y + 1);
        else
            instantiateTile(Wall[3], x, y + 1);
        instantiateTile(Wall[4], x + 1, y + 1);
        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[10], x + 2, y + 1);
        else
            instantiateTile(Wall[5], x + 2, y + 1);

        if ((x/3) !=0 && generator.map[(x / 3)-1][y / 3] == 0)
            instantiateTile(Wall[0], x, y + 2);
        else
            instantiateTile(Wall[6], x, y + 2);

        instantiateTile(Wall[7], x + 1, y + 2);

        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[2], x + 2, y + 2);
        else
            instantiateTile(Wall[8], x + 2, y + 2);
    }

    void setWallBot(int x, int y)
    {
        instantiateTile(Wall[0], x, y);
        instantiateTile(Wall[1], x + 1, y);
        instantiateTile(Wall[2], x + 2, y); 
        if ((x / 3) != 0 && generator.map[(x / 3) - 1][y / 3] == 0)
            instantiateTile(Wall[9], x, y + 1);
        else
            instantiateTile(Wall[3], x, y + 1);
        instantiateTile(Wall[4], x + 1, y + 1);
        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[10], x + 2, y + 1);
        else
            instantiateTile(Wall[5], x + 2, y + 1);
        
        if ((x / 3) != 0 && generator.map[(x / 3) - 1][y / 3] == 0)
            instantiateTile(Wall[9], x, y + 2);
        else
            instantiateTile(Wall[3], x, y + 2);
        instantiateTile(Wall[4], x + 1, y + 2);
        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[10], x + 2, y + 2);
        else
            instantiateTile(Wall[5], x + 2, y + 2);
    }

    void setWallMid(int x, int y)
    {
        if ((x / 3) != 0 && generator.map[(x / 3) - 1][y / 3] == 0)
            instantiateTile(Wall[9], x, y);
        else
            instantiateTile(Wall[3], x, y);
        instantiateTile(Wall[4], x + 1, y);
        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[10], x + 2, y);
        else
            instantiateTile(Wall[5], x + 2, y);
        
        if ((x / 3) != 0 && generator.map[(x / 3) - 1][y / 3] == 0)
            instantiateTile(Wall[9], x, y + 1);
        else
            instantiateTile(Wall[3], x, y + 1);
        instantiateTile(Wall[4], x + 1, y + 1);
        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[10], x + 2, y + 1);
        else
            instantiateTile(Wall[5], x + 2, y + 1);
        
        if ((x / 3) != 0 && generator.map[(x / 3) - 1][y / 3] == 0)
            instantiateTile(Wall[9], x, y + 2);
        else
            instantiateTile(Wall[3], x, y + 2);
        instantiateTile(Wall[4], x + 1, y + 2);
        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[10], x + 2, y + 2);
        else
            instantiateTile(Wall[5], x + 2, y + 2);
    }

    void setWallTopBot(int x, int y)
    {
        instantiateTile(Wall[0], x, y);
        instantiateTile(Wall[1], x + 1, y);
        instantiateTile(Wall[2], x + 2, y); 
        if ((x / 3) != 0 && generator.map[(x / 3) - 1][y / 3] == 0)
            instantiateTile(Wall[9], x, y + 1);
        else
            instantiateTile(Wall[3], x, y + 1);
        instantiateTile(Wall[4], x + 1, y + 1);
        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[10], x + 2, y + 1);
        else
            instantiateTile(Wall[5], x + 2, y + 1);
        if ((x / 3) != 0 && generator.map[(x / 3) - 1][y / 3] == 0)
            instantiateTile(Wall[0], x, y + 2);
        else
            instantiateTile(Wall[6], x, y + 2);

        instantiateTile(Wall[7], x + 1, y + 2);

        if ((x / 3) != generator.map.Length - 1 && generator.map[(x / 3) + 1][y / 3] == 0)
            instantiateTile(Wall[2], x + 2, y + 2);
        else
            instantiateTile(Wall[8], x + 2, y + 2);
    }
    void setFloor(int x,int y)
    {
        instantiateTile(Floor[0], x, y);
        instantiateTile(Floor[1], x + 1, y);
        instantiateTile(Floor[2], x + 2, y);
        instantiateTile(Floor[3], x, y + 1);
        instantiateTile(Floor[4], x + 1, y + 1);
        instantiateTile(Floor[5], x + 2, y + 1);
        instantiateTile(Floor[6], x, y + 2);
        instantiateTile(Floor[7], x + 1, y + 2);
        instantiateTile(Floor[8], x + 2, y + 2);
    }

    void instantiateTile(GameObject tile, int x, int y)
    {
        if (tile != null)
        {
            GameObject temp = Instantiate(tile, new Vector3(x * imageSize, y * imageSize, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = transform;
            map[x][y] = temp;
        }
    }

    void instantiateTile(GameObject tile, int x, int y, Color newColor)
    {
        if (tile != null)
        {
            GameObject temp = Instantiate(tile, new Vector3(x * imageSize, y * imageSize, 0), Quaternion.identity) as GameObject;
            temp.transform.parent = transform;
            temp.GetComponent<SpriteRenderer>().color = newColor;
            map[x][y] = temp;
        }
    }

    void instantiateChest(int x, int y)
    {
        GameObject temp = Instantiate(Chest, new Vector3(x * imageSize, y * imageSize, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = transform;
        temp.GetComponent<Chest>().setPos((x - 1) / 3, (y - 1) / 3);
        map[x][y] = temp;
    }
    void instantiateOpenChest(int x, int y)
    {
        GameObject temp = Instantiate(Chest, new Vector3(x * imageSize, y * imageSize, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = transform;
        temp.GetComponent<Chest>().forceOpen();
        map[x][y] = temp;
    }

    void setCamraBlockers()
    {
        GameObject temp = Instantiate(camraBlocker, new Vector3(((mapXSize * 3) / 2) * imageSize - .25f, -imageSize - .25f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = transform;
        temp.transform.localScale=new Vector3((mapXSize*3)/2,1,1);

        temp = Instantiate(camraBlocker, new Vector3(((mapXSize * 3) / 2) * imageSize - .25f, ((mapYSize * 3) / 2) + 1 * imageSize - .25f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = transform;
        temp.transform.localScale = new Vector3((mapXSize * 3) / 2, 1, 1);

        temp = Instantiate(camraBlocker, new Vector3(-imageSize - .25f, ((mapYSize * 3) / 2) * imageSize - .25f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = transform;
        temp.transform.localScale = new Vector3(1, (mapYSize * 3) / 2, 1);

        temp = Instantiate(camraBlocker, new Vector3(((mapXSize * 3) / 2) + 1 * imageSize - .25f, ((mapYSize * 3) / 2) * imageSize - .25f, 0), Quaternion.identity) as GameObject;
        temp.transform.parent = transform;
        temp.transform.localScale = new Vector3(1, (mapYSize * 3) / 2, 1);
    }
}
