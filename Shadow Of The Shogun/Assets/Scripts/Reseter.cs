﻿using UnityEngine;
using System.Collections;

public class Reseter : MonoBehaviour 
{

	void Start ()
    {
        Time.timeScale = 1;
        if(BattleManager.instance != null)
            BattleManager.instance.Reset();
	}
	
}
