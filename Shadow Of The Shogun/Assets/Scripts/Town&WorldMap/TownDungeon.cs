﻿using UnityEngine;
using System.Collections;

public class TownDungeon : TownSelectable
{
    public GameObject Info;
    public TownSelector selector;
    bool active;
    void Start()
    {

        Info.SetActive(false);
        active = false;
    }

    public override void Run()
    {
        if(BattleManager.instance.party.Wipe())
        {
            active = !active;
            Info.SetActive(active);
            selector.stop = active;
        }
        else
            GameManager.instance.LoadDungeon();
    }
}
