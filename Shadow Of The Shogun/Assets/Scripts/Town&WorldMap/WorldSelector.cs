﻿using UnityEngine;
using System.Collections;

public class WorldSelector : MonoBehaviour 
{
    public GameObject[] Locations;
    public GameObject Selectors;
    public bool stop;

    int index;
	void Start () 
    {
        index = 0;
        Selectors.transform.position = Locations[index].transform.position;
        stop = false;
	}
	
	void Update () 
    {
        if (Time.timeScale == 0)
            return;
        if(!stop)
        {
            if (DirectionalPadHandler.instance.Up)
            {
                index++;
                if (index >= Locations.Length)
                    index = 0;
                Selectors.transform.position = Locations[index].transform.position;
                SoundManager.instance.PlaySingle("click");
            }
            if (DirectionalPadHandler.instance.Down)
            {
                index--;
                if (index < 0)
                    index = Locations.Length - 1;
                Selectors.transform.position = Locations[index].transform.position;
                SoundManager.instance.PlaySingle("click");
            }
        }
        if(InputHandler.instance.AButton || InputHandler.instance.StartButton)
        {
            GameManager.instance.TownName = "TownMap";
            GameManager.instance.LoadTown();
        }
	}
}
