﻿using UnityEngine;
using System.Collections;

public abstract class Item
{
    public int quantity;
    public string name;
    public string icon;
    public virtual void StartSelect() { }
    public virtual void runSelect(BattleCharacter character) { }
    public virtual State nextStateSelect(State state)
    {
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            return new ItemRunState();
        }
        if (InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            return new ItemState();
        }
        return state;
    }
    public virtual void Start() { }
    public virtual void run(BattleCharacter character) { }
    public virtual State nextState(State state)
    {
        if (BattleManager.instance.textBox.isMainActive())
            return state;

        GameObject.Find("ScrollPanel").GetComponent<ListController>().usedItem();
        return new EndTraitCheckState(); 
    }
    public bool useItem()
    {
        quantity--;
        if (quantity == 0)
            return false;
        return true;
    }
    public void addItem(int amount)
    {
        quantity += amount;
        if (quantity > 99)
            quantity = 99;
    }
}
