﻿using UnityEngine;
using System.Collections;

public class LoyaltyMaxItem : Item
{
    bool once;
    public LoyaltyMaxItem()
    {
        quantity = 0;
        name = "Loyalty Max";
        icon = "GoblinPortrait";
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character) 
    {
        if(!once)
        {
            BattleManager.instance.cursor.setState(CursorControl.EnemySelectState);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            TextBox.instance.clear();
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);


            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override void Start() 
    {
        once = false;
    }
    public override void run(BattleCharacter character) 
    {
        if(!once)
        {
            BattleManager.instance.textBox.clear();
            string[] message = {character.name +" used LoyaltyMax on " 
                                   + BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].name
                               ,BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].name+"'s Loyalty is 100."};
            BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].addLoyalty(100);
            BattleManager.instance.textBox.setMessageMain(message);
            once = true;
        }
    }
}
