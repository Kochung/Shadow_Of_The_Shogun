﻿using UnityEngine;
using System.Collections;

public class NoneItem : Item
{
    public NoneItem()
    {
        quantity = -1;
        name = "Back";
        icon = "";
    }
}