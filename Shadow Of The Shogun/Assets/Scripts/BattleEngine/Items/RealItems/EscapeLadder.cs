﻿using UnityEngine;
using System.Collections;

public class EscapeLadder : Item
{
    // runs from battle and escapes to town
    bool once;
    public EscapeLadder()
    {
        quantity = 0;
        name = "Escape Ladder";
        icon = "ladder";
    }
    public override State nextStateSelect(State state)
    {
        BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
        GameObject.Find("ScrollPanel").GetComponent<ListController>().usedItem();
        return new ItemRunState();
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            if(BattleManager.instance.boss)
            {
                string[] message = { character.name + " used Escape Ladder \nCannot run from this battle" };
                TextBox.instance.setMessageMain(message);
            }
            else
            {
                BattleManager.instance.Done = true;
                BattleManager.instance.escapeDungeon = true;
                string[] message = { character.name + " used Escape Ladder \nParty returns to town" };
                TextBox.instance.setMessageMain(message);
            }
        }
    }
}
