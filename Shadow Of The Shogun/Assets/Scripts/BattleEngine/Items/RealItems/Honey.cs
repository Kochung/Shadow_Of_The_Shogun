﻿using UnityEngine;
using System.Collections;

public class Honey : Item
{
    // restores 75% max MP changed to 50%
    bool once;
    public Honey()
    {
        quantity = 0;
        name = "Honey";
        icon = "honey";
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.cursor.setState(CursorControl.PlayerSelectSate);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);

            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            int amount = (int)(target.MaxMp * .50);
            target.gainMp(amount);
            string[] message = new string[1];
            if (BattleManager.instance.cursor.getSelected()[0] == BattleManager.instance.turnOrder.getCurrentTurnIndex())
                message[0] = character.name + " drank honey \n" + target.name + " gained " + amount + "Mp";
            else
                message[0] = character.name + " gave honey to " + target.name + "\n" + target.name + " gained " + amount + "Mp";
            BattleManager.instance.textBox.setMessageMain(message);
            once = true;
        }
    }
}
