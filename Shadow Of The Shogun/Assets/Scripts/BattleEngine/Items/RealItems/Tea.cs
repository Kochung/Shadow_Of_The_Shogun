﻿using UnityEngine;
using System.Collections;

public class Tea : Item
{
    // restores 50% max MP changed to 30%
    bool once;
    public Tea()
    {
        quantity = 0;
        name = "Tea";
        icon = "tea";
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.cursor.setState(CursorControl.PlayerSelectSate);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);

            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            int amount = (int)(target.MaxMp * .30);
            target.gainMp(amount);
            string[] message = new string[1];
            if (BattleManager.instance.cursor.getSelected()[0] == BattleManager.instance.turnOrder.getCurrentTurnIndex())
                message[0] = character.name + " drank tea \n" + target.name + " gained " + amount + " Mp";
            else
                message[0] = character.name + " gave tea to " + target.name + "\n" + target.name + " gained " + amount + " Mp";
            BattleManager.instance.textBox.setMessageMain(message);
            once = true;
        }
    }
}