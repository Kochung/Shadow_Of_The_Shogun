﻿using UnityEngine;
using System.Collections;

public class PaperBomb : Item
{
    //escape battle
    bool once;
    public PaperBomb()
    {
        quantity = 0;
        name = "PaperBomb";
        icon = "paper_bomb";
    }
    public override State nextStateSelect(State state)
    {
        BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
        GameObject.Find("ScrollPanel").GetComponent<ListController>().usedItem();
        return new ItemRunState();
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            if (BattleManager.instance.boss)
            {
                string[] message = { character.name + " used Paper Bomb \nCannot run from this battle" };
                TextBox.instance.setMessageMain(message);
            }
            else
            {
                BattleManager.instance.Done = true;
                string[] message = { character.name + " used Paper Bomb \nParty escapes" };
                TextBox.instance.setMessageMain(message);
            }
        }
    }
}
