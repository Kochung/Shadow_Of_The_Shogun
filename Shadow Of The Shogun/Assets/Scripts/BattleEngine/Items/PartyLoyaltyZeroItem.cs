﻿using UnityEngine;
using System.Collections;

public class PartyLoyaltyZeroItem : Item
{
    bool once;
    public PartyLoyaltyZeroItem()
    {
        quantity = 0;
        name = "Party Zero";
        icon = "antidote";
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleManager.instance.textBox.setMessageRight("Select whole party.");
            BattleManager.instance.cursor.setState(CursorControl.PlayerSelectAllSate);
            once = true;
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
      
        if (!once)
        {
            once = true;
            int lenght = BattleManager.instance.cursor.getSelected().Length;
            int counter = 0;
            BattleCharacter[] newList = new BattleCharacter[lenght];
            for (int i = 0; i < lenght; i++)
            {
                if (BattleManager.instance.cursor.getSelected()[i] != -1)
                {
                    newList[counter] = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[i]];
                    counter++;
                }
            }
            string[] message = new string[counter + 1];
            message[0] = character.name + " uses Party Zero.";
            for (int i = 0; i < counter; i++)
            {
                BattleCharacter target = newList[i]; 
                message[i+1] = target.name + "'s Loyalty is 0.";
                target.addLoyalty(-100);
            }
            TextBox.instance.setMessageMain(message);
        }
    }
}