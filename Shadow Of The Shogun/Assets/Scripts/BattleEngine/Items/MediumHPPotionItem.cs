﻿using UnityEngine;
using System.Collections;

public class MediumHPPotionItem : Item
{
    bool once;
    public MediumHPPotionItem()
    {
        quantity = 0;
        name = "Medium HP Potion";
        icon = "ThiefPortrait";
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.cursor.setState(CursorControl.PlayerSelectSate);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            int amount = (int)(target.MaxHp * .30);
            string[] message = { character.name + " used Medium HP Potion on " + target.name + "\n" + target.name + " gained " + amount + "Hp" };
            target.Heal(amount);
            BattleManager.instance.textBox.setMessageMain(message);
            once = true;
        }
    }
}