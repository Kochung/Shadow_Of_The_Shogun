﻿using UnityEngine;
using System.Collections;

public class TraitorPowder : Item
{
    bool once;
    public TraitorPowder()
    {
        quantity = 0;
        name = "Traitor Powder";
        icon = "CobraPortrait";
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.cursor.setState(CursorControl.PlayerSelectSate);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();

            string[] message = new string[1];
            BattleCharacter betrayer = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            message[0] = character.name + " used Traitor Powder on " + betrayer.name + ".\n";
            betrayer.addLoyalty(-100);
            int result = betrayer.Betray();
            if (result == 1)
            {
                message[0] += betrayer.name + " Betrayed you.";
                TextBox.instance.setMessageMain(message);
            }
            else if (result == 0)
            {
                message[0] += betrayer.name + " Wanted to betray you, but there are no open slots." ;
                TextBox.instance.setMessageMain(message);
            }
            else if (result == -1)
            {
                message[0] += betrayer.name + " Did not betray you.";
                TextBox.instance.setMessageMain(message);
            }
            BattleManager.instance.textBox.setMessageMain(message);
            once = true;
        }
    }
}
