﻿using UnityEngine;
using System.Collections;

public class LargeHPPotionItem : Item
{
    bool once;
    public LargeHPPotionItem()
    {
        quantity = 0;
        name = "Large HP Potion";
        icon = "WhiteMagePortrait";
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.cursor.setState(CursorControl.PlayerSelectSate);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            TextBox.instance.clear();
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);


            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            int amount = (int)(target.MaxHp * .50);
            string[] message = { character.name + " used Large HP Potion on " + target.name + "\n" + target.name + " gained " + amount + "Hp" };
            target.Heal(amount);
            BattleManager.instance.textBox.setMessageMain(message);
            once = true;
        }
    }
}
