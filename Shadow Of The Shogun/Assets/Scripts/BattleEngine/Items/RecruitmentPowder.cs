﻿using UnityEngine;
using System.Collections;

public class RecruitmentPowder : Item
{
    bool once;
    public RecruitmentPowder()
    {
        quantity = 0;
        name = "Recruitment Powder";
        icon = "CrocodilePortrait";
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.cursor.setState(CursorControl.EnemySelectState);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();

            string[] message = new string[1];
            BattleCharacter betrayer = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            message[0] = character.name + " used Recruitment Powder on " + betrayer.name + ".\n";
            betrayer.addLoyalty(100);
            int result = betrayer.Capture();
            if(result == 1)
            {
                message[0] += character.name + " joins your party.";
            } 
            if (result == -1)
            {
                message[0] += character.name + " refused.";
            } 
            if (result == 0)
            {
                message[0] += character.name + " wanted to join but the party is full.";
            }
            BattleManager.instance.textBox.setMessageMain(message);
            once = true;
        }
    }
}
