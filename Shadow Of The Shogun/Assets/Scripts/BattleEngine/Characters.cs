﻿using UnityEngine;
using System.Collections;

public class Characters : MonoBehaviour 
{
    public static Characters instance = null;
    public GameObject[] characters;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    public GameObject get(string name)
    {
        int length = characters.Length;
        for (int i = 0; i < length; i++)
        {
            if(name.Equals(characters[i].name))
                return characters[i];
        }
        return null;
    }
}
