﻿using UnityEngine;
using System.Collections;

public class DirectionalPadHandler : MonoBehaviour 
{
    public static DirectionalPadHandler instance = null;

    public bool Up;
    public bool Down;
    public bool Left;
    public bool Right;

    public int upTimer;
    public int downTimer;
    public int leftTimer;
    public int rightTimer;

    private int upTime;
    private int downTime;
    private int leftTime;
    private int rightTime;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        Up = false;
        Down = false;
        Left = false;
        Right = false;
    }

    void Update()
    {
        int Horizontal = (int)Input.GetAxisRaw("Horizontal");
        int Vertical = (int)Input.GetAxisRaw("Vertical");

        Up = false;
        Down = false;
        Left = false;
        Right = false;

        if (Vertical != 1)
            upTime = 0;
        if (Vertical != -1)
            downTime = 0;
        if (Horizontal != 1)
            leftTime = 0;
        if (Horizontal != -1)

        if (upTime == 0)
        {
            if (Vertical == 1)
            {
                Up = true;
                upTime = upTimer;
            }
        }
        else
            upTime--;


        if (downTime == 0)
        {
            if (Vertical == -1)
            {
                Down = true;
                downTime = downTimer;
            }
        }
        else
            downTime--;


        if (leftTime == 0)
        {
            if (Horizontal == 1)
            {
                Left = true;
                leftTime = leftTimer;
            }
        }
        else
            leftTime--;


        if (rightTime == 0)
        {
            if (Horizontal == -1)
            {
                Right = true;
                rightTime = rightTimer;
            }
        }
        else
            rightTime--;
    }
}
