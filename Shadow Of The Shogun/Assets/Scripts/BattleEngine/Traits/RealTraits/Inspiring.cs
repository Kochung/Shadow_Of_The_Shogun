﻿using UnityEngine;
using System.Collections;

public class Inspiring : Trait
{
    public Inspiring()
    {
        type = BattleStartType;
        name = "Inspiring";
        info = "Gain Speed max";
    }
    public override string run(BattleCharacter character)
    {
        if (BattleManager.instance.random.Next(0, 3) == 0)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (BattleManager.instance.board.playerBord[i][j] != 0)
                    {
                        BattleManager.instance.characters.list[BattleManager.instance.board.playerBord[i][j]].addStatus(new Focused());
                    }
                }
            }
            return character.name + " trait Inspiring activated\nParty is Focused";
        }
        return "";
    }
    public override string enemyRun(BattleCharacter character)
    {
        if (BattleManager.instance.random.Next(0, 3) == 0)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (BattleManager.instance.board.enemyBord[i][j] != 0)
                    {
                        BattleManager.instance.characters.list[BattleManager.instance.board.enemyBord[i][j]].addStatus(new Focused());
                    }
                }
            }
            return character.name + " trait Inspiring activated\nEnemies are now Focused";
        }
        return "";
    }
}

