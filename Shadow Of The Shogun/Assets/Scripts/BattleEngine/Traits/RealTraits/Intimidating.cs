﻿using UnityEngine;
using System.Collections;

public class Intimidating : Trait
{
    public Intimidating()
    {
        type = BattleStartType;
        name = "Intimidating";
        info = "Gain Speed max";
    }
    public override string run(BattleCharacter character)
    {
        if (BattleManager.instance.random.Next(0, 3) == 0)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (BattleManager.instance.board.enemyBord[i][j] != 0)
                    {
                        BattleManager.instance.characters.list[BattleManager.instance.board.enemyBord[i][j]].addStatus(new Fear());
                    }
                }
            }
            return character.name + " trait Intimidating activated\nEnemies are now frightened";
        }
        return "";
    }
    public override string enemyRun(BattleCharacter character)
    {
        if (BattleManager.instance.random.Next(0, 3) == 0)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (BattleManager.instance.board.playerBord[i][j] != 0)
                    {
                        BattleManager.instance.characters.list[BattleManager.instance.board.playerBord[i][j]].addStatus(new Fear());
                    }
                }
            }
            return character.name+" trait Intimidating activated\nParty is now frightened";
        }
        return "";
    }
}

