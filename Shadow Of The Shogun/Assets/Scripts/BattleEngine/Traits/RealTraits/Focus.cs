﻿using UnityEngine;
using System.Collections;

public class Focus : Trait
{
    public Focus()
    {
        type = GuardType;
        name = "Focus";
        info = "Gain Focus";
    }
    public override string run(BattleCharacter character)
    {
        StatusEffect temp = new Focused();
        temp.turnCount = 2;
        character.addStatus(temp);
        return "Focus activates\n" + character.name + " is Focused";
    }
    public override string enemyRun(BattleCharacter character)
    {
        return run(character);
    }
}