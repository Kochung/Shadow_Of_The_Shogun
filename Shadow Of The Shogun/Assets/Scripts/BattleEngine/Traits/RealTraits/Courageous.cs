﻿using UnityEngine;
using System.Collections;

public class Courageous : Trait
{
    public Courageous()
    {
        type = GuardType;
        name = "Courageous";
        info = "Gain Courage";
    }
    public override string run(BattleCharacter character)
    {
        StatusEffect temp = new Courage();
        temp.turnCount = 2;
        character.addStatus(temp);
        return "Courageous activates\n" + character.name + " is now Courageous";
    }
    public override string enemyRun(BattleCharacter character)
    {
        return run(character);
    }
}
