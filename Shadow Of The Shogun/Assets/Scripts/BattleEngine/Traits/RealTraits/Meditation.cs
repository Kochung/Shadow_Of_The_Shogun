﻿using UnityEngine;
using System.Collections;

public class Meditation : Trait
{
    public Meditation()
    {
        type = TurnStartType;
        name = "Meditation";
        info = "Regen Mp";
    }
    public override string run(BattleCharacter character)
    {
        int number = (int)(character.MaxMp * .05);
        character.gainMp(number);
        return "Meditation activates\n" + character.name + " Gained "+ number +" Mp";
    }
    public override string enemyRun(BattleCharacter character)
    {
        return run(character);
    }
}
