﻿using UnityEngine;
using System.Collections;

public class MindControl : Trait
{
    public MindControl()
    {
        type = TurnStartType;
        name = "MindControl";
        info = "Mind Control when only enemy";
    }
    public override string run(BattleCharacter character)
    {
        return "";
    }
    public override string enemyRun(BattleCharacter character)
    {
        int[][] board = BattleManager.instance.board.enemyBord;
        int counter = 0;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (board[i][j] != 0)
                    counter++;
            }
        }
        if(counter == 1)
        {
            int num = -1;
            for (int i = 0; i < 100; i++)
            {
                int x = BattleManager.instance.random.Next(0, 2);
                int y = BattleManager.instance.random.Next(0, 2);
                if (BattleManager.instance.board.playerBord[x][y] != 0)
                {
                    num = BattleManager.instance.board.playerBord[x][y];
                    break;
                }
            }
            if (num != -1)
            {
                BattleCharacter target = BattleManager.instance.characters.list[num];
                target.forceBetray();
                int amount = BattleManager.instance.random.Next(10,20);
                target.addLoyalty(-amount);
                return "Mind Control Activates" + "\n" + target.name + " betrayed you\n" + target.name + " loses "+ amount + " loyalty.";
            }
        }
        return "";
    }
}