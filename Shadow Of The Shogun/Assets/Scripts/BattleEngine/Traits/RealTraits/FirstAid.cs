﻿using UnityEngine;
using System.Collections;

public class FirstAid : Trait
{
    public FirstAid()
    {
        type = TurnStartType;
        name = "FirstAid";
        info = "Regen Hp";
    }
    public override string run(BattleCharacter character)
    {
        int number = (int)(character.MaxHp * .025);
        character.Heal(number);
        return "First Aid activates\n" + character.name + " Gained " + number + " Hp";
    }
    public override string enemyRun(BattleCharacter character)
    {
        return run(character);
    }
}