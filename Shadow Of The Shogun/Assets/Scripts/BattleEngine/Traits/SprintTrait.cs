﻿using UnityEngine;
using System.Collections;

public class SprintTrait : Trait
{
    public SprintTrait()
    {
        type = BattleStartType;
        name = "Sprint";
        info = "Gain Speed max";
    }
    public override string run(BattleCharacter character) 
    {
        if(BattleManager.instance.random.Next(0,3) == 0)
        {
            character.addStatus(new SpeedMax());
            return "Sprint activated " + character.name + " Gains Speed Max.";
        }
        return "";
    }
    public override string enemyRun(BattleCharacter character) 
    {
        return run(character);
    }
}
