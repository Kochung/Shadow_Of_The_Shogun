﻿using UnityEngine;
using System.Collections;

public class NoTrait : Trait
{
    public NoTrait()
    {
        name = "None";
        info = "No trait";
        type = NoType;
    }

}
