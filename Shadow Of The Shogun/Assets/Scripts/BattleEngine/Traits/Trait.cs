﻿using UnityEngine;
using System.Collections;

public abstract class Trait
{
    public const int NoType = -1;
    public const int BattleStartType = 0;
    public const int RoundStartType = 1;
    public const int TurnStartType = 2;
    public const int TurnEndType = 3;
    public const int GuardType = 4;
    public const int AttackType = 5;
    public const int SkillType = 6;
    public const int AttackAndSkillType = 7;
    public const int HitType = 8;
    public int type;
    public string name;
    public string info;
    public virtual string run(BattleCharacter character){ return "";}
    public virtual string enemyRun(BattleCharacter character) { return ""; }
    public bool checkType(int Type)
    {
        if (type == AttackAndSkillType)
        {
            return Type == AttackType || Type == SkillType;
        }
        return type == Type;
    }
}