﻿using UnityEngine;
using System.Collections;

public class MpRegenTrait : Trait
{
    public MpRegenTrait()
    {
        type = GuardType;
        name = "Mp Regen";
        info = "Regen Mp";
    }
    public override string run(BattleCharacter character)
    {
        int number = (int)(character.MaxMp * .05);
        character.gainMp(number);
        return character.name + " Gained " + number + " Mp";
    }
    public override string enemyRun(BattleCharacter character)
    {
        return run(character);
    }
}
