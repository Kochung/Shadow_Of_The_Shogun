﻿using UnityEngine;
using System.Collections;

public class AttackSelectAction : Action
{
    public int type;
    public CursorPoints [][] custom;
    public int customX;
    public int customY;
    public AttackSelectAction()
    {
        type = CursorControl.EnemySelectState;   
    }
    public override void start()
    {
        once = false;
    }
    public override void run()
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            if (type == CursorControl.CustomSelect)
                BattleManager.instance.cursor.setCustom(custom, customX, customY);
            else
                BattleManager.instance.cursor.setState(type);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);

            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override State getNextState(State state)
    {
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            return new PlayerAttackState();
        }
        if (InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            return new FightState();
        }
        return state;
    }
}
