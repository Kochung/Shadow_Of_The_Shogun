﻿using UnityEngine;
using System.Collections;

public class Action
{
    public bool once;
    public Action(){}
    public virtual void start()
    {
        once = false;
    }
    public virtual void run() {}
    public virtual void run(BattleCharacter character){}
    public virtual State getNextState(State state)
    {
        return state;
    }
    public string[] interpret(string [] message,string [] key, string [] value)
    {
        int length = message.Length;
        string[] returnMessage = new string[length];
        for (int i = 0; i < length; i++)
        {
            returnMessage[i] = interpret(message[i], key, value);
        }
        return returnMessage;
    }
    private string interpret(string message,string [] key,string [] value)
    {
        int length = message.Length;
        string returnMessage = "";
        for (int i = 0; i < length; i++)
        {
            if (message[i] != '(')
                returnMessage += message[i];
            else
            {
                string temp = getSection(message, ref i);
                temp = getValue(temp, key, value);
                temp = interpret(temp, key, value);
                returnMessage += temp;
            }
        }
        return returnMessage;
    }
    private string getSection(string message, ref int j)
    {
        int length = message.Length;
        int index = j + 1;
        string returnString = "";
        int openCounter = 1;
        int closeCounter = 0;
        while (index < length && openCounter > closeCounter)
        {
            if (message[index] == '(')
                openCounter++;
            if (message[index] == ')')
                closeCounter++;
            if (openCounter > closeCounter)
                returnString += message[index];
            index++;
        }
        j = index - 1;
        return returnString;
    }
    private string getValue(string message, string [] key,string[] value)
    {
        if(message.StartsWith("C?"))
        {
            return Decision(message, key, value, "Critical");
        }
        if(message.StartsWith("G?"))
        {
            return Decision(message, key, value, "Guard");
        }
        if (message.StartsWith("E?"))
        {
            return Decision(message, key, value, "Enemy");
        }
        for (int i = 0; i < key.Length;i++ )
        {
            if (key[i].Equals(message))
                return value[i];
        }
        return message;
    }
    private string Decision(string message,string[]key,string[]value,string findString)
    {
        string[] lines = splitIf(message);
        for (int i = 0; i < key.Length; i++)
        {
            if (key[i].Equals(findString))
            {
                if (value[i].Equals("True"))
                    return lines[0];
                else
                    return lines[1];
            }
        }
        return message;
    }
    private string[] splitIf(string message)
    {
        string [] returnString = new string[2];
        returnString[0] = "";
        returnString[1] = "";
        int index = -1;
        for (int i = 0; i < message.Length;i++)
        {
            if(index == -1)
            {
                if (message[i] == '?')
                    index = 0;
            }
            else
            {
                if (message[i] == ':')
                    index = 1;
                else
                    returnString[index] += message[i];
            }
        }
        return returnString;
    }
}