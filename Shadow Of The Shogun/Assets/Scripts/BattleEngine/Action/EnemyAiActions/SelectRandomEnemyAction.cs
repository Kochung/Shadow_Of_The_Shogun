﻿using UnityEngine;
using System.Collections;

public class SelectRandomEnemyAction : Action
{
    public bool notSelf;
    public SelectRandomEnemyAction()
    {
        notSelf = false;
    }
    public override void run()
    {
        int[] temp = { 0 };
        for (int i = 0; i < 500; i++)
        {
            int x = BattleManager.instance.random.Next(0, 3);
            int y = BattleManager.instance.random.Next(0, 3);
            temp[0] = -1;
            if (BattleManager.instance.board.enemyBord[x][y] != 0)
            {   
                if(notSelf)
                {
                    if (BattleManager.instance.turnOrder.getCurrentTurnIndex() != BattleManager.instance.board.enemyBord[x][y])
                    {
                        temp[0] = BattleManager.instance.board.enemyBord[x][y];
                        break;
                    }
                }
                else
                {
                    temp[0] = BattleManager.instance.board.enemyBord[x][y];
                    break;
                }
            }
        }
        BattleManager.instance.cursor.setState(CursorControl.ForceState);
        BattleManager.instance.cursor.setForceSelect(temp);
    }
}
