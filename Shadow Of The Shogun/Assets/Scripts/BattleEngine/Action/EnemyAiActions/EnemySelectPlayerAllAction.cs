﻿using UnityEngine;
using System.Collections;

public class EnemySelectPlayerAllAction : Action
{
    public EnemySelectPlayerAllAction()
    {

    }
    public override void run()
    {
        int[] temp = {0,0,0,0};

        temp[0] = BattleManager.instance.board.playerBord[1][0];
        temp[1] = BattleManager.instance.board.playerBord[0][0];
        temp[2] = BattleManager.instance.board.playerBord[1][1];
        temp[3] = BattleManager.instance.board.playerBord[0][1];

        for (int i = 0; i < 4; i++)
            if (temp[i] == 0)
                temp[i] = -1;

        BattleManager.instance.cursor.setState(CursorControl.ForceState);
        BattleManager.instance.cursor.setForceSelect(temp);
    }
}

