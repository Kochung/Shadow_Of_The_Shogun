﻿using UnityEngine;
using System.Collections;

public class SelectPlayerFrontAction: Action
{
    public SelectPlayerFrontAction()
    {

    }
    public override void run()
    {
        int[] temp = {0, 0};

        if (BattleManager.instance.board.playerBord[1][0] != 0)
            temp[0] = BattleManager.instance.board.playerBord[1][0];
        else
            temp[0] = BattleManager.instance.board.playerBord[0][0];
        if (BattleManager.instance.board.playerBord[1][1] != 0)
            temp[1] = BattleManager.instance.board.playerBord[1][1];
        else
            temp[1] = BattleManager.instance.board.playerBord[0][1];
        BattleManager.instance.cursor.setState(CursorControl.ForceState);
        BattleManager.instance.cursor.setForceSelect(temp);
    }
}

