﻿using UnityEngine;
using System.Collections;

public class EnemySelectEnemyAllAction : Action
{
    public EnemySelectEnemyAllAction()
    {

    }
    public override void run()
    {
        int[] temp = {0,0,0,0,0,0,0,0,0};

        temp[0] = BattleManager.instance.board.enemyBord[0][0];
        temp[1] = BattleManager.instance.board.enemyBord[1][0];
        temp[2] = BattleManager.instance.board.enemyBord[2][0];
        temp[3] = BattleManager.instance.board.enemyBord[0][1];
        temp[4] = BattleManager.instance.board.enemyBord[1][1];
        temp[5] = BattleManager.instance.board.enemyBord[2][1];
        temp[6] = BattleManager.instance.board.enemyBord[0][2];
        temp[7] = BattleManager.instance.board.enemyBord[1][2];
        temp[8] = BattleManager.instance.board.enemyBord[2][2];

        for (int i = 0; i < 9; i++)
            if (temp[i] == 0)
                temp[i] = -1;

            BattleManager.instance.cursor.setState(CursorControl.ForceState);
        BattleManager.instance.cursor.setForceSelect(temp);
    }
}
