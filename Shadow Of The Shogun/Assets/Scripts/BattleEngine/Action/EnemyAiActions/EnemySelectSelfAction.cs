﻿using UnityEngine;
using System.Collections;

public class EnemySelectSelfAction : Action
{
    public EnemySelectSelfAction()
    {

    }
    public override void run()
    {
        int[] temp = { 0 };
        temp[0] = BattleManager.instance.turnOrder.getCurrentTurnIndex();

        BattleManager.instance.cursor.setState(CursorControl.ForceState);
        BattleManager.instance.cursor.setForceSelect(temp);
    }
}
