﻿using UnityEngine;
using System.Collections;

public class SelectRandomPlayerAction : Action
{ 
    public SelectRandomPlayerAction()
    {

    }
    public override void run()
    {
        int[] temp = { 0 };
        for (int i = 0; i < 100; i++)
        {
            int x = BattleManager.instance.random.Next(0, 2);
            int y = BattleManager.instance.random.Next(0, 2);
            if (BattleManager.instance.board.playerBord[x][y] != 0)
            {
                temp[0] = BattleManager.instance.board.playerBord[x][y];
                break;
            }
        }
        BattleManager.instance.cursor.setState(CursorControl.ForceState);
        BattleManager.instance.cursor.setForceSelect(temp);
    }
}
