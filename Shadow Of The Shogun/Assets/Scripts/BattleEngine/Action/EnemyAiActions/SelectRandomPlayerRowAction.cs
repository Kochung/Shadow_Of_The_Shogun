﻿using UnityEngine;
using System.Collections;

public class SelectRandomPlayerRowAction: Action
{
    public SelectRandomPlayerRowAction()
    {

    }
    public override void run()
    {
        int[] temp = { 0, 0 };
        for (int i = 0; i < 100; i++)
        {
            int y = BattleManager.instance.random.Next(0, 2);
            if (BattleManager.instance.board.playerBord[0][y] != 0 || BattleManager.instance.board.playerBord[1][y] != 0)
            {
                temp[0] = BattleManager.instance.board.playerBord[0][y];
                temp[1] = BattleManager.instance.board.playerBord[1][y];
                break;
            }
        }
        BattleManager.instance.cursor.setState(CursorControl.ForceState);
        BattleManager.instance.cursor.setForceSelect(temp);
    }
}
