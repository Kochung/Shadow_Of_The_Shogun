﻿using UnityEngine;
using System.Collections;

public class AttackAction : Action
{
    public bool physical;
    public int baseDamage;
    public string soundEffect;
    public AttackAction()
    {
        physical = true;
        baseDamage = 80;
    }
    public override void start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            if (target.immune)
            {
                string[] immuneMessage = { target.name + " is immune to attacks" };
                TextBox.instance.setMessageMain(immuneMessage);
                return;
            }
            bool crit = character.getCrit();
            double damage = target.getDamage(character, baseDamage, physical, crit);
            character.runStatusSkill(ref damage, physical, ref crit, ref target.guarding);
            target.Damage((int)damage);
            string[] message = new string[2];
            message[0] = character.name + " Attacks " + target.name + ".";
            message[1] = "";
            if (crit)
                message[1] += "Critical Hit\n";
            if (target.guarding)
                message[1] += target.name + " Guards\n";
            message[1] += target.name + " takes " + (int)damage + ".";

            BattleManager.instance.textBox.setMessageMain(message);
            EffectList.instance.Instantiate(1, target.character.transform.position);
            SoundManager.instance.PlaySingle(soundEffect);
        }
    }
    public override State getNextState(State state)
    {
        if (TextBox.instance.isMainActive())
            return state;
        return new AttackTraitCheckState();
    }
}

