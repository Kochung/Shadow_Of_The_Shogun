﻿using UnityEngine;
using System.Collections;

public class SingleTargetSkillAction : Action
{
    public bool physical;
    public int MpUsed;
    public int MpGain;
    public int LoyaltyGain;
    public int HpUsed;
    public int HpGain;
    public int baseDamage;
    public bool healDamage;
	public int effectIndex;
    public StatusEffect[] statusEffectList;
    public string[] message;
    public string soundEffect;
    public SingleTargetSkillAction()
    {
        physical = true;
        MpUsed = 0;
        baseDamage = 0;
		effectIndex = 0;
        statusEffectList = null;
    }
    public override void start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            character.useMp(MpUsed);
            character.gainMp(MpGain);
            character.NoHitDamage(HpUsed);
            character.Heal(HpGain);
            character.addLoyalty(LoyaltyGain);
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]]; 
            if (target.immune)
            {
                string[] immuneMessage = { target.name + " is immune to attacks" };
                TextBox.instance.setMessageMain(immuneMessage);
                return;
            }
            if(statusEffectList != null)
            {
                int length = statusEffectList.Length;
                for (int i = 0; i < length; i++)
                {
                    target.addStatus(statusEffectList[i].clone(character));
                }
            }

            bool crit = character.getCrit();
            double damage = target.getDamage(character, baseDamage, physical, crit);
            character.runStatusSkill(ref damage, physical, ref crit, ref target.guarding);
            target.Damage((int)damage);
            if (healDamage)
                character.Heal((int)damage / 2);
            string[] key = { "D", "N", "TN", "Critical", "Guard","Enemy" };
            string[] value = { "" + ((int)damage), character.name, target.name, "" + crit, "" + target.guarding, "" + BattleManager.instance.characters.isEnemy[character.index] };
            TextBox.instance.setMessageMain(interpret(message,key,value));
            if(BattleManager.instance.characters.isEnemy[character.index])
            {
                GameObject effect = EffectList.instance.Instantiate(effectIndex, target.character.transform.position);
                effect.transform.localScale = new Vector3(-effect.transform.localScale.x, effect.transform.localScale.y, effect.transform.localScale.z);
            }
            else
                EffectList.instance.Instantiate(effectIndex, target.character.transform.position);
            SoundManager.instance.PlaySingle(soundEffect);
        }
    }
    public override State getNextState(State state)
    {
        if (TextBox.instance.isMainActive())
            return state;
        return new SkillTraitCheckState();
    }
}
