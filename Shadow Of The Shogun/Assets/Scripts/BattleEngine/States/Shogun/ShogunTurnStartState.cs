﻿using UnityEngine;
using System.Collections;

public class ShogunTurnStartState : State
{
     bool once;
     public ShogunTurnStartState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            TextBox.instance.setMessageLeft("Talk\nItem\nInfo\nRun");
            BattleManager.instance.cursor.setState(CursorControl.Select4State);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            int number = BattleManager.instance.cursor.getSelected()[0];
            switch(number)
            {
                case 0:
                    TextBox.instance.setMessageRight("Talk");
                    break;
                case 1:
                    TextBox.instance.setMessageRight("Item");
                    break;
                case 2:
                    TextBox.instance.setMessageRight("Info");
                    break;
                case 3:
                    TextBox.instance.setMessageRight("Run");
                    break;
            }
        }
    }

    public State getNextState()
    {
        if(InputHandler.instance.AButton)
        {
            int number = BattleManager.instance.cursor.getSelected()[0];
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            switch (number)
            {
                case 0:
                     return new ShogunTalkState();
                case 1:
                    return new ItemState();
                case 2:
                    return new InfoState();
                case 3:
                    return new RunState();
            }
        }
        return this;
    }
}
