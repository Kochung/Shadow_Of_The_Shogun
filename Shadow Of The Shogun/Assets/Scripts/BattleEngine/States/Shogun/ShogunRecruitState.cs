﻿using UnityEngine;
using System.Collections;

public class ShogunRecruitState: State
{
    bool once;
    public ShogunRecruitState()
    {
        once = false;
    }
    public void Run()
    {
        if(!once)
        {
            BattleCharacter trator = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            if (trator.cannotRecruit)
            {
                string[] message = { "Shogun cannot recruit " + trator.name };
                TextBox.instance.setMessageMain(message);
            }
            else
            {
                int result = trator.Capture();
                if (result == 1)
                {
                    string[] message = { "Shogun Recruits " + trator.name };
                    TextBox.instance.setMessageMain(message);
                }
                if (result == -1)
                {
                    string[] message = { trator.name + " refused" };
                    TextBox.instance.setMessageMain(message);
                }
                if (result == 0)
                {
                    string[] message = { trator.name + " wanted to join but the party is full" };
                    TextBox.instance.setMessageMain(message);
                }
            }
            once = true;
        }
    }

    public State getNextState()
    {
        if(TextBox.instance.isMainActive())
            return this;
        return new EndTraitCheckState();
    }
}