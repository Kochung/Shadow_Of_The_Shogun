﻿using UnityEngine;
using System.Collections;

public class ShogunBribeSelectState : State
{
    bool once;
    public ShogunBribeSelectState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            TextBox.instance.setMessageLeft("        Bribe\n10 Gold\n25 Gold\n50 Gold");
            BattleManager.instance.cursor.setState(CursorControl.Select3State);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            int number = BattleManager.instance.cursor.getSelected()[0];
            switch (number)
            {
                case 1:
                    TextBox.instance.setMessageRight("Small 10 gold bribe"); 
                    break;
                case 2:
                    TextBox.instance.setMessageRight("Medium 25 gold bribe");
                    break;
                case 3:
                    TextBox.instance.setMessageRight("Large 50 gold bribe");
                    break;
            }
        }
    }

    public State getNextState()
    {
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            StateInformation.instance.shogunBribeAmount = BattleManager.instance.cursor.getSelected()[0];
            if (StateInformation.instance.shogunBribeAmount == 1 && BattleManager.instance.gold.amount < 10)
                return new ShogunNotEnoughGoldState();
            if (StateInformation.instance.shogunBribeAmount == 2 && BattleManager.instance.gold.amount < 25)
                return new ShogunNotEnoughGoldState();
            if (StateInformation.instance.shogunBribeAmount == 3 && BattleManager.instance.gold.amount < 50)
                return new ShogunNotEnoughGoldState();
            if (StateInformation.instance.shogunActionSelect == 3)
                return new ShogunSelectPartyState();
            return new ShogunSelectEnemyState();
        }
        if (InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            if (StateInformation.instance.shogunActionSelect == 3)
                return new ShogunTalkPartyState();
            return new ShogunTalkEnemyState();
        }
        return this;
    }
}