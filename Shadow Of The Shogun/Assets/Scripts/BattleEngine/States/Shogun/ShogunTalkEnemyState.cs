﻿using UnityEngine;
using System.Collections;

public class ShogunTalkEnemyState : State
{
    bool once;
    public ShogunTalkEnemyState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            TextBox.instance.setMessageLeft("   Talk With Enemy\nNegotiate\nRecruit\nBribe");
            BattleManager.instance.cursor.setState(CursorControl.Select3State);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            int number = BattleManager.instance.cursor.getSelected()[0];
            switch (number)
            {
                case 1:
                    TextBox.instance.setMessageRight("Negotiate");
                    break;
                case 2:
                    TextBox.instance.setMessageRight("Recruit");
                    break;
                case 3:
                    TextBox.instance.setMessageRight("Bribe");
                    break;
            }
        }
    }

    public State getNextState()
    {
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            StateInformation.instance.shogunActionSelect = BattleManager.instance.cursor.getSelected()[0];
            if (BattleManager.instance.cursor.getSelected()[0] == 3)
            {
                StateInformation.instance.shogunActionSelect++;
                return new ShogunBribeSelectState();
            }
            return new ShogunSelectEnemyState();
        }
        if (InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            return new ShogunTalkState();
        }
        return this;
    }
}

