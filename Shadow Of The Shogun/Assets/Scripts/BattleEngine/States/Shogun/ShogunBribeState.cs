﻿using UnityEngine;
using System.Collections;

public class ShogunBribeState : State
{
    bool once;
    public ShogunBribeState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            string bribeSize = "10"; 
            int number = 0;
            if(StateInformation.instance.shogunBribeAmount == 1)
            {
                number = BattleManager.instance.random.Next(5, 10);
                BattleManager.instance.gold.add(-10);
                BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].addLoyalty(number);
            }
            else if (StateInformation.instance.shogunBribeAmount == 2)
            {
                bribeSize = "25";
                number = BattleManager.instance.random.Next(20, 30);
                BattleManager.instance.gold.add(-25);
                BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].addLoyalty(number);
            }
            else if (StateInformation.instance.shogunBribeAmount == 3)
            {
                bribeSize = "50";
                number = BattleManager.instance.random.Next(35, 50);
                BattleManager.instance.gold.add(-50);
                BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].addLoyalty(number);
            }
            string[] message = { "Shogun gives " + BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].name + " " + bribeSize +" Gold.\n"+
                               BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].name+" gains " +number+" Loyalty"};
            TextBox.instance.setMessageMain(message);
            SoundManager.instance.PlaySingle("bribe.wav");
            once = true;
        }
    }

    public State getNextState()
    {
        if (TextBox.instance.isMainActive())
            return this;
        return new EndTraitCheckState();
    }
}
