﻿using UnityEngine;
using System.Collections;

public class ShogunTalkState : State
{
    bool once;
    public ShogunTalkState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            TextBox.instance.setMessageLeft("        Talk\nWith Party\nWith Enemy\nPass");
            BattleManager.instance.cursor.setState(CursorControl.Select3State);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            int number = BattleManager.instance.cursor.getSelected()[0];
            switch (number)
            {
                case 1:
                    TextBox.instance.setMessageRight("Talk with party");
                    break;
                case 2:
                    TextBox.instance.setMessageRight("Talk with enemy");
                    break;
                case 3:
                    TextBox.instance.setMessageRight("Pass the turn");
                    break;
            }
        }
    }

    public State getNextState()
    {
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            if (BattleManager.instance.cursor.getSelected()[0] == 1)
                return new ShogunTalkPartyState(); 
            if (BattleManager.instance.cursor.getSelected()[0] == 2)
                return new ShogunTalkEnemyState();
            return new ShogunPassState();
        }
        if (InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            return new ShogunTurnStartState();
        }
        return this;
    }
}
