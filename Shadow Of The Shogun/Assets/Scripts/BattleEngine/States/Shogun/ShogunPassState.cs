﻿using UnityEngine;
using System.Collections;

public class ShogunPassState : State
{
    bool once;
    public ShogunPassState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            string[] message = { BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].name + " passes his turn." };
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }

    public State getNextState()
    {
        if (BattleManager.instance.textBox.isMainActive())
            return this;
        return new EndTraitCheckState();
    }
}
