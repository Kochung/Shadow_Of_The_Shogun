﻿using UnityEngine;
using System.Collections;

public class ShogunNotEnoughGoldState : State
{
    bool once;
    public ShogunNotEnoughGoldState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            string[] message = { "Not Enough Gold."};
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }

    public State getNextState()
    {
        if (TextBox.instance.isMainActive())
            return this;
        return new ShogunBribeSelectState();
    }
}
