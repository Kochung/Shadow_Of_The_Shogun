﻿using UnityEngine;
using System.Collections;

public class ShogunSelectPartyState : State
{
    bool once;
    public ShogunSelectPartyState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            TextBox.instance.setMessageLeft("Stuff");
            BattleManager.instance.cursor.setState(CursorControl.PlayerSelectSate);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left,-1,right,15);
            
            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }

    public State getNextState()
    {
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            switch (StateInformation.instance.shogunActionSelect)
            {
                case 1:
                    return new ShogunMotivateState();
                case 2:
                    return new ShogunBerateState();
                case 3:
                    return new ShogunBribeState();
            }
        }
        if(InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            if (StateInformation.instance.shogunActionSelect == 3)
                return new ShogunBribeSelectState();
            return new ShogunTalkPartyState();
        }
        return this;
    }
}

