﻿using UnityEngine;
using System.Collections;

public class ShogunMotivateState : State
{
    bool once;
    public ShogunMotivateState()
    {
        once = false;
    }
    public void Run()
    {
        if(!once)
        {
            int number = BattleManager.instance.random.Next(1, 10);
            BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].addLoyalty(number);
            string[] message = { "Shogun Motivates " + BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].name + "\n" +
                                 BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].name + " gains " + number +" Loyalty"};
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }

    public State getNextState()
    {
        if(TextBox.instance.isMainActive())
            return this;
        return new EndTraitCheckState();
    }
}
