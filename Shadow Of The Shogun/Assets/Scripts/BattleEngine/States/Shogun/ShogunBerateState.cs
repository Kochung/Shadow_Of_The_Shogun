﻿using UnityEngine;
using System.Collections;

public class ShogunBerateState : State
{
    bool once;
    public ShogunBerateState()
    {
        once = false;
    }
    public void Run()
    {
        if(!once)
        {
            BattleCharacter character = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            character.addLoyalty(-5);
            character.addStatus(new GuaranteeCrit());
            int result = character.Betray();
            string[] message = new string[1];
            if(result == 1)
            {
                message[0] = "Shogun Berates " + character.name + "\n" +
                             character.name + " Betrayed You.\n"+
                             character.name + " Gains Guarantee Crit" + "\n" +
                             character.name + " loses 5 Loyalty";
                TextBox.instance.setMessageMain(message);
            }
            else if(result == 0)
            {
                message[0] = "Shogun Berates " + character.name + "\n" +
                             character.name + " Wanted to betray you, but there are no open slots.\n"+
                             character.name + " Gains Guarantee Crit" + "\n" +
                             character.name + " loses 5 Loyalty";
                TextBox.instance.setMessageMain(message);
            }
            else
            {
                message[0] = "Shogun Berates " + character.name + "\n" +
                character.name + " Gains Guarantee Crit" + "\n" +
                character.name + " losses 5 Loyalty";
            }
                               
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }

    public State getNextState()
    {
        if(TextBox.instance.isMainActive())
            return this;
        return new EndTraitCheckState();
    }
}

