﻿using UnityEngine;
using System.Collections;

public class ShogunTalkPartyState : State
{
    bool once;
    public ShogunTalkPartyState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            TextBox.instance.setMessageLeft("   Talk With Party\nMotivate\nBerate\nBribe");
            BattleManager.instance.cursor.setState(CursorControl.Select3State);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            int number = BattleManager.instance.cursor.getSelected()[0];
            switch (number)
            {
                case 1:
                    TextBox.instance.setMessageRight("Motivate");
                    break;
                case 2:
                    TextBox.instance.setMessageRight("Berate");
                    break;
                case 3:
                    TextBox.instance.setMessageRight("Bribe");
                    break;
            }
        }
    }

    public State getNextState()
    {
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            StateInformation.instance.shogunActionSelect = BattleManager.instance.cursor.getSelected()[0];
            if (BattleManager.instance.cursor.getSelected()[0] == 3)
                return new ShogunBribeSelectState();
            return new ShogunSelectPartyState();
        }
        if (InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            return new ShogunTalkState();
        }
        return this;
    }
}
