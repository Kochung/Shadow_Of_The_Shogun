﻿using UnityEngine;
using System.Collections;

public class TurnOverState : State
{
    bool once;
    public TurnOverState()
    {
        once = false;
    }
    public void Run()
    {
        if(!once)
        {
       /*     if(StateInformation.instance.suicide)
            {
                string[] message = { StateInformation.instance.suicideName + " Turn is over." };
                TextBox.instance.setMessageMain(message);
                StateInformation.instance.suicide = false;
            }
            else
            {
                string[] message = { BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].name + " Turn is over."};
       //         TextBox.instance.setMessageMain(message);
            }*/
            once = true;
        }
    }

    public State getNextState()
    {
        if (BattleManager.instance.party.Wipe())
            return new LoseState();
        if (BattleManager.instance.enemyParty.Wipe())
            return new WinState();
        if(TextBox.instance.isMainActive())
            return this;
        if (BattleManager.instance.turnOrder.nextTurn())
            return new TurnOrderState();
        return new NextTurnState();
    }
}
