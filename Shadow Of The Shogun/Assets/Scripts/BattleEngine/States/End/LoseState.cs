﻿using UnityEngine;
using System.Collections;

public class LoseState : State
{
    bool once;
    public LoseState()
    {
        once = false;
    }
    public void Run()
    {
        if(!once)
        {
            TextBox.instance.TurnBoxDeactivate();
            string[] message = { "Your party wiped", "You retreat back to town" };
            TextBox.instance.setMessageMain(message);
            BattleManager.instance.Done = true;
            once = true;
        }
    }

    public State getNextState()
    {
        return this;
    }
}
