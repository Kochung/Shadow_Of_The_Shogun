﻿using UnityEngine;
using System.Collections;

public class WinState : State
{
    bool once;
    public WinState()
    {
        once = false;
    }
    public void Run()
    {
        if(!once)
        {
            TextBox.instance.TurnBoxDeactivate();
            string[] message = { "All enemies have been eliminated", "You obtained " + BattleManager.instance.winGold + " gold", "Party Recovers 25% of HP and Mp" };
            BattleManager.instance.party.afterBattleHeal();
            TextBox.instance.setMessageMain(message);
            BattleManager.instance.Done = true;
            once = true;
        }
    }

    public State getNextState()
    {
        return this;
    }
}
