﻿using UnityEngine;
using System.Collections;

public class DiedState : State
{
    bool once;
    int deadCount;
    BattleCharacter[] dead;
    int counter;
    public DiedState()
    {
        BattleManager.instance.healthBars.update();
        once = false;
        int length = BattleManager.instance.characters.getLength() + 1;
        dead = new BattleCharacter[length];
        deadCount = 0;
        for(int i=0;i<length;i++)
        {
            if((!BattleManager.instance.characters.list[i].job.Equals("")) && BattleManager.instance.characters.list[i].Hp == 0)
            {
                dead[deadCount] = BattleManager.instance.characters.list[i];
                deadCount++;
            }
        }
    }
    public void Run()
    {
        if(!once)
        {
            if(deadCount != 0 )
            {
                string[] message = { dead[counter].name + " Died." };
                TextBox.instance.setMessageMain(message);
                dead[counter].die();
                once = true;
            }
        }
    }

    public State getNextState()
    {
        if(TextBox.instance.isMainActive())
            return this;
        counter++;
        if (counter >= deadCount)
            return new TurnOverState();
        once = false;
        return this;
    }
}
