﻿using UnityEngine;
using System.Collections;

public class StatusEffectDropState : State
{
    bool once;
    public StatusEffectDropState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            once = true;
            BattleCharacter character = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()];
            int length = character.statusEffects.Length;
            int counter = 0;
            for (int i = 0; i < length; i++)
            {
                character.statusEffects[i].turnOver();
                if (character.statusEffects[i].turnCount == 0)
                    counter++;
            }
            if(counter!=0)
            {
                string[] message = new string[counter];
                int index = 0;
                for (int i = 0; i < length; i++)
                {
                    if (character.statusEffects[i].turnCount == 0)
                    {
                        message[index] = character.statusEffects[i].name + " has dropped off.";
                        index++;
                        if (index == counter)
                            break;
                    }
                }
                BattleManager.instance.textBox.setMessageMain(message);
            }
            character.cleanStatusEffects();
        }
    }

    public State getNextState()
    {
        if (BattleManager.instance.textBox.isMainActive())
            return this;
        return new DiedState();
    }
}
