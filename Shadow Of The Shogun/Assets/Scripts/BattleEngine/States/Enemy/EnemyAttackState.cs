﻿using UnityEngine;
using System.Collections;

public class EnemyAttackState : State
{
    bool once;
    BattleCharacter character;
    public EnemyAttackState()
    {
        character = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()];
        character.enemyAttackAction.start();
    }
    public void Run()
    {
        character.enemyAttackAction.run(character);
    }

    public State getNextState()
    {
        return character.enemyAttackAction.getNextState(this);
    }
}
