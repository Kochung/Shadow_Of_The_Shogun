﻿using UnityEngine;
using System.Collections;

public class EnemySkillState : State
{
    bool once;
    bool canUseSkill;
    public EnemySkillState()
    {
        once = false;
        canUseSkill = false;
    }
    public void Run()
    {
        if (!once)
        {
            once = true;
            BattleCharacter character = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()];
            for (int i = 0; i < 100; i++)
            {
                int number = BattleManager.instance.random.Next(0, character.skills.Length);
                if(character.skills[number] !=0)
                {
                    if (character.Mp >= SkillList.instance.list[character.skills[number]].MpUsed)
                    {
                        SkillList.instance.list[character.skills[number]].setEnhance(false);
                        SkillList.instance.list[character.skills[number]].runEnemySelect();
                        SkillList.instance.list[character.skills[number]].Start();
                        SkillList.instance.list[character.skills[number]].run(character);
                        canUseSkill = true;
                    }
                    else if(BattleManager.instance.boss)
                    {
                        int number2 = BattleManager.instance.random.Next(0,2);
                        if(number == 1)
                        {
                            SkillList.instance.list[character.skills[number]].setEnhance(true);
                            SkillList.instance.list[character.skills[number]].runEnemySelect();
                            SkillList.instance.list[character.skills[number]].Start();
                            SkillList.instance.list[character.skills[number]].run(character);
                            canUseSkill = true;
                        }
                        else
                            canUseSkill = false;

                    }
                    else
                        canUseSkill = false;
                    break;
                }
            }
        }
    }

    public State getNextState()
    {
        if (!canUseSkill)
            return new EnemyAttackState();
        if (TextBox.instance.isMainActive())
            return this;
        BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
        return new SkillTraitCheckState();
    }
}
