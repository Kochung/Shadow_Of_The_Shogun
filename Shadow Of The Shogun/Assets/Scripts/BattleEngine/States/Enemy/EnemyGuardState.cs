﻿using UnityEngine;
using System.Collections;

public class EnemyGuardState : State
{
    bool once;
    public EnemyGuardState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            BattleCharacter character = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()];
            string[] message = { character.name + " Guards" };
            character.guarding = true;
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }

    public State getNextState()
    {
        if (TextBox.instance.isMainActive())
            return this;
        return new GuardTraitCheckState();
    }
}
