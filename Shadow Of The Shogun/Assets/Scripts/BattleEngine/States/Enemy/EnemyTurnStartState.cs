﻿using UnityEngine;
using System.Collections;

public class EnemyTurnStartState : State
{
    bool once;
    int goTo;
    public EnemyTurnStartState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            int number = BattleManager.instance.random.Next(0, 10);
            if ( number < 2)
            {
                goTo = 1;
                if (BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].noAttack)
                    goTo = 2;
            }
            else if(number < 6)
            {
                goTo = 2;
            }
            else
            {
                goTo = 0;
                if (BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].noAttack)
                    goTo = 2;
            }
            once = true;
        }
    }

    public State getNextState()
    {
        switch(goTo)
        {
            case 0:
                return new EnemyAttackState();
            case 1:
                return new EnemyGuardState();
            case 2:
                return new EnemySkillState();
        }
        return new EndTraitCheckState();
    }
}
