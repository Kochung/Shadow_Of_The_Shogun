﻿using UnityEngine;
using System.Collections;

public class HitTraitCheckState : State
{
    bool once;
    public HitTraitCheckState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            once = true;
            int length = BattleManager.instance.cursor.getSelected().Length;
            string[] message = new string[BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].traits.Length * length];
            int count = 0;
            for(int j = 0; j<length;j++)
            {
                if (BattleManager.instance.cursor.getSelected()[j] != -1 && BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[j]].Hit)
                {
                    BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[j]].Hit = false;
                    int[] list = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[j]].traits;
                    for (int i = 0; i < list.Length; i++)
                    {
                        if (TraitList.instance.list[list[i]].checkType(Trait.HitType))
                        {
                            bool enemyTurn = BattleManager.instance.characters.isEnemy[BattleManager.instance.cursor.getSelected()[j]];
                            string tempMessage = "";
                            if (enemyTurn)
                                tempMessage = TraitList.instance.list[list[i]].enemyRun(BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[j]]);
                            else
                                tempMessage = TraitList.instance.list[list[i]].run(BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[j]]);
                            if (!tempMessage.Equals(""))
                            {
                                message[count] = tempMessage;
                                count++;
                            }
                        }
                    }
                }
            }
            if (count != 0)
            {
                string[] trueMessage = new string[count];
                for (int i = 0; i < count; i++)
                {
                    trueMessage[i] = message[i];
                }
                BattleManager.instance.textBox.setMessageMain(trueMessage);
            }
        }
    }

    public State getNextState()
    {
        if (BattleManager.instance.textBox.isMainActive())
            return this;
        return new EndTraitCheckState();
    }
}
