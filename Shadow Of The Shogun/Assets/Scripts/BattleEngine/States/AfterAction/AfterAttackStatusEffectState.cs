﻿using UnityEngine;
using System.Collections;

public class AfterAttackStatusEffectState : State
{
    bool once;
    public AfterAttackStatusEffectState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            once = true;
            StatusEffect[] list = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].statusEffects;
            string[] message = new string[list.Length];
            int count = 0;
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].checkType(StatusEffect.AffterAttackType))
                {
                    string tempMessage = list[i].run(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
                    if (!tempMessage.Equals(""))
                    {
                        message[count] = tempMessage;
                        count++;
                    }
                }
            }
            if (count != 0)
            {
                string[] trueMessage = new string[count];
                for (int i = 0; i < count; i++)
                {
                    trueMessage[i] = message[i];
                }
                BattleManager.instance.textBox.setMessageMain(trueMessage);
            }
        }
    }

    public State getNextState()
    {
        if (BattleManager.instance.textBox.isMainActive())
            return this;
        return new HitTraitCheckState();
    }
}
