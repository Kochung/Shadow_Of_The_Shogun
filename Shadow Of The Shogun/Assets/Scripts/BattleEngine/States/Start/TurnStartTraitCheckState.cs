﻿using UnityEngine;
using System.Collections;

public class TurnStartTraitCheckState : State
{
    bool once;
    public TurnStartTraitCheckState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            once = true;
            int [] list = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].traits;
            string[] message = new string[list.Length];
            int count = 0;
            for (int i = 0; i < list.Length; i++)
            {
                if (TraitList.instance.getCurrentCharicterTrait(list[i]).checkType(Trait.TurnStartType))
                {
                    bool enemyTurn = BattleManager.instance.characters.isEnemy[BattleManager.instance.turnOrder.getCurrentTurnIndex()];
                    string tempMessage = "";
                    if (enemyTurn)
                        tempMessage = TraitList.instance.getCurrentCharicterTrait(list[i]).enemyRun(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
                    else
                        tempMessage = TraitList.instance.getCurrentCharicterTrait(list[i]).run(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
                    if (!tempMessage.Equals(""))
                    {
                        message[count] = tempMessage;
                        count++;
                    }
                }
            }
            if(count != 0)
            {
                string[] trueMessage = new string[count];
                for (int i = 0; i < count; i++)
                {
                    trueMessage[i] = message[i];
                }
                BattleManager.instance.textBox.setMessageMain(trueMessage);
            }
        }
    }

    public State getNextState()
    {
        if (BattleManager.instance.textBox.isMainActive())
            return this;
        return new TurnStartStatusEffectState();
    }
}
