﻿using UnityEngine;
using System.Collections;

public class StartState : State
{
    bool once;
    public StartState()
    {
        once = false;
    }
    public void Run()
    {
        if(!once)
        {
            CharacterList temp = BattleManager.instance.characters;
            for (int i = 0; i < temp.list.Length; i++)
            {
                temp.list[i].guarding = false;
            }
            TextBox.instance.clear();
            TextBox.instance.TurnBoxDeactivate();
            GameObject.Find("ScrollPanel").GetComponent<ListController>().Deactivate();
            string[] message = { "Enemies Appear" };
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }

    public State getNextState()
    {
        if(TextBox.instance.isMainActive())
            return this;
        return new PlayerBetrayState();
    }
}
