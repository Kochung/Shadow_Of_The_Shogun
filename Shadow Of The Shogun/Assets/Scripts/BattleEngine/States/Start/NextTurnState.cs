﻿using UnityEngine;
using System.Collections;

public class NextTurnState : State
{
    bool once;
    public NextTurnState()
    {
        once = false;
        BattleManager.instance.cursor.selectSelfOn();
    }
    public void Run()
    {
        if(!once)
        {
            BattleCharacter character = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()];
            BattleManager.instance.cursor.selectSelfOn();
            if (BattleManager.instance.turnOrder.getCurrentTurnIndex() == 1)
                TextBox.instance.TurnBoxDeactivate();
            else
                TextBox.instance.setTurnMessage(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
            if(character.guarding)
            {
                string[] message = { character.name + " is no longer guarding" };
                character.guarding = false;
                TextBox.instance.setMessageMain(message);
            }
            once = true;
        }
    }

    public State getNextState()
    {
        if(TextBox.instance.isMainActive())
            return this;
        return new TurnStartTraitCheckState();
    }
}
