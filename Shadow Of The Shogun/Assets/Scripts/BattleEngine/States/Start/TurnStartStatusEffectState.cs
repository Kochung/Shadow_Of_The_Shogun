﻿using UnityEngine;
using System.Collections;

public class TurnStartStatusEffectState : State
{
    bool once;
    bool skip;
    public TurnStartStatusEffectState()
    {
        once = false;
        skip = false;
    }
    public void Run()
    {
        if (!once)
        {
            once = true;
            StatusEffect[] list = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].statusEffects;
            string[] message = new string[list.Length];
            int count = 0;
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].checkType(StatusEffect.TurnStartType))
                {
                    string tempMessage = list[i].run(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
                    if (!tempMessage.Equals(""))
                    {
                        if (tempMessage.StartsWith("Stun causes"))
                            skip = true;
                        message[count] = tempMessage;
                        count++;
                    }
                }
            }
            if (count != 0)
            {
                string[] trueMessage = new string[count];
                for (int i = 0; i < count; i++)
                {
                    trueMessage[i] = message[i];
                }
                BattleManager.instance.textBox.setMessageMain(trueMessage);
            }
        }
    }

    public State getNextState()
    {
        if (BattleManager.instance.textBox.isMainActive())
            return this;
        if (BattleManager.instance.party.Wipe())
            return new LoseState();
        if (BattleManager.instance.enemyParty.Wipe())
            return new WinState();
        bool enemyTurn = BattleManager.instance.characters.isEnemy[BattleManager.instance.turnOrder.getCurrentTurnIndex()];
        if (skip)
            return new EndTraitCheckState();
        if (BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].Hp == 0)
            return new DiedState();
        if (BattleManager.instance.turnOrder.getCurrentTurnIndex() == 1)
            return new ShogunTurnStartState();
        else if (enemyTurn)
            return new EnemyTurnStartState();
        else
            return new PlayerTurnStartState();
    }
}

