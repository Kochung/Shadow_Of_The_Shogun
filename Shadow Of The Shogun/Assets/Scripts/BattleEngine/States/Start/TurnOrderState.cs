﻿using UnityEngine;
using System.Collections;

public class TurnOrderState : State
{
    public void Run()
    {
        BattleManager.instance.turnOrder.Order(BattleManager.instance.characters);
        BattleManager.instance.turnOrder.displayPortraits(BattleManager.instance.characters);
    }

    public State getNextState()
    {
        return new NextTurnState();
    }
}
