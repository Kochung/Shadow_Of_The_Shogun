﻿using UnityEngine;
using System.Collections;

public class PlayerBetrayState : State
{
    bool once;
    int count;
    public PlayerBetrayState()
    {
        once = false;
        count = 0;
    }
    public void Run()
    {
        if(!once)
        {
            int num = BattleManager.instance.board.playerBord[count / 2][count % 2];
            while (num == 0 && count != 3)
            {
                count++;
                num = BattleManager.instance.board.playerBord[count / 2][count % 2];
            }
            if (num != 0)
            {
                BattleCharacter betrayer = BattleManager.instance.characters.list[num];
                int result = betrayer.Betray();
                if(result == 1)
                {
                    string[] message = { betrayer.name + " Betrayed You." };
                    TextBox.instance.setMessageMain(message);
                }
                if(result == 0)
                {
                    string[] message = {  betrayer.name + " Wanted to betray you, but there are no open slots." };
                    TextBox.instance.setMessageMain(message);
                }
            }
            once = true;
        }
    }

    public State getNextState()
    {
        if(TextBox.instance.isMainActive())
            return this;
        count++;
        once = false;
        if (count == 4)
            return new StartTraitCheckState();
        return this;
    }
}
