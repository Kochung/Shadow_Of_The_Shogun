﻿using UnityEngine;
using System.Collections;

public class StartTraitCheckState : State
{
    bool once;
    public StartTraitCheckState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            once = true;
            int length = BattleManager.instance.characters.list.Length; 
            int[] list = BattleManager.instance.characters.list[0].traits;
            string[] message = new string[list.Length * length];
            int count = 0;
            for (int j = 0; j < length; j++)
            {
                if(!BattleManager.instance.characters.list[j].name.Equals(""))
                {
                    list = BattleManager.instance.characters.list[j].traits;
                    for (int i = 0; i < list.Length; i++)
                    {
                        if (TraitList.instance.list[list[i]].checkType(Trait.BattleStartType))
                        {
                            bool enemyTurn = BattleManager.instance.characters.isEnemy[j];
                            string tempMessage = "";
                            if (enemyTurn)
                                tempMessage = TraitList.instance.list[list[i]].enemyRun(BattleManager.instance.characters.list[j]);
                            else
                                tempMessage = TraitList.instance.list[list[i]].run(BattleManager.instance.characters.list[j]);
                            if (!tempMessage.Equals(""))
                            {
                                message[count] = tempMessage;
                                count++;
                            }
                        }
                    }
                }
            }
            if(count != 0)
            {
                string[] trueMessage = new string[count];
                for (int i = 0; i < count; i++)
                {
                    trueMessage[i] = message[i];
                }
                BattleManager.instance.textBox.setMessageMain(trueMessage);
            }
        }
    }
    public State getNextState()
    {
        if (BattleManager.instance.textBox.isMainActive())
            return this;
        return new TurnOrderState();
    }
}

