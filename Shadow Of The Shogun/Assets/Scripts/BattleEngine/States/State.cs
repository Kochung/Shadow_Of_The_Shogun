﻿using UnityEngine;
using System.Collections;

public interface State
{
    void Run();
    State getNextState();
}
