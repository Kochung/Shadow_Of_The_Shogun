﻿using UnityEngine;
using System.Collections;

public class FightState : State
{
    bool once;
    public FightState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            TextBox.instance.setMessageLeft("        Fight\nAttack\nSkill\nGuard");
            BattleManager.instance.cursor.setState(CursorControl.Select3State);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            int number = BattleManager.instance.cursor.getSelected()[0];
            switch (number)
            {
                case 1:
                    TextBox.instance.setMessageRight("Attack");
                    break;
                case 2:
                    TextBox.instance.setMessageRight("Skill");
                    break;
                case 3:
                    TextBox.instance.setMessageRight("Guard");
                    break;
            }
        }
    }

    public State getNextState()
    {
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            if (BattleManager.instance.cursor.getSelected()[0] == 1)
                return new PlayerAttackSelectState(); 
            if (BattleManager.instance.cursor.getSelected()[0] == 2)
                return new SelectSkillState();
            return new GuardState();
        }
        if (InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            return new PlayerTurnStartState();
        }
        return this;
    }
}
