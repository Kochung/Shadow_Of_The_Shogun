﻿using UnityEngine;
using System.Collections;

public class ItemState  : State
{
    bool once;
    ListController items;
    public ItemState()
    {
        items = GameObject.Find("ScrollPanel").GetComponent<ListController>();
        once = false;
    }
    public void Run()
    {
        if(!once)
        {
            BattleManager.instance.cursor.selectSelfOff();
            items.Activate();
            once = true;
        }
    }

    public State getNextState()
    {
        if(items.isActive())
            return this;
        if (items.getItem().GetComponent<ItemController>().label.text.Equals("Back"))
        {
            if (BattleManager.instance.turnOrder.getCurrentTurnIndex() == 1)
                return new ShogunTurnStartState();
            return new PlayerTurnStartState();
        }
        return new ItemSelectTargetState();
    }
}
