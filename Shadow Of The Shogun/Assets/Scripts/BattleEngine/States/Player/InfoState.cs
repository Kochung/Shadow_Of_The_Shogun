﻿using UnityEngine;
using System.Collections;

public class InfoState : State
{
    bool once;
    public InfoState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            BattleManager.instance.cursor.setState(CursorControl.PlayerAndEnemySelectState);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            TextBox.instance.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left,-1,right,15);

            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }

    public State getNextState()
    {
        if(InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            if (BattleManager.instance.turnOrder.getCurrentTurnIndex() == 1)
                return new ShogunTurnStartState();
            return new PlayerTurnStartState();
        }
        return this;
    }
}
