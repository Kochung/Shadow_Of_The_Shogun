﻿using UnityEngine;
using System.Collections;

public class SelectSkillState : State
{
    bool once;
    public SelectSkillState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            if (!BattleManager.instance.cursor.isState(CursorControl.Select6State))
                BattleManager.instance.cursor.setState(CursorControl.Select6State);
            TextBox.instance.clear();
            TextBox.instance.setMessageLeft("       Skill");
            string leftMessageLeft = "";
            string leftMessageRight = "";
            for (int i = 0; i < 3; i++)
            {
                leftMessageLeft += "\n" + SkillList.instance.getCurrentCharicterSkill(i).name;
            }
            for (int i = 3; i < 6; i++)
            {
                leftMessageRight += "\n" + SkillList.instance.getCurrentCharicterSkill(i).name;
            }

            TextBox.instance.setMessageLeft(leftMessageLeft, leftMessageRight);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            TextBox.instance.setMessageRight("");
            int number = BattleManager.instance.cursor.getSelected()[0];
            string message = SkillList.instance.getCurrentCharicterSkill(number - 1).name + "\n" + SkillList.instance.getCurrentCharicterSkill(number - 1).info;
            switch (number)
            {
                case 1:
                    TextBox.instance.setMessageRight(message, 15);
                    break;
                case 2:
                    TextBox.instance.setMessageRight(message, 15);
                    break;
                case 3:
                    TextBox.instance.setMessageRight(message, 15);
                    break;
                case 4:
                    TextBox.instance.setMessageRight(message, 15);
                    break;
                case 5:
                    TextBox.instance.setMessageRight(message, 15);
                    break;
                case 6:
                    TextBox.instance.setMessageRight(message, 15);
                    break;
            }
        }
    }

    public State getNextState()
    {
        if (InputHandler.instance.StartButton)
        {
            return new SelectEnhancedSkillState();
        }
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
			StateInformation.instance.skillSelected();
            if (enoughMp())
                return new SkillSelectTargetState();
            else
                return new NotEnoughMpState();
        }
        if (InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            return new FightState();
        }
        return this;
    }
    public bool enoughMp()
    {
        return BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].Mp >= SkillList.instance.getCurrentCharicterSkill(StateInformation.instance.skillIndex).MpUsed;
    }
}
