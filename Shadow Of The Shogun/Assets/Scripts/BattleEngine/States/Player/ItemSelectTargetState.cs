﻿using UnityEngine;
using System.Collections;

public class ItemSelectTargetState : State
{
    Item item;
    ItemController controller;
    ListController listController;
    public ItemSelectTargetState()
    {
        listController  = GameObject.Find("ScrollPanel").GetComponent<ListController>();
        controller = listController.getItem().GetComponent<ItemController>();
        item = BattleManager.instance.bag.getItem(controller.getName());
        item.StartSelect();
    }
    public void Run()
    {
        item.runSelect(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
    }

    public State getNextState()
    {
        return item.nextStateSelect(this);
    }
}
