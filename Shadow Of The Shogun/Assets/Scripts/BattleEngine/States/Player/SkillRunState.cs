﻿using UnityEngine;
using System.Collections;

public class SkillRunState : State
{
	Skill skill;
	public SkillRunState()
	{
        skill = SkillList.instance.getCurrentCharicterSkill(StateInformation.instance.skillIndex);
        skill.setEnhance(false);
        skill.Start();
    }
    public void Run()
    {
		skill.run(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
    }

    public State getNextState()
    {
        return skill.nextState(this);
    }
}
