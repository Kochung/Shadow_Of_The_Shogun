﻿using UnityEngine;
using System.Collections;

public class PlayerAttackSelectState : State
{
    bool once;
    public PlayerAttackSelectState()
    {
        BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].attackSelectAction.start();
    }
    public void Run()
    {
        BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].attackSelectAction.run();
    }

    public State getNextState()
    {
        return BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].attackSelectAction.getNextState(this);
    }
}
