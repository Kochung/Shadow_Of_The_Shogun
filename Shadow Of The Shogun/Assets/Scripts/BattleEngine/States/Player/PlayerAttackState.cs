﻿using UnityEngine;
using System.Collections;

public class PlayerAttackState : State
{
    bool once;
    BattleCharacter character;
    public PlayerAttackState()
    {
        character = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()];
        character.attackAction.start();
    }
    public void Run()
    {
        character.attackAction.run(character);
    }

    public State getNextState()
    {
        return character.attackAction.getNextState(this);
    }
}
