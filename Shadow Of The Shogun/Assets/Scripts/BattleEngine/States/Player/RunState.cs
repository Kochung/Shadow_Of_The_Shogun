﻿using UnityEngine;
using System.Collections;

public class RunState  : State
{
    bool once;
    bool ran;
    public RunState()
    {
        once = false;
        ran = false;
    }
    public void Run()
    {
        if(!once)
        {
            if(BattleManager.instance.boss)
            {
                string[] message = { "Cannot run from this battle" };
                TextBox.instance.setMessageMain(message);

            }
            else
            {
                int num = BattleManager.instance.random.Next(0, 256);
                if (num < 104)
                {
                    ran = true;
                    BattleManager.instance.Done = true;
                    string[] message = { "Run Successful" };
                    TextBox.instance.setMessageMain(message);
                }
                else
                {
                    string[] message = { "Run Failed" };
                    TextBox.instance.setMessageMain(message);
                }
            }
            once = true;
        }
    }

    public State getNextState()
    {
        if(TextBox.instance.isMainActive())
            return this;
        if (ran)
            return this;
        return new EndTraitCheckState();
    }
}
