﻿using UnityEngine;
using System.Collections;

public class EnhancedSkillSelectTargetState : State
{
    
	Skill skill;
    public EnhancedSkillSelectTargetState()
	{
        skill = SkillList.instance.getCurrentCharicterSkill(StateInformation.instance.skillIndex);
        skill.setEnhance(true);
        skill.StartSelect();
	}
    public void Run()
    {
		skill.runSelect(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
    }

    public State getNextState()
    {
        return skill.getNextStateSelect(this);
    }
}
