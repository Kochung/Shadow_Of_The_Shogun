﻿using UnityEngine;
using System.Collections;

public class EnhancedSkillRunState : State
{
	Skill skill;
    public EnhancedSkillRunState()
    {
        skill = SkillList.instance.getCurrentCharicterSkill(StateInformation.instance.skillIndex);
        skill.setEnhance(true);
        skill.Start();
    }
    public void Run()
    {
		skill.run(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
    }

    public State getNextState()
    {
        return skill.nextState(this);
    }
}
