﻿using UnityEngine;
using System.Collections;

public class NotEnoughMpState : State
{
    bool once;
    public NotEnoughMpState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            string[] message = { "Not Enough Mp." };
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }

    public State getNextState()
    {
        if (TextBox.instance.isMainActive())
            return this;
        return new SelectSkillState();
    }
}


