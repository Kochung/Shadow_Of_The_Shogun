﻿using UnityEngine;
using System.Collections;

public class SkillSelectTargetState : State
{
    
	Skill skill;
    public SkillSelectTargetState()
	{
        skill = SkillList.instance.getCurrentCharicterSkill(StateInformation.instance.skillIndex);
        skill.setEnhance(false);
        skill.StartSelect();
	}
    public void Run()
    {
		skill.runSelect(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
    }

    public State getNextState()
    {
        return skill.getNextStateSelect(this);
    }
}
