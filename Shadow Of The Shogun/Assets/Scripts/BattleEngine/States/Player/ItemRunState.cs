﻿using UnityEngine;
using System.Collections;

public class ItemRunState : State
{
    Item item;
    ItemController controller;
    ListController listController;
    bool once;
    public ItemRunState()
    {
        listController = GameObject.Find("ScrollPanel").GetComponent<ListController>();
        controller = listController.getItem().GetComponent<ItemController>();
        item = BattleManager.instance.bag.getItem(controller.getName());
        item.Start();
        once = false;
    }
    public void Run()
    {
        item.run(BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()]);
        if(!once)
        {
            SoundManager.instance.PlaySingle("item_toss.wav");
            once = true;
        }
    }

    public State getNextState()
    {
        return item.nextState(this);
    }
}