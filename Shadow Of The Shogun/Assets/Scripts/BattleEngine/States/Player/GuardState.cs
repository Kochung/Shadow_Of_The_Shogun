﻿using UnityEngine;
using System.Collections;

public class GuardState : State
{
    bool once;
    public GuardState()
    {
        once = false;
    }
    public void Run()
    {
        if(!once)
        {
            BattleCharacter character = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()];
            string[] message = { character.name+" Guards" };
            character.guarding = true;
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }

    public State getNextState()
    {
        if(TextBox.instance.isMainActive())
            return this;
        return new GuardTraitCheckState();
    }
}

