﻿using UnityEngine;
using System.Collections;

public class PlayerTurnStartState : State
{
    bool once;
    public PlayerTurnStartState()
    {
        once = false;
    }
    public void Run()
    {
        if (!once)
        {
            TextBox.instance.clear();
            TextBox.instance.setMessageLeft("Fight\nItem\nInfo\nRun");
            BattleManager.instance.cursor.setState(CursorControl.Select4State);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);
        }
    }

    public State getNextState()
    {
        if(InputHandler.instance.AButton)
        {
            int number = BattleManager.instance.cursor.getSelected()[0];
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            switch (number)
            {
                case 0:
                    return new FightState();
                case 1:
                    return new ItemState();
                case 2:
                    return new InfoState();
                case 3:
                    return new RunState();
            }
        }
        return this;
    }
}
