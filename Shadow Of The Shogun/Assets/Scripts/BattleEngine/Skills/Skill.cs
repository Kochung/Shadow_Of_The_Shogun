﻿using UnityEngine;
using System.Collections;

public abstract class Skill
{
    public int MpUsed;
    public string name;
    public string info;
    public Action selectorAction;
    public Action action;
    public Action enhancedAction;
    public Action enhancedSelectorAction;
    public Action enemySelectAction;

    protected bool enhance;
    public void setEnhance(bool value)
    {
        enhance = value;
    }
	public virtual void StartSelect()
    {
        if (enhance)
            enhancedSelectorAction.start();
        else
            selectorAction.start();
    }
    public virtual void runSelect(BattleCharacter character)
    {
        if (enhance)
            enhancedSelectorAction.run();
        else
            selectorAction.run();
    }
    public virtual State getNextStateSelect(State state)
    {
        if (enhance && enhancedSelectorAction != null)
            return enhancedSelectorAction.getNextState(state);
        else if (selectorAction != null)
            return selectorAction.getNextState(state);
        if (InputHandler.instance.AButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState);
            if (enhance)
                return new EnhancedSkillRunState();
            return new SkillRunState();
        }
        if (InputHandler.instance.BButton)
        {
            BattleManager.instance.cursor.setState(CursorControl.NotActiveState); 
            if (enhance)
                return new SelectEnhancedSkillState();
            return new SelectSkillState();
        }
        return state;
    }
	public virtual void Start()
    {
        if (enhance)
            enhancedAction.start();
        else
            action.start();
    }
    public virtual void run(BattleCharacter character)
    {
        if (enhance)
            enhancedAction.run(character);
        else
            action.run(character);
    }
    public virtual State nextState(State state)
    {
        if (enhance && enhancedAction != null)
            return enhancedAction.getNextState(state);
        else if (action != null)
            return action.getNextState(state);
        if (TextBox.instance.isMainActive())
            return state;
        return new SkillTraitCheckState();
    }
	public bool canUse(BattleCharacter character)
	{
		return character.Mp >= MpUsed;
	}
    public void runEnemySelect()
    {
        enemySelectAction.run();
    }
}
