﻿using UnityEngine;
using System.Collections;
using System;

public class HitAllSkill : Skill
{
    bool once;

	public HitAllSkill()
	{
		MpUsed = 10;
        name = "Hit All";
        info = "Hits all enamies";
        once = true;
	}
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if(!once)
        {
            BattleManager.instance.textBox.clear();
            BattleManager.instance.textBox.setMessageRight("Select All Enemies");
            BattleManager.instance.cursor.setState(CursorControl.EnemySelectAllState);
            once = true;
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            int lenght = BattleManager.instance.cursor.getSelected().Length;
            int counter = 0;
            BattleCharacter[] newList = new BattleCharacter[lenght];
            for (int i = 0; i < lenght; i++)
            {
                if (BattleManager.instance.cursor.getSelected()[i] != -1)
                {
                    newList[counter] = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[i]];
                    counter++;
                }
            }
            string[] message = new string[counter + 1];
            message[0] = character.name + " Attacks All Enemies.";
            for (int i = 0; i < counter; i++)
            {
                BattleCharacter target = newList[i];
                bool crit = character.getCrit();
                double damage = target.getDamage(character, 1000, true, crit);
                character.runStatusSkill(ref damage, true, ref crit, ref target.guarding);
                newList[i].Damage((int)damage);
                message[i + 1] = "";
                if (crit)
                    message[i + 1] += "Critical Hit\n";
                if (target.guarding)
                    message[i + 1] += target.name + " Guards\n";
                message[i + 1] += target.name + " takes " + (int)damage + ".";
            }
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }
}
