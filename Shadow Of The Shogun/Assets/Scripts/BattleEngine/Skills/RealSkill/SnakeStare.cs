﻿using UnityEngine;
using System.Collections;

public class SnakeStare : Skill
{
    bool once;

    public SnakeStare()
    {
        MpUsed = 20;
        name = "SnakeStare";
        info = "20Mp\nApply stun to target";
        once = true;
        enemySelectAction = new SelectRandomPlayerAction();
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleManager.instance.cursor.setState(CursorControl.EnemySelectState);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);

            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    character.addLoyalty(10);
                else
                    character.addLoyalty(-10);
                character.gainMp(MpUsed);
            }
            else
                character.useMp(MpUsed);
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            if (target.immune)
            {
                string[] immuneMessage = { target.name + " is immune to attacks" };
                TextBox.instance.setMessageMain(immuneMessage);
                return;
            }

            bool crit = character.getCrit();
            double damage = target.getDamage(character, 60, true, crit);
            character.runStatusSkill(ref damage, true, ref crit, ref target.guarding);
            target.addStatus(new Stun());
            target.Damage((int)damage);
            if (enhance)
                character.Heal((int)damage / 2);
            string[] message = new string[3];
            message[0] = character.name + " uses Snake Stare.";
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    message[0] += "\n" + character.name + " gains 10 loyalty.";
                else
                    message[0] += "\n" + character.name + " loses 10 loyalty.";
            }
            if (crit)
                message[1] += "Critical Hit\n";
            if (target.guarding)
                message[1] += target.name + " Guards\n";
            message[1] += target.name + " takes " + (int)damage + ".";
            message[2] += target.name + " is Stunned.";
            TextBox.instance.setMessageMain(message);
            EffectList.instance.Instantiate(2, target.character.transform.position);
            SoundManager.instance.PlaySingle("stare.wav");
        }
    }
}
