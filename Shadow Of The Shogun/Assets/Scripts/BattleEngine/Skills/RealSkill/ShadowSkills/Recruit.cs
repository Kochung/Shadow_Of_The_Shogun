﻿using UnityEngine;
using System.Collections;

public class Recruit : Skill
{
    bool once;

    public Recruit()
    {
        MpUsed = 0;
        name = "Recruit";
        info = "";
        once = true;
        enemySelectAction = new SelectRandomPlayerAction();

        selectorAction = new SkillSelectEnemyAction();
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            
            string[] message = new string[1];
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            int amount = BattleManager.instance.random.Next(5, 20);
            target.addLoyalty(-amount);
            int result = target.Betray();
            message[0] = character.name + " tries to recruit " + target.name;
            message[0] = target.name + " loses " + amount + " loyalty.";
            if (result == 1)
            {
                message[0] += target.name + " Betrayed You.";
                TextBox.instance.setMessageMain(message);
            }
            if (result == -1)
            {
                message[0] += target.name + " refused";
                TextBox.instance.setMessageMain(message);
            }
            if (result == 0)
            {
                message[0] += target.name + " Wanted to betray you, but there are no open slots.";
                TextBox.instance.setMessageMain(message);
            }
            EffectList.instance.Instantiate(7, target.character.transform.position);
            SoundManager.instance.PlaySingle("bribe.wav");
            TextBox.instance.setMessageMain(message);
        }
    }
}
