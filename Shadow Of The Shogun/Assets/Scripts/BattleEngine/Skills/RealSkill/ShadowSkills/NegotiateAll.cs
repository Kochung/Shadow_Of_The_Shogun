﻿using UnityEngine;
using System.Collections;

public class NegotiateAll : Skill
{
    bool once;

    public NegotiateAll()
    {
        MpUsed = 0;
        name = "NegotiateAll";
        info = "";
        once = true;
        enemySelectAction = new EnemySelectPlayerAllAction();
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            int lenght = BattleManager.instance.cursor.getSelected().Length;
            int counter = 0;
            BattleCharacter[] newList = new BattleCharacter[lenght];
            for (int i = 0; i < lenght; i++)
            {
                if (BattleManager.instance.cursor.getSelected()[i] != -1)
                {
                    if (!BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[i]].job.Equals(""))
                    {
                        newList[counter] = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[i]];
                        counter++;
                    }
                }
            }
            string[] message = new string[counter + 1];
            message[0] = character.name + " Negotiates with your party.";
            for (int i = 0; i < counter; i++)
            {
                BattleCharacter target = newList[i];
                int amount = BattleManager.instance.random.Next(5, 20);
                target.addLoyalty(-amount);
                message[i + 1] += target.name + " loses " + amount + " loyalty.";
                EffectList.instance.Instantiate(7, target.character.transform.position);
                SoundManager.instance.PlaySingle("bribe.wav");
            }
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }
}

