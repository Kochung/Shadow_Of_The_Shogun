﻿using UnityEngine;
using System.Collections;

public class Berate : Skill
{
    bool once;

    public Berate()
    {
        MpUsed = 0;
        name = "Berate";
        info = "";
        once = true;
        SelectRandomEnemyAction temp = new SelectRandomEnemyAction();
        temp.notSelf = true;
        enemySelectAction = temp;
        selectorAction = new SelectRandomPlayerAction();
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            string[] message = new string[1];
            int selected = BattleManager.instance.cursor.getSelected()[0];
            if (selected == -1)
            {
                message[0] = character.name + " has no one to berate";
            }
            else
            {
                BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
                message[0] = character.name + " Berates " + target.name;
                message[0] += "\n" + target.name + "  Gains Guarantee Crit.";
                message[0] += "\n" + target.name + "  Gains 5 Loyalty.";
                target.addStatus(new GuaranteeCrit());
                target.addLoyalty(5);
                EffectList.instance.Instantiate(7, target.character.transform.position);
                SoundManager.instance.PlaySingle("bribe.wav");
            }
            TextBox.instance.setMessageMain(message);
        }
    }
}