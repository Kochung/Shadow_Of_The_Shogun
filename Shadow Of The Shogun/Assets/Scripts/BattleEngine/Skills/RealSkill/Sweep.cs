﻿using UnityEngine;
using System.Collections;

public class Sweep : Skill
{
    bool once;

    public Sweep()
    {
        MpUsed = 20;
        name = "Sweep";
        info = "20Mp\nHits all enemies in the front";
        once = true;
        enemySelectAction = new SelectPlayerFrontAction();
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleManager.instance.textBox.setMessageRight("Confirm Targets");
            CursorControl cursor = BattleManager.instance.cursor;
            CursorPoints[][] points = new CursorPoints[1][];
            points[0] = new CursorPoints[1];
            int[] r = { -1, -1, -1 };
            int index = 0;
            if (cursor.enemyIndex(6) != -1)
            {
                r[index] = cursor.enemyIndex(6);
                index++;
            }
            else if (cursor.enemyIndex(7) != -1)
            {
                r[index] = cursor.enemyIndex(7);
                index++;
            }
            else if (cursor.enemyIndex(8) != -1)
            {
                r[index] = cursor.enemyIndex(8);
                index++;
            }
            if (cursor.enemyIndex(3) != -1)
            {
                r[index] = cursor.enemyIndex(3);
                index++;
            }
            else if (cursor.enemyIndex(4) != -1)
            {
                r[index] = cursor.enemyIndex(4);
                index++;
            }
            else if (cursor.enemyIndex(5) != -1)
            {
                r[index] = cursor.enemyIndex(5);
                index++;
            }
            if (cursor.enemyIndex(0) != -1)
                r[index] = cursor.enemyIndex(0);
            else if (cursor.enemyIndex(1) != -1)
                r[index] = cursor.enemyIndex(1);
            else if (cursor.enemyIndex(2) != -1)
                r[index] = cursor.enemyIndex(2);
            points[0][0] = new CursorPoints(r);
            cursor.setCustom(points, 0, 0);
            once = true;
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    character.addLoyalty(10);
                else
                    character.addLoyalty(-10);
                character.gainMp(MpUsed);
            }
            else
                character.useMp(MpUsed);
            int lenght = BattleManager.instance.cursor.getSelected().Length;
            int counter = 0;
            BattleCharacter[] newList = new BattleCharacter[lenght];
            for (int i = 0; i < lenght; i++)
            {
                if (BattleManager.instance.cursor.getSelected()[i] != -1)
                {
                    if (!BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[i]].job.Equals(""))
                    {
                        newList[counter] = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[i]];
                        counter++;
                    }
                }
            }
            string[] message = new string[counter + 1];
            message[0] = character.name + " uses Sweep.";
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    message[0] += "\n" + character.name + " gains 10 loyalty.";
                else
                    message[0] += "\n" + character.name + " loses 10 loyalty.";
            }
            for (int i = 0; i < counter; i++)
            {

                BattleCharacter target = newList[i];
                if (target.immune)
                {
                    message[i + 1] = target.name + " is immune to attacks";
                }
                else
                {
                    bool crit = character.getCrit();
                    double damage = target.getDamage(character, 90, true, crit);
                    character.runStatusSkill(ref damage, true, ref crit, ref target.guarding);
                    target.Damage((int)damage);
                    if (enhance)
                        character.Heal((int)damage / 2);
                    message[i + 1] = "";
                    if (crit)
                        message[i + 1] += "Critical Hit\n";
                    if (target.guarding)
                        message[i + 1] += target.name + " Guards\n";
                    message[i + 1] += target.name + " takes " + (int)damage + "."; 
                    if (BattleManager.instance.characters.isEnemy[character.index])
                    {
                        GameObject effect = EffectList.instance.Instantiate(4, target.character.transform.position);
                        effect.transform.localScale = new Vector3(-effect.transform.localScale.x, effect.transform.localScale.y, effect.transform.localScale.z);
                    }
                    else
					    EffectList.instance.Instantiate(4, target.character.transform.position);

                    SoundManager.instance.PlaySingle("slash.wav");
                }
            }
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }
}
