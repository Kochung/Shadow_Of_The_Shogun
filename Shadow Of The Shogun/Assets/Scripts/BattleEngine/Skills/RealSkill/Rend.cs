﻿using UnityEngine;
using System.Collections;

public class Rend : Skill
{
    bool once;

    public Rend()
    {
        MpUsed = 15;
        name = "Rend";
        info = "15Mp\nApply bleed to target";
        once = true;
        enemySelectAction = new SelectRandomPlayerAction();
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleManager.instance.cursor.setState(CursorControl.EnemySelectState);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);

            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    character.addLoyalty(10);
                else
                    character.addLoyalty(-10);
                character.gainMp(MpUsed);
            }
            else
                character.useMp(MpUsed);
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            if (target.immune)
            {
                string[] immuneMessage = { target.name + " is immune to attacks" };
                TextBox.instance.setMessageMain(immuneMessage);
                return;
            }
            
            bool physical = true;
            string[] message = new string[3];
            int baseDamage = 80;
            message[2] = target.name + " starts to bleed.";
            target.addStatus(new Bleed(character));
            bool crit = character.getCrit();
            double damage = target.getDamage(character, baseDamage, physical, crit);
            character.runStatusSkill(ref damage, physical, ref crit, ref target.guarding);
            target.Damage((int)damage);
            if (enhance)
                character.Heal((int)damage / 2);
            message[0] = character.name + " uses Rend.";
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    message[0] += "\n" + character.name + " gains 10 loyalty.";
                else
                    message[0] += "\n" + character.name + " loses 10 loyalty.";
            }
            message[1] = "";
            if (crit)
                message[1] += "Critical Hit\n";
            if (target.guarding)
                message[1] += target.name + " Guards\n";
            message[1] += target.name + " takes " + (int)damage + ".";
            TextBox.instance.setMessageMain(message);
            if (BattleManager.instance.characters.isEnemy[character.index])
            {
                GameObject effect = EffectList.instance.Instantiate(3, target.character.transform.position);
                effect.transform.localScale = new Vector3(-effect.transform.localScale.x, effect.transform.localScale.y, effect.transform.localScale.z);

                GameObject effect2 = EffectList.instance.Instantiate(6, target.character.transform.position);
                effect2.transform.localScale = new Vector3(-effect2.transform.localScale.x, effect2.transform.localScale.y, effect2.transform.localScale.z);
            
            }
            else
            {
                EffectList.instance.Instantiate(3, target.character.transform.position);
                EffectList.instance.Instantiate(6, target.character.transform.position);
            }

            SoundManager.instance.PlaySingle("slash.wav");
        }
    }
}
