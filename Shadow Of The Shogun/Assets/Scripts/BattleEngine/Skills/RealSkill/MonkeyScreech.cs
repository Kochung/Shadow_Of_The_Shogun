﻿using UnityEngine;
using System.Collections;

public class Screech : Skill
{
    bool once;

    public Screech()
    {
        MpUsed = 25;
        name = "Screech";
        info = "25Mp\nApply fear to enemy party";
        enemySelectAction = new EnemySelectPlayerAllAction();
        once = true;
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleManager.instance.textBox.setMessageRight("Select All Enemies");
            BattleManager.instance.cursor.setState(CursorControl.EnemySelectAllState);
            once = true;
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    character.addLoyalty(10);
                else
                    character.addLoyalty(-10);
                character.gainMp(MpUsed);
            }
            else
                character.useMp(MpUsed);
            int lenght = BattleManager.instance.cursor.getSelected().Length;
            int counter = 0;
            BattleCharacter[] newList = new BattleCharacter[lenght];
            for (int i = 0; i < lenght; i++)
            {
                if (BattleManager.instance.cursor.getSelected()[i] != -1)
                {
                    if(!BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[i]].job.Equals(""))
                    {
                        newList[counter] = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[i]];
                        counter++;
                    }
                }
            }
            string[] message = new string[counter + 1];
            message[0] = character.name + " screeches.";
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    message[0] += "\n" + character.name + " gains 10 loyalty.";
                else
                    message[0] += "\n" + character.name + " loses 10 loyalty.";
            }
            for (int i = 0; i < counter; i++)
            {
                BattleCharacter target = newList[i];

                if (target.immune)
                {
                    message[i + 1] = target.name + " is immune to attacks" ;
                }
                else
                {
                    bool crit = character.getCrit();
                    double damage = target.getDamage(character, 60, true, crit);
                    character.runStatusSkill(ref damage, true, ref crit, ref target.guarding);
                    target.addStatus(new Fear());
                    target.Damage((int)damage);
                    if (enhance)
                        character.Heal((int)damage / 2);
                    message[i + 1] = "";
                    if (crit)
                        message[i + 1] += "Critical Hit\n";
                    if (target.guarding)
                        message[i + 1] += target.name + " Guards\n";
                    message[i + 1] += target.name + " takes " + (int)damage + ".\n";
                    message[i + 1] += target.name + " is stricken with fear.";

                    if (BattleManager.instance.characters.isEnemy[character.index])
                    {
                        GameObject effect = EffectList.instance.Instantiate(2, target.character.transform.position);
                        effect.transform.localScale = new Vector3(-effect.transform.localScale.x, effect.transform.localScale.y, effect.transform.localScale.z);
                    }
                    else
                        EffectList.instance.Instantiate(2, target.character.transform.position);
                    SoundManager.instance.PlaySingle("screech.wav");
                }
            }
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }
}

