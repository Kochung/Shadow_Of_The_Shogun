﻿using UnityEngine;
using System.Collections;

public class Drink : Skill
{
    bool once;

    public Drink()
    {
        MpUsed = 10;
        name = "Drink";
        info = "10Mp\nApply courage to self";
        enemySelectAction = new EnemySelectSelfAction();
        once = true;
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();

            CursorPoints[][] points = new CursorPoints[1][];
            points[0] = new CursorPoints[1];
            if(BattleManager.instance.cursor.isIndex(7,BattleManager.instance.turnOrder.getCurrentTurnIndex()))
                points[0][0] = new CursorPoints(7);
            else if (BattleManager.instance.cursor.isIndex(8, BattleManager.instance.turnOrder.getCurrentTurnIndex()))
                points[0][0] = new CursorPoints(8);
            else if (BattleManager.instance.cursor.isIndex(9, BattleManager.instance.turnOrder.getCurrentTurnIndex()))
                points[0][0] = new CursorPoints(9);
            else
                points[0][0] = new CursorPoints(10);
            BattleManager.instance.cursor.setCustom(points, 0, 0);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);

            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    character.addLoyalty(10);
                else
                    character.addLoyalty(-10);
                character.gainMp(MpUsed);
            }
            else
                character.useMp(MpUsed);
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            string[] message = new string[1];
            message[0] = character.name + " Drinks."; 
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    message[0] += "\n" + character.name + " gains 10 loyalty.";
                else
                    message[0] += "\n" + character.name + " loses 10 loyalty.";
            }
            StatusEffect temp = new Courage();
            temp.turnCount++;
            target.addStatus(temp);
            TextBox.instance.setMessageMain(message);
			EffectList.instance.Instantiate(7, target.character.transform.position);
            SoundManager.instance.PlaySingle("buff.wav");
            once = true;
        }
    }
}