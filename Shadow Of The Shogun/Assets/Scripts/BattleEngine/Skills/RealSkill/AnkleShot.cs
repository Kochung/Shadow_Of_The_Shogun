﻿using UnityEngine;
using System.Collections;

public class AnkleShot : Skill
{
    public AnkleShot()
    {
        MpUsed = 15;
        name = "AnkleShot";
        info = "15Mp\n makes the target sluggish";
        enemySelectAction = new SelectRandomPlayerAction();
        selectorAction = new SkillSelectEnemyAction();
        enhancedSelectorAction = new SkillSelectEnemyAction(true);

        SingleTargetSkillAction temp = new SingleTargetSkillAction();

        temp.physical = false;
        temp.MpUsed = MpUsed;
        temp.baseDamage = 60;
		temp.effectIndex = 5;

        temp.statusEffectList = new StatusEffect[1];
        temp.statusEffectList[0] = new Slugish();

        temp.message = new string[2];
        temp.message[0] = "(N) uses Ankle Shot.";
        temp.message[1] = "(C?Critical Hit\n:)";
        temp.message[1] += "(G?(TN) Guards\n:)";
        temp.message[1] += "(TN) takes (D).\n";
        temp.message[1] += "(TN) is sluggish.";
        temp.soundEffect = "arrow.wav";

        action = temp;

        SingleTargetSkillAction Etemp = new SingleTargetSkillAction();

        Etemp.physical = false;
        Etemp.LoyaltyGain = -10;
        Etemp.MpGain = MpUsed;
        Etemp.baseDamage = 60;
        Etemp.healDamage = true;
		Etemp.effectIndex = 5;

        Etemp.statusEffectList = new StatusEffect[1];
        Etemp.statusEffectList[0] = new Slugish();

        Etemp.message = new string[2];
        Etemp.message[0] = "(N) uses Ankle Shot.\n";
        Etemp.message[0] += "(N) (E?loses:gains) 10 loyalty.";
        Etemp.message[1] = "(C?Critical Hit\n:)";
        Etemp.message[1] += "(G?(TN) Guards\n:)";
        Etemp.message[1] += "(TN) takes (D).\n";
        Etemp.message[1] += "(TN) is sluggish.";
        Etemp.soundEffect = "arrow.wav";

        enhancedAction = Etemp;
    }
}

