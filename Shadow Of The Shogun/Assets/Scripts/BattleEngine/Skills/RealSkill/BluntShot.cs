﻿using UnityEngine;
using System.Collections;

public class BluntShot : Skill
{
    bool once;

    public BluntShot()
    {
        MpUsed = 25;
        name = "BluntShot";
        info = "25Mp\nNo Damage\nApply Fear, Unfocus, and Stun";
        once = true;
        enemySelectAction = new SelectRandomPlayerAction();
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleManager.instance.cursor.setState(CursorControl.EnemySelectState);
            once = true;
        }
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);

            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    character.addLoyalty(10);
                else
                    character.addLoyalty(-10);
                character.gainMp(MpUsed);
            }
            else
                character.useMp(MpUsed);
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            if (target.immune)
            {
                string[] immuneMessage = { target.name + " is immune to attacks" };
                TextBox.instance.setMessageMain(immuneMessage);
                return;
            }
            
            string[] message = new string[2];
            message[0] = character.name + " uses Blunt Shot.";
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    message[0] += "\n" + character.name + " gains 10 loyalty.";
                else
                    message[0] += "\n" + character.name + " loses 10 loyalty.";
            }
            message[1] = target.name + " is in a state of fear.\n";
            message[1] += target.name + " is Unfocused.\n";
            message[1] += target.name + " is Stunned.";
            target.addStatus(new Fear());
            target.addStatus(new Unfocus());
            target.addStatus(new Stun());
            TextBox.instance.setMessageMain(message);
            if (BattleManager.instance.characters.isEnemy[character.index])
            {
                GameObject effect = EffectList.instance.Instantiate(5, target.character.transform.position);
                effect.transform.localScale = new Vector3(-effect.transform.localScale.x, effect.transform.localScale.y, effect.transform.localScale.z);
            }
            else
			    EffectList.instance.Instantiate(5, target.character.transform.position);

            SoundManager.instance.PlaySingle("arrow.wav");
        }
    }
}

