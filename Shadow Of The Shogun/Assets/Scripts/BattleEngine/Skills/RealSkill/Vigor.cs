﻿using UnityEngine;
using System.Collections;

public class Vigor : Skill
{
    bool once;

    public Vigor()
    {
        MpUsed = 25;
        name = "Vigor";
        info = "25Mp\nGives determination to all party members";
        once = true;
        enemySelectAction = new EnemySelectEnemyAllAction();
    }
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (!once)
        {
            BattleManager.instance.textBox.clear();
            BattleManager.instance.textBox.setMessageRight("Select All Party Members");
            BattleManager.instance.cursor.setState(CursorControl.PlayerSelectAllSate);
            once = true;
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            once = true;
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    character.addLoyalty(10);
                else
                    character.addLoyalty(-10);
                character.gainMp(MpUsed);
            }
            else
                character.useMp(MpUsed);
            int lenght = BattleManager.instance.cursor.getSelected().Length;
            int counter = 0;
            BattleCharacter[] newList = new BattleCharacter[lenght];
            for (int i = 0; i < lenght; i++)
            {
                if (BattleManager.instance.cursor.getSelected()[i] != -1)
                {
                    newList[counter] = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[i]];
                    counter++;
                }
            }
            string[] message = new string[counter + 1];
            message[0] = character.name + " uses Vigor.";
            if (enhance)
            {
                if (BattleManager.instance.characters.isEnemy[character.index])
                    message[0] += "\n" + character.name + " gains 10 loyalty.";
                else
                    message[0] += "\n" + character.name + " loses 10 loyalty.";
            }
            for (int i = 0; i < counter; i++)
            {
                BattleCharacter target = newList[i];
                message[i + 1] = target.name + " gains determination.";
                target.addStatus(new Determined());
                EffectList.instance.Instantiate(7, target.character.transform.position);
                SoundManager.instance.PlaySingle("buff.wav");
            }
            TextBox.instance.setMessageMain(message);
        }
    }
}