﻿using UnityEngine;
using System.Collections;

public class NoSkill : Skill
{
    bool first;
    public NoSkill()
    {
        MpUsed = 0;
        name = "None";
        info = "No skill in this slot.";
        first = true;
    }
    public override void StartSelect()
    {
        first = true;
    }
    public override void runSelect(BattleCharacter character)
    {
        if (first)
        {
            string [] message = {"No skill in this slot."};
            BattleManager.instance.textBox.setMessageMain(message);
            first = false;
        }
    }
    public override State getNextStateSelect(State state) 
    {
        if (BattleManager.instance.textBox.isMainActive())
            return state;
        return new SelectSkillState();

    }
}
