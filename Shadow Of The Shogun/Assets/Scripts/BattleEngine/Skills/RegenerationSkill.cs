﻿using UnityEngine;
using System.Collections;

public class RegenerationSkill : Skill
{
    bool once;

    public RegenerationSkill()
	{
		MpUsed = 100;
        name = "Regen";
        info = "Heals 50 for 3 turns";
        once = true;
	}
    public override void StartSelect()
    {
        once = false;
    }
    public override void runSelect(BattleCharacter character)
    {
        if(!once)
        {
            BattleManager.instance.textBox.clear();
            BattleManager.instance.cursor.setState(CursorControl.PlayerSelectSate);
            once = true;
        } 
        if (BattleManager.instance.cursor.getSelected()[0] != -1)
        {
            BattleManager.instance.textBox.clear();
            TextBox.instance.setMessageRight("");
            string left = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationLeft();
            string right = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getInformationRight();
            TextBox.instance.setMessageRight(left, -1, right, 15);

            int font = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsFontSize();
            string statusLeft = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffects();
            string statusRight = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]].getStatusEffectsRight();
            if (font == 24)
                TextBox.instance.setMessageLeft(statusLeft, font);
            else
                TextBox.instance.setMessageLeft(statusLeft, font, statusRight, font);
        }
    }
    public override void Start()
    {
        once = false;
    }
    public override void run(BattleCharacter character)
    {
        if (!once)
        {
            character.useMp(MpUsed);
            BattleCharacter target = BattleManager.instance.characters.list[BattleManager.instance.cursor.getSelected()[0]];
            string[] message = new string[1];
            if (BattleManager.instance.cursor.getSelected()[0] != BattleManager.instance.turnOrder.getCurrentTurnIndex())
            {
                message[0] = character.name + " gives " + target.name + "Regen";
                target.addStatus(new Regeneration());
            }
            else
            {
                message[0] = character.name + " gives himself Regen";
                StatusEffect temp = new Regeneration();
                temp.turnCount++;
                target.addStatus(temp);
            }
            TextBox.instance.setMessageMain(message);
            once = true;
        }
    }
}