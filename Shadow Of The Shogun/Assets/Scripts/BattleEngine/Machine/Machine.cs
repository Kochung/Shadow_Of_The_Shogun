﻿using UnityEngine;
using System.Collections;

public class Machine
{
    State state;
    public Machine(State startState)
    {
        state = startState;
    }
    public void Run()
    {
        state.Run();
        state = state.getNextState();
    }
}
