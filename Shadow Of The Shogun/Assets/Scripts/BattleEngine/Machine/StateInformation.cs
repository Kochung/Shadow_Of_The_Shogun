﻿using UnityEngine;
using System.Collections;

public class StateInformation
{
    public static StateInformation instance = null;
    public int SelectIndex;
    public bool suicide;
    public string suicideName;
	public int skillIndex;
    public int shogunActionSelect;
    public int shogunBribeAmount;
    public StateInformation()
    {
        if (instance == null)
            instance = this;
    }
	public void skillSelected()
	{
		skillIndex = BattleManager.instance.cursor.getSelected()[0] - 1;
	}
}
