﻿using UnityEngine;
using System.Collections;

public class Portraits : MonoBehaviour 
{
    public static Portraits instance = null;
    public GameObject[] portraits;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    public GameObject get(string name)
    {
        int length = portraits.Length;
        for (int i = 0; i < length; i++)
        {
            if (name.Equals(portraits[i].name))
                return portraits[i];
        }
        return null;

    }
}
