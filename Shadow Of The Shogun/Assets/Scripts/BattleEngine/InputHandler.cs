﻿using UnityEngine;
using System.Collections;

public class InputHandler : MonoBehaviour
{
    public static InputHandler instance = null;

    public bool AButton;
    public bool BButton;
    public bool StartButton;
    public bool SelectButton;

    public int aTimer;
    public int bTimer;
    public int startTimer;
    public int selectTimer;

    private int aTime;
    private int bTime;
    private int startTime;
    private int selectTime;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
	void Start () 
    {
        AButton = false;
        BButton = false;
        StartButton = false;
        SelectButton = false;
	}
	
	void Update () 
    {
        int a = (int)Input.GetAxisRaw("Fire1");
        int b = (int)Input.GetAxisRaw("Fire2");
        int start = (int)Input.GetAxisRaw("Submit");
        int select = (int)Input.GetAxisRaw("Cancel");

        AButton = false;
        BButton = false;
        StartButton = false;
        SelectButton = false;

        if (a == 0)
            aTime = 0;
        if (b == 0)
            bTime = 0;
        if (start == 0)
            startTime = 0;
        if (select == 0)
            selectTime = 0;

        if (aTime == 0)
        {
            if (a == 1)
            {
                AButton = true;
                aTime = aTimer;
            }
        }
        else
            aTime--;


        if (bTime == 0)
        {
            if (b == 1)
            {
                BButton = true;
                bTime = bTimer;
            }
        }
        else
            bTime--;


        if (startTime == 0)
        {
            if (start == 1)
            {
                StartButton = true;
                startTime = startTimer;
            }
        }
        else
            startTime--;


        if (selectTime == 0)
        {
            if (select == 1)
            {
                SelectButton = true;
                selectTime = selectTimer;
            }
        }
        else
            selectTime--;
	}
}
