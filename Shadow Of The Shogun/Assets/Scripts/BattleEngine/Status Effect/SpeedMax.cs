﻿using UnityEngine;
using System.Collections;

public class SpeedMax : StatusEffect
{
    int speedIncrese;
    public SpeedMax()
    {    
        type = PassiveType;
        turnCount = 1;
        name = "Speed Max";
    }
    public override string run(BattleCharacter character)
    {
        speedIncrese = character.RealSpd / 2;
        character.Spd += speedIncrese;
        return ""; 
    }
    public override void turnOver() 
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character) 
    {
        character.Spd -= speedIncrese;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new SpeedMax();
    }
	
}

