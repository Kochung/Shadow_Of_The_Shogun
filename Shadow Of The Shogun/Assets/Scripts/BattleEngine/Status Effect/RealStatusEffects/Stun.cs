﻿using UnityEngine;
using System.Collections;

public class Stun : StatusEffect
{
    public Stun()
    {
        type = TurnStartType;
        name = "Stun";
        turnCount = 3;
        // roll randon chance to skip turn
        //between 3 and 4
        //on stack reapply turn count
    }
    public override string run(BattleCharacter character)
    {
        if (BattleManager.instance.random.Next(0, 2) == 0)
            return "Stun causes " + character.name + " to skip his turn.";
        return character.name + " is stunned, but does not skip his turn.";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Stun();
    }
}
