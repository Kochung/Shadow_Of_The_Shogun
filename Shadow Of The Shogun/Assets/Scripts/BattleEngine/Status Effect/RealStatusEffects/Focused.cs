﻿using UnityEngine;
using System.Collections;

public class Focused : StatusEffect
{
    int Increase;
    public Focused()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Focused";
    }
    public override string run(BattleCharacter character)
    {
        Increase = (int)(character.RealMAtk * .25);
        character.MAtk += Increase;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.MAtk -= Increase;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Focused();
    }
}