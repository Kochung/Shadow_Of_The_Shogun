﻿using UnityEngine;
using System.Collections;

public class Determined : StatusEffect
{
    int Increase;
    public Determined()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Determined";
    }
    public override string run(BattleCharacter character)
    {
        Increase = (int)(character.RealDef * .25);
        character.Def += Increase;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.Def -= Increase;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Determined();
    }
}