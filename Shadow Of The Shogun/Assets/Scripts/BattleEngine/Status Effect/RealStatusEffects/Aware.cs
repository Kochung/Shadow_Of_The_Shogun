﻿using UnityEngine;
using System.Collections;

public class Aware : StatusEffect
{
    int Increase;
    public Aware()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Aware";
    }
    public override string run(BattleCharacter character)
    {
        Increase = (int)(character.RealMDef * .25);
        character.MDef += Increase;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.MDef -= Increase;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Aware();
    }
}

