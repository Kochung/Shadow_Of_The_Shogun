﻿using UnityEngine;
using System.Collections;

public class Slugish : StatusEffect
{
    int Decrese;
    public Slugish()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Slugish";
    }
    public override string run(BattleCharacter character)
    {
        Decrese = (int)(character.RealSpd * .25);
        character.Spd -= Decrese;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.Spd += Decrese;
    }

    public override StatusEffect clone(BattleCharacter character)
    {
        return new Slugish();
    }
}

