﻿using UnityEngine;
using System.Collections;

public class SheathSwordBuff : StatusEffect
{
    int spdIncrese;
    int defDecrese;
    public SheathSwordBuff()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Sword Sheathed";
    }
    public override string run(BattleCharacter character)
    {
        spdIncrese = (int)(character.RealSpd * .1);
        defDecrese = (int)(character.RealDef * .1);
        character.Spd += spdIncrese;
        character.Def -= defDecrese;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.Spd -= spdIncrese;
        character.Def += defDecrese;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new SheathSwordBuff();
    }

}

