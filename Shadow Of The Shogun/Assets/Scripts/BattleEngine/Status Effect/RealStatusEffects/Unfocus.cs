﻿using UnityEngine;
using System.Collections;

public class Unfocus : StatusEffect
{
    int Decrese;
    public Unfocus()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Unfocus";
    }
    public override string run(BattleCharacter character)
    {
        Decrese = (int)(character.RealMAtk * .25);
        character.MAtk -= Decrese;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.MAtk += Decrese;
    }

    public override StatusEffect clone(BattleCharacter character)
    {
        return new Unfocus();
    }
}


