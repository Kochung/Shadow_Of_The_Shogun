﻿using UnityEngine;
using System.Collections;

public class Panic : StatusEffect
{
    int Decrese;
    public Panic()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Panic";
    }
    public override string run(BattleCharacter character)
    {
        Decrese = (int)(character.RealDef * .25);
        character.Def -= Decrese;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.Def += Decrese;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Panic();
    }

}

