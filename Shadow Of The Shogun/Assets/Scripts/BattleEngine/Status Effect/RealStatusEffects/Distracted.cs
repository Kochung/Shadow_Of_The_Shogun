﻿using UnityEngine;
using System.Collections;

public class Distracted : StatusEffect
{
    int Decrese;
    public Distracted()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Distracted";
    }
    public override string run(BattleCharacter character)
    {
        Decrese = (int)(character.RealMDef * .25);
        character.MDef -= Decrese;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.MDef += Decrese;
    }

    public override StatusEffect clone(BattleCharacter character)
    {
        return new Distracted();
    }

}
