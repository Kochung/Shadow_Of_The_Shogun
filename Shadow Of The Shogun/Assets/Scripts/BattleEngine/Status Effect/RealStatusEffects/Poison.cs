﻿using UnityEngine;
using System.Collections;

public class Poison : StatusEffect
{
    public BattleCharacter applyCharacter;
    public Poison()
    {

    }
    public Poison(BattleCharacter character)
    {
        applyCharacter = character;
        type = TurnStartType;
        turnCount = 5;
        name = "Poison";
        //pick some potency look at 20
        //deal magic
        //bleed for phisical
        //between 4 and 5
        //on stack reapply turn count, combine Charicter stat, if broken double potency
    }
    public override void Stack(StatusEffect effect)
    {
        applyCharacter = ((Poison)effect).applyCharacter;
        turnCount = 5;
    }
    public override string run(BattleCharacter character)
    {
        bool crit = character.getCrit();
        double damage = character.getDamage(applyCharacter, 30, false, crit);
        character.NoHitDamage((int)damage);
        if (!crit)
            return character.name + " takes " + (int)damage + " from Poison";
        else
            return "Critical\n"+character.name + " takes " + (int)damage + " from Poison";
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Poison(character);
    }
}
