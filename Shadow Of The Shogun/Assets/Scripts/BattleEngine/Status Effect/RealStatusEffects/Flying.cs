﻿using UnityEngine;
using System.Collections;

public class Flying : StatusEffect
{
    public Flying()
    {
        type = PassiveType;
        turnCount = 1;
        name = "Flying";
    }
    public override string run(BattleCharacter character)
    {
        character.immune = true;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.immune = false;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Flying();
    }
}