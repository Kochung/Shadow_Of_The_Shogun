﻿using UnityEngine;
using System.Collections;

public class Hasty : StatusEffect
{
    int Increase;
    public Hasty()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Hasty";
    }
    public override string run(BattleCharacter character)
    {
        Increase = (int)(character.RealSpd * .25);
        character.Spd += Increase;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.Spd -= Increase;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Hasty();
    }
}