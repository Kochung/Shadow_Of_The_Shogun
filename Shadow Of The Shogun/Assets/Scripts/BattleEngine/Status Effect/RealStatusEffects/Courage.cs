﻿using UnityEngine;
using System.Collections;

public class Courage : StatusEffect
{
    int Increase;
    public Courage()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Courage";
    }
    public override string run(BattleCharacter character)
    {
        Increase = (int)(character.RealAtk * .25);
        character.Atk += Increase;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.Atk -= Increase;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Courage();
    }
}