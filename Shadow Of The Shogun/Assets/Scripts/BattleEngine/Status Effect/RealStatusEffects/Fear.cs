﻿using UnityEngine;
using System.Collections;

public class Fear : StatusEffect
{
    int Decrese;
    public Fear()
    {
        type = PassiveType;
        turnCount = 3;
        name = "Fear";
    }
    public override string run(BattleCharacter character)
    {
        Decrese = (int)(character.RealAtk * .25);
        character.Atk -= Decrese;
        return "";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = effect.turnCount;
    }
    public override void turnOver()
    {
        turnCount--;
    }
    public override void dropoff(BattleCharacter character)
    {
        character.Atk += Decrese;
    }

    public override StatusEffect clone(BattleCharacter character)
    {
        return new Fear();
    }
}

