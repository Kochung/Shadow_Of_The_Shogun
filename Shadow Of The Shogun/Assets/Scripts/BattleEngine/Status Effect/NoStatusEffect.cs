﻿using UnityEngine;
using System.Collections;

public class NoStatusEffect : StatusEffect
{
    public NoStatusEffect()
    {
        type = NoType;
        turnCount = -1;
        name = "";
    }
}
