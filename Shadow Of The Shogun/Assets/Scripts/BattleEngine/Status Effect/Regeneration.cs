﻿using UnityEngine;
using System.Collections;

public class Regeneration : StatusEffect
{
    public Regeneration()
    {    
        type = TurnStartType;
        turnCount = 3;
        name = "Regeneration";
    }
    public override void Stack(StatusEffect effect)
    {
        turnCount = 3;
    }
    public override string run(BattleCharacter character)
    {
        character.Heal(50);
        return character.name + " Heals 50 Hp";
    }
    public override void turnOver() 
    {
        turnCount--;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new Regeneration();
    }
	
}
