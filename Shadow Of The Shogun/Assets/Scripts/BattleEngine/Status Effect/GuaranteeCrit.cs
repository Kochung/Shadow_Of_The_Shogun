﻿using UnityEngine;
using System.Collections;

public class GuaranteeCrit : StatusEffect
{
    public GuaranteeCrit()
    {    
        type = SkillAndAttackType;
        turnCount = 1;
        name = "Guarantee Crit";
    }
    public override double runAttack(ref double damage, bool physical, ref bool crit, ref bool guarding) 
    {
        if(crit)
            return damage;
        else
        {
            crit = true;
            damage *= 1.5f;
            return damage;
        }
    }
    public override void turnOver() 
    {
        turnCount--;
    }
    public override StatusEffect clone(BattleCharacter character)
    {
        return new GuaranteeCrit();
    }
	
}
