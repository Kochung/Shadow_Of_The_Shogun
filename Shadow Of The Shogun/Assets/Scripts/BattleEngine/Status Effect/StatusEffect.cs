﻿using UnityEngine;
using System.Collections;

public class StatusEffect 
{
    public const int TurnStartType = 0;
    public const int TurnEndType = 1;
    public const int AttackType = 2;
    public const int SkillType = 3;
    public const int SkillAndAttackType = 4;
    public const int PassiveType = 5;
    public const int NoType = 6;
    public const int AffterAttackType = 7;
    public const int AffterSkillType = 8;
    public const int AffterSkillAndAttackType = 9;
    public int type;
    public int turnCount;
    public string name;
    public virtual void Stack(StatusEffect effect){ }
    public virtual string run(BattleCharacter character) { return ""; }
    public virtual double runAttack(ref double damage, bool physical, ref bool crit, ref bool guarding) { return damage; }
    public virtual void turnOver() { }
    public virtual void dropoff(BattleCharacter character) { }
    public bool checkType(int Type)
    {
        if (type == AffterSkillAndAttackType)
        {
            return Type == AffterAttackType || Type == AffterSkillType;
        }
        if (type == SkillAndAttackType)
        {
            return Type == AttackType || Type == SkillType;
        }
        return type == Type;
    }
    public virtual StatusEffect clone(BattleCharacter character)
    {
        return new NoStatusEffect();
    }
}
