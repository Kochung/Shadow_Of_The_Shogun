﻿using UnityEngine;
using System.Collections;

public class CursorPoints 
{
    public int[] points;
    public CursorPoints()
    {
        points = new int[1];
        points[0] = -1;
    }
    public CursorPoints(int Point)
    {
        points = new int [1];
        points[0] = Point;
    }
    public CursorPoints(int [] Points)
    {
        points = Points;
    }
}
