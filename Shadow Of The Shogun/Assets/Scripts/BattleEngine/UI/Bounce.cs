﻿using UnityEngine;
using System.Collections;

public class Bounce : MonoBehaviour 
{
    public int timeDown;
    public int timeUp;
    public float distance;

    private bool up = false;
    private int counter;
    private float DistanceUp;
    private float DistanceDown;

    void Start()
    {
        DistanceUp = distance / timeUp;
        DistanceDown = distance / timeDown;
    }
    void Update()
    {
        if (up)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + DistanceUp, transform.position.z);
            if (counter == timeUp)
            {
                up = false;
                counter = 0;
            }
            else
                counter++;
        }
        else
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - DistanceDown, transform.position.z);
            if (counter == timeDown)
            {
                up = true;
                counter = 0;
            }
            else
                counter++;
        }
    }
}
