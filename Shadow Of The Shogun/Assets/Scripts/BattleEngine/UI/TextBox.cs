﻿using UnityEngine;
using System.Collections;

public class TextBox
{
    public static TextBox instance = null;
    private SmallTextBox Left;
    private SmallTextBox Right;
    private SmallTextBox TurnBox;
    private BigTextBox Main;
    public TextBox()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public void reInstantiate()
    {
        Left = GameObject.Find("LeftTextBox").GetComponent<SmallTextBox>();
        Right = GameObject.Find("RightTextBox").GetComponent<SmallTextBox>();
        Main = GameObject.Find("MainTextBox").GetComponent<BigTextBox>();
        TurnBox = GameObject.Find("TurnBox").GetComponent<SmallTextBox>();
    }
    public void setMessageLeft(string message)
    {
        Left.Activate();
        Left.SetMessage(message);
    }
    public void setMessageLeft(string message, int size)
    {
        Left.Activate();
        Left.SetMessage(message, size);
    }
    public void setMessageRight(string message)
    {
        Right.Activate();
        Right.SetMessage(message);
    }
    public void setMessageRight(string message, int size)
    {
        Right.Activate();
        Right.SetMessage(message, size);
    }
    public void setTurnMessage(BattleCharacter character)
    {
        TurnBox.Activate();
        string message = character.name + "\n" +
                         character.job + "\n" +
                         "Hp:" + character.Hp + "/" + character.MaxHp + "\n" +
                         "Mp:" + character.Mp + "/" + character.MaxMp + "\n" +
                         "Loyalty:" + character.loyalty;
        TurnBox.SetMessage(message);
    }
    public void TurnBoxDeactivate()
    {
        TurnBox.DeActivate();
        TurnBox.SetMessage("", "");
    }
    public void setMessageLeft(string leftMessage, string rightMessage)
    {
        Left.Activate();
        Left.SetMessage(leftMessage, rightMessage);
    }
    public void setMessageLeft(string leftMessage, int leftFontSize, string rightMessage, int rightFontSize)
    {
        Left.Activate();
        Left.SetMessage(leftMessage, leftFontSize, rightMessage, rightFontSize);
    }
    public void setMessageRight(string leftMessage, string rightMessage)
    {
        Right.Activate();
        Right.SetMessage(leftMessage, rightMessage);
    }
    public void setMessageRight(string leftMessage, int leftFontSize, string rightMessage, int rightFontSize)
    {
        Right.Activate();
        Right.SetMessage(leftMessage, leftFontSize, rightMessage, rightFontSize);
    }

    public void setMessageMain(string[] message)
    {
        Left.DeActivate();
        Right.DeActivate();
        Main.SetMessage(message);
    }
    public void setMessageMain(string[] message, int[] size)
    {
        Left.DeActivate();
        Right.DeActivate();
        Main.SetMessage(message, size);
    }
    public void clear()
    {
        Left.SetMessage("");
        Right.SetMessage("");

        Left.SetMessage("","");
        Right.SetMessage("","");
    }
    public bool isMainActive()
    {
        return Main.isActive();
    }
}
