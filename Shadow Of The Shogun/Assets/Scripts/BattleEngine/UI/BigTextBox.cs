﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BigTextBox : MonoBehaviour
{
    private Image box;
    private Text text;
    private Canvas canvas;
    private Image arrow;
    private int messageIndex;
    private int messagesIndex;
    private int messagelength;
    private int messageslength;
    private int counter;
    private bool Active;

    public int speed;
    public int defaultFontSize;
    public string[] messages;
    public int[] fontSizes;

    void Start()
    {
        canvas = GetComponent<Canvas>();
        box = transform.GetChild(0).GetComponent<Image>();
        text = transform.GetChild(1).GetComponent<Text>();
        arrow = transform.GetChild(2).GetComponent<Image>();
        canvas.enabled = false;
        box.enabled = false;
        arrow.enabled = false;
        text.text = "";
    }

    void Update()
    {
        if (Time.timeScale == 0)
            return;
        if (Active)
        {
            if (InputHandler.instance.AButton)
            {
                if (messageIndex != messagelength)
                {
                    messageIndex = messagelength;
                    text.text = messages[messagesIndex];
                }
                else if (messagesIndex != messageslength - 1)
                {
                    messagesIndex++;
                    messageIndex = 0;
                    messagelength = messages[messagesIndex].Length + 1;
                    if (fontSizes.Length != 0 && fontSizes[messagesIndex] != 0)
                        text.fontSize = fontSizes[messagesIndex];
                    counter = 0;
                }
                else
                {
                    canvas.enabled = false;
                    box.enabled = false;
                    text.text = "";
                    messageIndex = 0;
                    messagelength = 0;
                    messagesIndex = 0;
                    messageslength = 0;
                    Active = false;
                    arrow.enabled = false;

                }
            }
            if (messageIndex != messagelength)
            {
                if (counter == 0)
                {
                    text.text = messages[messagesIndex].Substring(0, messageIndex);
                    counter++;
                }
                else
                {
                    counter++;
                    if (counter == speed)
                    {
                        messageIndex++;
                        counter = 0;
                    }
                }
            }
            if (box.enabled)
                arrow.enabled = (messageIndex == messagelength) && (messagesIndex != messageslength - 1);
        }
    }

    public void SetMessage(string[] Messages)
    {
        messages = Messages;
        fontSizes = new int[0];
        messageIndex = 0;
        messagesIndex = 0;
        messagelength = messages[messagesIndex].Length + 1;
        messageslength = messages.Length;
        counter = 0;
        canvas.enabled = true;
        box.enabled = true;
        Active = true;
        text.fontSize = defaultFontSize;
    }
    public void SetMessage(string[] Messages, int[] FontSize)
    {
        messages = Messages;
        fontSizes = FontSize;
        messageIndex = 0;
        messagesIndex = 0;
        messagelength = messages[messagesIndex].Length + 1;
        messageslength = messages.Length;
        counter = 0;
        canvas.enabled = true;
        box.enabled = true;
        Active = true;
        if (fontSizes[messagesIndex] != -1)
            text.fontSize = fontSizes[messagesIndex];
        else
            text.fontSize = defaultFontSize;
    }
    public bool isActive()
    {
        return Active;
    }
}
