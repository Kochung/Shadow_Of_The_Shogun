﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SmallTextBox : MonoBehaviour
{
    private Image box;
    private Text text;
    private Text leftText;
    private Text rightText;
    private Canvas canvas;
    private bool Active;

    public int defaultFontSize;

	void Start ()
    {
        canvas = GetComponent<Canvas>();
        box = transform.GetChild(0).GetComponent<Image>();
        text = transform.GetChild(1).GetComponent<Text>();
        leftText = transform.GetChild(2).GetComponent<Text>();
        rightText = transform.GetChild(3).GetComponent<Text>();
        canvas.enabled = true;
        box.enabled = true;
        text.text = "";
	}
    public void SetMessage(string Message)
    {
        text.fontSize = defaultFontSize;
        text.text = Message;
    }
    public void SetMessage(string Message, int FontSize)
    {
        if (FontSize == -1)
            text.fontSize = defaultFontSize;
        else
            text.fontSize = FontSize;
        text.text = Message;
    }
    public void SetMessage(string leftMessage,string rightMessage)
    {
        leftText.fontSize = defaultFontSize;
        leftText.text = leftMessage;

        rightText.fontSize = defaultFontSize;
        rightText.text = rightMessage;
    }
    public void SetMessage(string leftMessage, int leftFontSize, string rightMessage, int rightFontSize)
    {
        if(leftFontSize == -1)
            leftText.fontSize = defaultFontSize;
        else
            leftText.fontSize = leftFontSize;
        leftText.text = leftMessage;

        if (rightFontSize == -1)
            rightText.fontSize = defaultFontSize;
        else
            rightText.fontSize = rightFontSize;
        rightText.text = rightMessage;
    }
    public bool isActive()
    {
        return Active;
    }
    public void Activate()
    {
        canvas.enabled = true;
        box.enabled = true;
        Active = true;
    }

    public void DeActivate()
    {
        canvas.enabled = false;
        box.enabled = false;
        Active = false;
    }
}
