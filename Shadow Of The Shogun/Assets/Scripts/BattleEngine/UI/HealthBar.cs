﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour
{

    private int MaxHealth;
    private int CurrentHealth;
    private int DrainHealth;
    private Slider bar;
    private Text text;
    private BattleCharacter character;
    void Awake ()
    {
        bar = gameObject.GetComponent<Slider>();
        text = transform.GetComponentInChildren<Text>();
        text.fontSize = 8;
    }
	public void Update ()
    {
        updateBarNoDeath();
        if (DrainHealth > CurrentHealth)
            DrainHealth--;
        if (DrainHealth < CurrentHealth)
            DrainHealth++;
    }
    public void updateBar()
    {
        if (bar.enabled)
        {
            if (character.Hp <= 0)
                disable();
            else
                UpdateCurrentHealth(character.Hp);
        }
    }
    public void updateBarNoDeath()
    {
        if (bar.enabled)
        {
            UpdateCurrentHealth(character.Hp);
        }
    }
    public void setCharacter(BattleCharacter Character)
    {
        character = Character;
        MaxHealth = character.MaxHp;
        CurrentHealth = character.Hp;
        DrainHealth = CurrentHealth;
    }
    public void disable()
    {
        transform.GetChild(1).GetChild(1).GetComponent<Text>().enabled = false;
        transform.GetChild(0).GetComponent<Image>().enabled = false;
        transform.GetChild(1).GetChild(0).GetComponent<Image>().enabled = false;
        bar.enabled = false;
    }
    public void enable()
    {
        transform.GetChild(1).GetChild(1).GetComponent<Text>().enabled = true;
        transform.GetChild(0).GetComponent<Image>().enabled = true;
        transform.GetChild(1).GetChild(0).GetComponent<Image>().enabled = true;
        bar.enabled = true;
    }

    public void UpdateMaxHealth(int maxVal)
    {
        MaxHealth = maxVal;
        UpdateBar();
    }

    public void UpdateCurrentHealth(int currentVal)
    {
        CurrentHealth = currentVal;
        UpdateBar();
    }

    public void UpdateBar()
    {
        bar.maxValue = MaxHealth;
        bar.value = DrainHealth;
        text.text = DrainHealth + "/" + MaxHealth;
        if (DrainHealth / (float)MaxHealth > .5)
            transform.GetChild(1).GetChild(0).GetComponent<Image>().color = Color.green;
        else if (DrainHealth / (float)MaxHealth > .25)
            transform.GetChild(1).GetChild(0).GetComponent<Image>().color = Color.yellow;
        else
            transform.GetChild(1).GetChild(0).GetComponent<Image>().color = Color.red;
    }
}
