﻿using UnityEngine;
using System.Collections;

public class CursorControl : MonoBehaviour
{
    public const int NotActiveState = 0;
    public const int Select4State = 1;
    public const int Select3State = 2;
    public const int Select6State = 3;
    public const int PlayerSelectSate = 4;
    public const int EnemySelectState = 5;
    public const int EnemyFrontSelectState = 6;
    public const int PlayerAndEnemySelectState = 7;
    public const int EnemySelectAllState = 8;
    public const int CustomSelect = 9;
    public const int PlayerSelectAllSate = 10;
    public const int ForceState = 11;
    public const int SelectSelfState = 12;

    private int state;
    private int realState;
    private int positionX;
    private int positionY;
    private CursorPoints[][] pointMap;
    private SpriteRenderer[] Cursors;
    private int[] forceSelect;
    private GameObject TurnCursor;
	void Start () 
    {
        state = 0;
        TurnCursor = GameObject.Find("TurnCursor");
        selectSelfOff();

        Cursors = new SpriteRenderer[21];

        GameObject tempParent = GameObject.Find("Cursors");
        for (int i = 0; i < 7; i++)
            Cursors[i] = tempParent.transform.GetChild(i).gameObject.GetComponent<SpriteRenderer>();

        tempParent = GameObject.Find("PlayerCursors");
        for (int i = 7; i < 12; i++)
            Cursors[i] = tempParent.transform.GetChild(i - 7).gameObject.GetComponent<SpriteRenderer>();

        tempParent = GameObject.Find("EnemyCursors");
        for (int i = 12; i < 21; i++)
            Cursors[i] = tempParent.transform.GetChild(i - 12).gameObject.GetComponent<SpriteRenderer>();
        clearAll();
        positionX = -1;
        positionY = -1;
        state = NotActiveState;
	}
	void Update ()
    {
        if (Time.timeScale == 0)
            return;
        if(state == CustomSelect)
            runPointerMap();
	}
    public void setCustom(CursorPoints[][]Points, int startX, int startY)
    {
        state = CustomSelect;
        pointMap = Points;
        positionX = startX;
        positionY = startY;
        checkSelection();
        moveCursors();
    }
    public void setState(int newState)
    {
        realState = newState;
        switch (newState)
        {
            case NotActiveState:
                state = 0;
                clearAll();
                selectSelfOn();
                break;
            case Select4State:
                setSelect(4);
                selectSelfOn();
                break;
            case Select3State:
                setSelect(3);
                selectSelfOn();
                break;
            case Select6State:
                setSelect6();
                selectSelfOn();
                break;
            case PlayerSelectSate:
                setPlayerSelect();
                selectSelfOff();
                break;
            case EnemySelectState:
                setEnemySelect();
                selectSelfOff();
                break;
            case EnemyFrontSelectState:
                setEnemyFrontSelect();
                selectSelfOff();
                break;
            case PlayerAndEnemySelectState:
                setPlayerAndEnemySelect();
                selectSelfOff();
                break;
            case EnemySelectAllState:
                setEnemySelectAll();
                selectSelfOff();
                break;
            case PlayerSelectAllSate:
                setPlayerSelectAllSate();
                selectSelfOff();
                break;
            case SelectSelfState:
                setSelectSelf();
                selectSelfOff();
                break;
            case ForceState:
                state = ForceState;
                clearAll();
                selectSelfOn();
                break;
            default:
                state = newState;
                clearAll();
                selectSelfOff();
                break;
        }
    }
    public void restartState()
    {
        setState(state);
    }
    public void clearAll()
    {
        for(int i = 0; i < 21; i++)
        {
            Cursors[i].enabled = false;
        }
    }
    private void setSelect(int max)
    {
        CursorPoints[][] points = new CursorPoints[1][];
        points[0] = new CursorPoints[max];
        for (int i = 0; i < max; i++)
        { 
            points[0][i]= new CursorPoints(3-i);
        }
        setCustom(points,0,max-1);
    }

    private void setSelect6()
    {
        CursorPoints[][] points = new CursorPoints[2][];
        points[0] = new CursorPoints[3];
        points[1] = new CursorPoints[3];
        
        points[0][0] = new CursorPoints(3);
        points[0][1] = new CursorPoints(2);
        points[0][2] = new CursorPoints(1);

        points[1][0] = new CursorPoints(6);
        points[1][1] = new CursorPoints(5);
        points[1][2] = new CursorPoints(4);

        setCustom(points, 0, 2);
    }

    private void setPlayerSelect()
    {
        CursorPoints[][] points = new CursorPoints[2][];
        points[0] = new CursorPoints[2];
        points[1] = new CursorPoints[2];

        points[0][0] = new CursorPoints(playerIndex(2));
        points[1][0] = new CursorPoints(playerIndex(0));
        points[0][1] = new CursorPoints(playerIndex(3));
        points[1][1] = new CursorPoints(playerIndex(1));
        setCustom(points, 0, 1);
    }

    private void setEnemySelect()
    {

        CursorPoints[][] points = new CursorPoints[3][];
        points[0] = new CursorPoints[3];
        points[1] = new CursorPoints[3];
        points[2] = new CursorPoints[3];

        points[0][2] = new CursorPoints(enemyIndex(6));
        points[1][2] = new CursorPoints(enemyIndex(7));
        points[2][2] = new CursorPoints(enemyIndex(8));

        points[0][1] = new CursorPoints(enemyIndex(3));
        points[1][1] = new CursorPoints(enemyIndex(4));
        points[2][1] = new CursorPoints(enemyIndex(5));

        points[0][0] = new CursorPoints(enemyIndex(0));
        points[1][0] = new CursorPoints(enemyIndex(1));
        points[2][0] = new CursorPoints(enemyIndex(2));

        setCustom(points, 0, 2);
    }
    public void selectSelfOn()
    {
        int pos = 0;
        CursorPoints[][] points = new CursorPoints[1][];
        points[0] = new CursorPoints[1];
        for (int i = 7; i < 20; i++)
        {
            if (isIndex(i, BattleManager.instance.turnOrder.getCurrentTurnIndex()))
            {
                pos = i;
                TurnCursor.transform.position = Cursors[i].transform.position;
                break;
            }
        }
    }
    public void selectSelfOff()
    {
        TurnCursor.transform.position = new Vector3(999,999,0);
    }
    private void setEnemyFrontSelect()
    {
        CursorPoints[][] points = new CursorPoints[1][];
        points[0] = new CursorPoints[3];

        if (enemyIndex(6) != -1)
            points[0][2] = new CursorPoints(enemyIndex(6));
        else if (enemyIndex(7) != -1)
            points[0][2] = new CursorPoints(enemyIndex(7));
        else
            points[0][2] = new CursorPoints(enemyIndex(8));

        if (enemyIndex(3) != -1)
            points[0][1] = new CursorPoints(enemyIndex(3));
        else if (enemyIndex(4) != -1)
            points[0][1] = new CursorPoints(enemyIndex(4));
        else
            points[0][1] = new CursorPoints(enemyIndex(5));

        if (enemyIndex(0) != -1)
            points[0][0] = new CursorPoints(enemyIndex(0));
        else if (enemyIndex(1) != -1)
            points[0][0] = new CursorPoints(enemyIndex(1));
        else
            points[0][0] = new CursorPoints(enemyIndex(2));

        setCustom(points, 0, 2);
    }

    private void setEnemySelectAll()
    {
        CursorPoints[][] points = new CursorPoints[1][];
        points[0] = new CursorPoints[1];
        int[] p = new int[9];


        p[0] = enemyIndex(6);
        p[1] = enemyIndex(7);
        p[2] = enemyIndex(8);

        p[3] = enemyIndex(3);
        p[4] = enemyIndex(4);
        p[5] = enemyIndex(5);

        p[6] = enemyIndex(0);
        p[7] = enemyIndex(1);
        p[8] = enemyIndex(2);

        points[0][0] = new CursorPoints(p);
        setCustom(points, 0, 0);
    }
    private void setPlayerSelectAllSate()
    {
        CursorPoints[][] points = new CursorPoints[1][];
        points[0] = new CursorPoints[1];
        int[] p = new int[4];

        p[0] = playerIndex(0);
        p[1] = playerIndex(1);
        p[2] = playerIndex(2);
        p[3] = playerIndex(3);

        points[0][0] = new CursorPoints(p);
        setCustom(points, 0, 0);
    }
    private void setPlayerAndEnemySelect()
    {
        CursorPoints[][] points = new CursorPoints[5][];
        points[0] = new CursorPoints[3];
        points[1] = new CursorPoints[3];
        points[2] = new CursorPoints[3];
        points[3] = new CursorPoints[3];
        points[4] = new CursorPoints[3];

        points[2][2] = new CursorPoints(enemyIndex(6));
        points[3][2] = new CursorPoints(enemyIndex(7));
        points[4][2] = new CursorPoints(enemyIndex(8));

        points[2][1] = new CursorPoints(enemyIndex(3));
        points[3][1] = new CursorPoints(enemyIndex(4));
        points[4][1] = new CursorPoints(enemyIndex(5));

        points[2][0] = new CursorPoints(enemyIndex(0));
        points[3][0] = new CursorPoints(enemyIndex(1));
        points[4][0] = new CursorPoints(enemyIndex(2));

        points[0][0] = new CursorPoints(playerIndex(3));
        points[1][0] = new CursorPoints(playerIndex(1));
        points[0][1] = new CursorPoints(playerIndex(2));
        points[1][1] = new CursorPoints(playerIndex(0));
        points[0][2] = new CursorPoints();
        points[1][2] = new CursorPoints();

        setCustom(points, 0, 1);
    }
    public void setSelectSelf()
    {
        CursorPoints[][] points = new CursorPoints[1][];
        points[0] = new CursorPoints[1];
        for (int i = 7; i < 20; i++)
        {
            if (isIndex(i, BattleManager.instance.turnOrder.getCurrentTurnIndex()))
            {
                points[0][0] = new CursorPoints(i);
                break;
            }

        }
        setCustom(points, 0, 0);
    }
    public void runPointerMap()
    {
        int oldX = positionX;
        int oldY = positionY;
        int horizontal = 0;
        int vertical = 0;
        if (DirectionalPadHandler.instance.Up)
            vertical++;
        if (DirectionalPadHandler.instance.Down)
            vertical--;
        if (DirectionalPadHandler.instance.Left)
            horizontal++;
        if (DirectionalPadHandler.instance.Right)
            horizontal--;
        changeHorizontal(horizontal);
        changeVertical(vertical);
        if (horizontal != 0 || vertical != 0)
        {
            SoundManager.instance.PlaySingle("click");
        }
        if (!(oldX == positionX && oldY == positionY))
        {
            pointCheck(horizontal,vertical, oldX, oldY);
            moveCursors();
        }
        
    }
    public void pointCheck(int horizontal, int vertical,int oldX, int oldY)
    {
        int width = pointMap.Length;
        int height = pointMap[0].Length;
        if (pointMap[positionX][positionY].points[0] == -1)
        {
            positionX = oldX;
            positionY = oldY;
            if (horizontal != 0)
            {
                int move = -1;
                if (positionY == 0)
                    move = 1;
                for (int i = 0; i < width; i++)
                {
                    changeHorizontal(horizontal);
                    for (int j = 0; j < height; j++)
                    {
                        if (pointMap[positionX][positionY].points[0] != -1)
                            return;
                        else
                            changeVertical(move);
                    }
                }
            }
            else if(vertical != 0)
            {
                for (int j = 0; j < height; j++)
                {
                    changeVertical(vertical);
                    if (pointMap[positionX][positionY].points[0] != -1)
                        return;
                }
            }

        }
    }
    public void changeHorizontal(int horizontal)
    {
        positionX += horizontal;
        int maxX = pointMap.Length;
        if (positionX < 0)
            positionX = maxX - 1;
        if (positionX == maxX)
            positionX = 0;
    }
    private void changeVertical(int vertical)
    {
        positionY += vertical;
        int maxY = pointMap[0].Length;
        if (positionY < 0)
            positionY = maxY - 1;
        if (positionY == maxY)
            positionY = 0;
    }
    private void moveCursors()
    {
        clearAll();
        int length = pointMap[positionX][positionY].points.Length;
        for (int i = 0; i < length; i++)
        {
            if(pointMap[positionX][positionY].points[i] != -1)
                Cursors[pointMap[positionX][positionY].points[i]].enabled = true;
        }
    }

    public int playerIndex(int index)
    {
        if (convertToBord(index + 7) == 0)
            return -1;
        return index += 7;
    }
    public int enemyIndex(int index)
    {
        if (convertToBord(index + 12) == 0)
            return -1;
        return index += 12;
    }
    public void setForceSelect(int [] selected)
    {
        forceSelect = selected;
    }
    public int[] getSelected()
    {
        if (state == ForceState)
            return forceSelect;
        if (pointMap == null)
            return forceSelect;
        int length = pointMap[positionX][positionY].points.Length;
        int[] selectedIndex = new int [length];
        for (int i = 0; i < length; i++)
        {
            int number = pointMap[positionX][positionY].points[i];
            selectedIndex[i] = convertToBord(number);
        }
        return selectedIndex;
    }
    public bool isIndex(int cursorIndex,int characterIndex)
    {
        return convertToBord(cursorIndex) == characterIndex;
    }
    public bool isState(int value)
    {
        return realState == value;
    }
    private int convertToBord(int number)
    {
        if (number < 7)
            return number;
        else if (number < 12)
        {
            number -= 7;
            switch (number)
            {
                case 0:
                    return BattleManager.instance.board.playerBord[1][0];
                case 1:
                    return BattleManager.instance.board.playerBord[1][1];
                case 2:
                    return BattleManager.instance.board.playerBord[0][0];
                case 3:
                    return BattleManager.instance.board.playerBord[0][1];
                case 4:
                    return 1;
            }
        }
        else
        {
            number -= 12; switch (number)
            {
                case 0:
                    return BattleManager.instance.board.enemyBord[0][2];
                case 1:
                    return BattleManager.instance.board.enemyBord[1][2];
                case 2:
                    return BattleManager.instance.board.enemyBord[2][2];
                case 3:
                    return BattleManager.instance.board.enemyBord[0][1];
                case 4:
                    return BattleManager.instance.board.enemyBord[1][1];
                case 5:
                    return BattleManager.instance.board.enemyBord[2][1];
                case 6:
                    return BattleManager.instance.board.enemyBord[0][0];
                case 7:
                    return BattleManager.instance.board.enemyBord[1][0];
                case 8:
                    return BattleManager.instance.board.enemyBord[2][0];
            }
        }
        return -1;
    }
    public void checkSelection()
    {
        int width = pointMap.Length;
        int height = pointMap[0].Length;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (pointMap[positionX][positionY].points[0] != -1)
                    return;
                else
                    changeVertical(-1);
            }
            changeHorizontal(1);
        }
    }
}
