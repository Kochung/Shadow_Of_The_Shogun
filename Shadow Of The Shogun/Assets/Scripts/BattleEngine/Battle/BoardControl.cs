﻿using UnityEngine;
using System.Collections;

public class BoardControl
{
    public int [][] playerBord;
    public int [][] enemyBord;
    public BoardControl()
    {
        playerBord = new int[2][];
        playerBord[0] = new int[2];
        playerBord[1] = new int[2];

        enemyBord = new int [3][];
        enemyBord[0] = new int[3];
        enemyBord[1] = new int[3];
        enemyBord[2] = new int[3];
        
    }
    public void setParty(Party party)
    {
        playerBord[1][0] = party.TopRight.index;
        playerBord[0][0] = party.TopLeft.index;
        playerBord[1][1] = party.BottomRight.index;
        playerBord[0][1] = party.BottomLeft.index;
    }
    public void setEnemyParty(EnemyParty party)
    {
        enemyBord[0][2] = party.Possition1.index;
        enemyBord[1][2] = party.Possition2.index;
        enemyBord[2][2] = party.Possition3.index;
        enemyBord[0][1] = party.Possition4.index;
        enemyBord[1][1] = party.Possition5.index;
        enemyBord[2][1] = party.Possition6.index;
        enemyBord[0][0] = party.Possition7.index;
        enemyBord[1][0] = party.Possition8.index;
        enemyBord[2][0] = party.Possition9.index;

    }
    public void switchPlacesPlayer(int x1,int y1,int x2, int y2)
    {
        int temp = playerBord[x1][y1];
        playerBord[x1][y1] = playerBord[x2][y2];
        playerBord[x2][y2] = temp;
        placeCharacters(BattleManager.instance.characters);
    }

    public void switchPlacesEnemy(int x1, int y1, int x2, int y2)
    {
        int temp = enemyBord[x1][y1];
        enemyBord[x1][y1] = enemyBord[x2][y2];
        enemyBord[x2][y2] = temp;
        placeCharacters(BattleManager.instance.characters);
    }

    public void switchPlacesPlayerEnemy(int playerX, int playerY, int enemyX, int enemyY)
    {
        int temp = playerBord[playerX][playerY];
        playerBord[playerX][playerY] = enemyBord[enemyX][enemyY];
        enemyBord[enemyX][enemyY] = temp;
        placeCharacters(BattleManager.instance.characters);
    }
    public void remove(BattleCharacter character)
    {
        int index = character.index;

        bool suicide = BattleManager.instance.turnOrder.getCurrentTurnIndex() == character.index;
        if(suicide)
        {
            StateInformation.instance.suicide = true;
            StateInformation.instance.suicideName = character.name;
        }
        BattleManager.instance.characters.remove(character);
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if (enemyBord[i][j] == index)
                    enemyBord[i][j] = 0;
                if (enemyBord[i][j] > index)
                    enemyBord[i][j]--;
            }
        }
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (playerBord[i][j] == index)
                    playerBord[i][j] = 0;
                if (playerBord[i][j] > index)
                    playerBord[i][j]--;
            }
        }
        BattleManager.instance.cursor.restartState();
        BattleManager.instance.turnOrder.remove(index);

    }
    public void placeCharacters(CharacterList list)
    {
        placePlayer(list.list[1], -6.55f, 1.6f);

        placePlayer(list.list[playerBord[1][0]], -2.25f, 2.25f);
        placePlayer(list.list[playerBord[1][1]], -2.25f, 0f);
        placePlayer(list.list[playerBord[0][0]], -5f, 2.25f);
        placePlayer(list.list[playerBord[0][1]], -5f, 0f);

        placeEnemy(list.list[enemyBord[0][0]], 1, 3.3f);
        placeEnemy(list.list[enemyBord[1][0]], 3.8f, 3.3f);
        placeEnemy(list.list[enemyBord[2][0]], 6.6f, 3.3f);
        placeEnemy(list.list[enemyBord[0][1]], 1, 1f);
        placeEnemy(list.list[enemyBord[1][1]], 3.8f, 1f);
        placeEnemy(list.list[enemyBord[2][1]], 6.6f, 1f);
        placeEnemy(list.list[enemyBord[0][2]], 1, -1.3f);
        placeEnemy(list.list[enemyBord[1][2]], 3.8f, -1.3f);
        placeEnemy(list.list[enemyBord[2][2]], 6.6f, -1.3f);
    }
    private void placePlayer(BattleCharacter character, float x, float y)
    {
        if (!character.job.Equals(""))
        {
            character.character.transform.position = new Vector2(x, y);
            character.face(true);
        }
    }
    private void placeEnemy(BattleCharacter character, float x, float y)
    {
        if (!character.name.Equals(""))
        {
            character.character.transform.position = new Vector2(x, y);
            character.face(false);
        }
    }
    public Vector2 firstPlayerOpen()
    {
        for(int i=0;i<2;i++)
        {
            for(int j=0;j<2;j++)
            {
                if (playerBord[i][j] == 0)
                    return new Vector2(i,j);
            }
        }
        return new Vector2(-1,-1);
    }
    public Vector2 firstEnemyOpen()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (enemyBord[i][j] == 0)
                    return new Vector2(i, j);
            }
        }
        return new Vector2(-1, -1);
    }
    public Vector2 getPosition(int index)
    {
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (playerBord[i][j] == index)
                    return new Vector2(i, j);
            }
        }

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (enemyBord[i][j] == index)
                    return new Vector2(i, j);
            }
        }
        return new Vector2(-1, -1);
    }


    public bool havePlayerOpen()
    {
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (playerBord[i][j] == 0)
                    return true;
            }
        }
        return false;
    }
    public bool haveEnemyOpen()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (enemyBord[i][j] == 0)
                    return true;
            }
        }
        return false;
    }
}
