﻿using UnityEngine;
using System.Collections;

public class CharacterList
{
    public BattleCharacter[] list;
    public bool[] isEnemy;

    public CharacterList()
    {
        list = new BattleCharacter[15];
        isEnemy = new bool[15];
        for (int i = 0; i < 15; i++)
        {
            list[i] = new EmptyCharacter();
        }
    }
    public void clear()
    {
        for (int i = 0; i < 15; i++)
        {
            list[i] = new EmptyCharacter();
            isEnemy[i] = false;
        }
    }
    public void add(BattleCharacter character, bool enemy)
    {
        if (character.name.Equals(""))
            return;
        for (int i = 1; i < 15; i++)
        {
            if (list[i].name.Equals(""))
            {
                list[i] = character;
                isEnemy[i] = enemy;
                character.index = i;
                break;
            }
        }
    }
    public void remove(BattleCharacter character)
    {
        for (int i = character.index; i < 14; i++)
        {
            list[i] = list[i + 1];
            isEnemy[i] = isEnemy[i+1];
            list[i].index = i;
        }
        list[14] = new EmptyCharacter();
        isEnemy[14] = false;
    }
    public int getLength()
    {
        int counter = 0;
        for (int i = 1; i < 15; i++)
        {
            if (!list[i].name.Equals(""))
            {
                counter++;
            }
        }
        return counter;
    }
}

