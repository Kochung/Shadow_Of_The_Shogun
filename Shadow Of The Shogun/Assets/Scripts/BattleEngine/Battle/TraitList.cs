﻿using UnityEngine;
using System.Collections;

public class TraitList
{
    public Trait[] list;
    public static TraitList instance;
    public TraitList()
    {
        if (instance == null)
            instance = this;
        list = new Trait[11];
        list[0] = new NoTrait();
        list[1] = new MpRegenTrait();
        list[2] = new SprintTrait();
        list[3] = new Commanding();
        list[4] = new Courageous();
        list[5] = new FirstAid();
        list[6] = new Focus();
        list[7] = new Inspiring();
        list[8] = new Intimidating();
        list[9] = new Meditation();
        list[10] = new MindControl();
    }
    public Trait getCurrentCharicterTrait(int index)
    {
        return list[index];
    }
}
