﻿using UnityEngine;
using System.Collections;

public class Party
{
    public BattleCharacter TopLeft;
    public BattleCharacter TopRight;
    public BattleCharacter BottomLeft;
    public BattleCharacter BottomRight;

    public Party()
    {
        TopLeft = new EmptyCharacter();
        TopRight = new EmptyCharacter();
        BottomLeft = new EmptyCharacter();
        BottomRight = new EmptyCharacter();
    }

    public void addAll(CharacterList list)
    {
        list.add(TopLeft, false);
        list.add(TopRight, false);
        list.add(BottomLeft, false);
        list.add(BottomRight, false);
    }
    public bool Wipe()
    {
        if (TopLeft.Hp != 0)
            return false;
        if (TopRight.Hp != 0)
            return false;
        if (BottomLeft.Hp != 0)
            return false;
        if (BottomRight.Hp != 0)
            return false;
        return true;
    }
    public void set(BattleCharacter character,int x,int y)
    {
        if (y == 0)
        {
            if(x == 0)
                TopLeft = character;
            if (x == 1)
                TopRight = character;
        }
        if (y == 1)
        {
            if (x == 0)
                BottomLeft = character;
            if(x == 1)
                BottomRight = character;
        }
    }
    public BattleCharacter getBetrayer()
    {
        if (TopLeft.Hp != 0)
            return TopLeft;
        if (TopRight.Hp != 0)
            return TopRight;
        if (BottomLeft.Hp != 0)
            return BottomLeft;
        if (BottomRight.Hp != 0)
            return BottomRight;
        return null;
    }
    public void reInstantiate()
    {

        if (TopLeft.Hp != 0)
            TopLeft.reInstantiate();
        else
            TopLeft = new EmptyCharacter();
        if (TopRight.Hp != 0)
            TopRight.reInstantiate();
        else
            TopRight = new EmptyCharacter();
        if (BottomLeft.Hp != 0)
            BottomLeft.reInstantiate();
        else
            BottomLeft = new EmptyCharacter();
        if (BottomRight.Hp != 0)
            BottomRight.reInstantiate();
        else
            BottomRight = new EmptyCharacter();
    }
    public void cleanStatus()
    {
        if (TopLeft.Hp != 0)
            TopLeft.clearStatus();
        if (TopRight.Hp != 0)
            TopRight.clearStatus();
        if (BottomLeft.Hp != 0)
            BottomLeft.clearStatus();
        if (BottomRight.Hp != 0)
            BottomRight.clearStatus();

    }
    public void afterBattleHeal()
    {
        if (TopLeft.Hp != 0)
        {
            TopLeft.Heal(TopLeft.MaxHp/4);
            TopLeft.gainMp(TopLeft.MaxMp / 4);
        }
        if (TopRight.Hp != 0)
        {
            TopRight.Heal(TopRight.MaxHp / 4);
            TopRight.gainMp(TopRight.MaxMp / 4);
        }
        if (BottomLeft.Hp != 0)
        {
            BottomLeft.Heal(BottomLeft.MaxHp / 4);
            BottomLeft.gainMp(BottomLeft.MaxMp / 4);
        }
        if (BottomRight.Hp != 0)
        {
            BottomRight.Heal(BottomRight.MaxHp / 4);
            BottomRight.gainMp(BottomRight.MaxMp / 4);
        }
    }
}
