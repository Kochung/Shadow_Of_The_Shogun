﻿using UnityEngine;
using System.Collections;

public class EnemyParty 
{
    public BattleCharacter Possition1;
    public BattleCharacter Possition2;
    public BattleCharacter Possition3;
    public BattleCharacter Possition4;
    public BattleCharacter Possition5;
    public BattleCharacter Possition6;
    public BattleCharacter Possition7;
    public BattleCharacter Possition8;
    public BattleCharacter Possition9;

    public EnemyParty()
    {
        Possition1 = new EmptyCharacter();
        Possition2 = new EmptyCharacter();
        Possition3 = new EmptyCharacter();
        Possition4 = new EmptyCharacter();
        Possition5 = new EmptyCharacter();
        Possition6 = new EmptyCharacter();
        Possition7 = new EmptyCharacter();
        Possition8 = new EmptyCharacter();
        Possition9 = new EmptyCharacter();
    }
    public void clear()
    {
        Possition1 = new EmptyCharacter();
        Possition2 = new EmptyCharacter();
        Possition3 = new EmptyCharacter();
        Possition4 = new EmptyCharacter();
        Possition5 = new EmptyCharacter();
        Possition6 = new EmptyCharacter();
        Possition7 = new EmptyCharacter();
        Possition8 = new EmptyCharacter();
        Possition9 = new EmptyCharacter();
    }
    public void addAll(CharacterList list)
    {
        list.add(Possition1, true);
        list.add(Possition2, true);
        list.add(Possition3, true);
        list.add(Possition4, true);
        list.add(Possition5, true);
        list.add(Possition6, true);
        list.add(Possition7, true);
        list.add(Possition8, true);
        list.add(Possition9, true);
    }
    public bool Wipe()
    {
        if (Possition1.Hp != 0)
            return false;
        if (Possition2.Hp != 0)
            return false;
        if (Possition3.Hp != 0)
            return false;
        if (Possition4.Hp != 0)
            return false;
        if (Possition5.Hp != 0)
            return false;
        if (Possition6.Hp != 0)
            return false;
        if (Possition7.Hp != 0)
            return false;
        if (Possition8.Hp != 0)
            return false;
        if (Possition9.Hp != 0)
            return false;
        return true;
    }
    public void set(BattleCharacter character, int x, int y)
    {
        if (y == 0)
        {
            switch(x)
            {
                case 0:
                    Possition7 = character;
                    break;
                case 1:
                    Possition8 = character;
                    break;
                case 2:
                    Possition9 = character;
                    break;
            }
        }
        if (y == 1)
        {
            switch (x)
            {
                case 0:
                    Possition4 = character;
                    break;
                case 1:
                    Possition5 = character;
                    break;
                case 2:
                    Possition6 = character;
                    break;
            }
        }
        if (y == 2)
        {
            switch (x)
            {
                case 0:
                    Possition1 = character;
                    break;
                case 1:
                    Possition2 = character;
                    break;
                case 2:
                    Possition3 = character;
                    break;
            }
        }
    }

    public BattleCharacter getCapture()
    {
        if (Possition1.Hp != 0)
            return Possition1;
        if (Possition2.Hp != 0)
            return Possition2;
        if (Possition3.Hp != 0)
            return Possition3;
        if (Possition4.Hp != 0)
            return Possition4;
        if (Possition5.Hp != 0)
            return Possition5;
        if (Possition6.Hp != 0)
            return Possition6;
        if (Possition7.Hp != 0)
            return Possition7;
        if (Possition8.Hp != 0)
            return Possition8;
        if (Possition9.Hp != 0)
            return Possition9;
        return null;
    }
    public void setRandomFreeSlot(BattleCharacter character)
    {
        for (int i = 0; i < 200; i++)
        {
            int number = BattleManager.instance.random.Next(0, 9);
            if (number == 0 && Possition1.job.Equals(""))
            {
                Possition1 = character;
                break;
            }
            else if (number == 1 && Possition2.job.Equals(""))
            {
                Possition2 = character;
                break;
            }
            else if (number == 2 && Possition3.job.Equals(""))
            {
                Possition3 = character;
                break;
            }
            else if (number == 3 && Possition4.job.Equals(""))
            {
                Possition4 = character;
                break;
            }
            else if (number == 4 && Possition5.job.Equals(""))
            {
                Possition5 = character;
                break;
            }
            else if (number == 5 && Possition6.job.Equals(""))
            {
                Possition6 = character;
                break;
            }
            else if (number == 6 && Possition7.job.Equals(""))
            {
                Possition7 = character;
                break;
            }
            else if (number == 7 && Possition8.job.Equals(""))
            {
                Possition8 = character;
                break;
            }
            else if (number == 8 && Possition9.job.Equals(""))
            {
                Possition9 = character;
                break;
            }
        }
    }
}
