﻿using UnityEngine;
using System.Collections;

public class ItemBag
{
    public Item[] bag;
    public int size;
    public ItemBag()
    {
        size = 14;
        bag = new Item[size];
        bag[0] = new Bread();
        bag[1] = new Meat();
        bag[2] = new Cake();

        bag[3] = new Water();
        bag[4] = new Tea();
        bag[5] = new Honey();

        bag[6] = new GoldBar();
        bag[7] = new Emerald();
        bag[8] = new Diamond();

        bag[9] = new PaperBomb();
        bag[10] = new EscapeLadder();

        bag[11] = new LoyaltyZeroItem();
        bag[12] = new PartyLoyaltyZeroItem();

        bag[size - 1] = new NoneItem();
    }
    public void addItem(string name, int amount)
    {
        for (int i = 0; i < size; i++)
        {
            if(bag[i].name.Equals(name))
            {
                bag[i].addItem(amount);
                break;
            }
        }
    }
    public bool useItem(string name, int amount)
    {
        for (int i = 0; i < size; i++)
        {
            if (bag[i].name.Equals(name))
            {
                return bag[i].useItem();
            }
        }
        return false;
    }
    public void addItem(int index, int amount)
    {
        bag[index].addItem(amount);
    }
    public bool useItem(int index, int amount)
    {
        return bag[index].useItem();
    }
    public void StartingItems()
    {
        bag[0].addItem(32);
        bag[1].addItem(24);
        bag[2].addItem(12);

        bag[3].addItem(32);
        bag[4].addItem(24);
        bag[5].addItem(12);

        bag[6].addItem(20);
        bag[7].addItem(8);
        bag[8].addItem(4);
        bag[9].addItem(20);
        bag[10].addItem(12);

        bag[11].addItem(1);
        bag[12].addItem(1);
    }
    public Item getItem(string name)
    {
        for (int i = 0; i < size; i++)
            if (bag[i].name.Equals(name))
                return bag[i];
        return null;
    }
}
