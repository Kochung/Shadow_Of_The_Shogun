﻿using UnityEngine;
using System.Collections;

public class SkillList
{
    public Skill[] list;
    public static SkillList instance;
    public SkillList()
    {
        if (instance == null)
            instance = this;
        list = new Skill[28];
        list[0] = new NoSkill();
        list[1] = new HitAllSkill();
        list[2] = new RegenerationSkill();
        list[3] = new SheathSword();
        list[4] = new QuickDraw();
        list[5] = new GuardCrush();
        list[6] = new AnkleShot();
        list[7] = new PoisonShot();
        list[8] = new BluntShot();
        list[9] = new PrayerBeads();
        list[10] = new BattleChant();
        list[11] = new SpiritChant();
        list[12] = new Heal();
        list[13] = new Sweep();
        list[14] = new Pierce();
        list[15] = new Vigor();

        list[16] = new Drink();
        list[17] = new Berserk();
        list[18] = new Rend();

        list[19] = new Fly();
        list[20] = new ShieldBash();

        list[21] = new Charge();
        list[22] = new Screech();
        list[23] = new SnakeStare();

        list[24] = new Recruit();
        list[25] = new Berate();
        list[26] = new NegotiateAll();
        list[27] = new MotivateAll();
    }
    public Skill getCurrentCharicterSkill(int index)
    {
        return list[BattleManager.instance.characters.list[BattleManager.instance.turnOrder.getCurrentTurnIndex()].skills[index]];
    }
}