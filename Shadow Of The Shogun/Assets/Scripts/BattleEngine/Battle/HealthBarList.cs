﻿using UnityEngine;
using System.Collections;

public class HealthBarList
{
    private HealthBar[] bars;
    public HealthBarList()
    {
        reset();
    }
    public void reset()
    {
        bars = new HealthBar[13];
        GameObject tempParent = GameObject.Find("HpBars");
        for (int i = 0; i < 13; i++)
        {
            bars[i] = tempParent.transform.GetChild(i).transform.GetComponent<HealthBar>();
            bars[i].disable();
        }
        BoardControl board = BattleManager.instance.board;
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (board.playerBord[i][j] != 0)
                    enable(getPlayerIndex(i, j), BattleManager.instance.characters.list[board.playerBord[i][j]]);
            }
        }
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (board.enemyBord[i][j] != 0)
                    enable(getEnemyIndex(i, j), BattleManager.instance.characters.list[board.enemyBord[i][j]]);
            }
        }
    }
	public void enable(int index, BattleCharacter character)
    {
        bars[index].enable();
        bars[index].setCharacter(character);
    }
    public void update (int index, BattleCharacter character)
    {
        bars[index].UpdateCurrentHealth(character.Hp);
    }
    public int getPlayerIndex(int x, int y)
    {
        if (x == 1 && y == 0)
            return 0;
        if (x == 1 && y == 1)
            return 1;
        if (x == 0 && y == 0)
            return 2;
        if (x == 0 && y == 1)
            return 3;
        return -1;
    }
    public int getEnemyIndex(int x,int y)
    {
        if (x == 0 && y == 2)
            return 4;
        if (x == 1 && y == 2)
            return 5;
        if (x == 2 && y == 2)
            return 6;
        if (x == 0 && y == 1)
            return 7;
        if (x == 1 && y == 1)
            return 8;
        if (x == 2 && y == 1)
            return 9;
        if (x == 0 && y == 0)
            return 10;
        if (x == 1 && y == 0)
            return 11;
        if (x == 2 && y == 0)
            return 12;
        return 0;
    }
    public void update()
    {
        for (int i = 0; i < 13; i++)
        {
            bars[i].updateBar();
        }
    }
}
