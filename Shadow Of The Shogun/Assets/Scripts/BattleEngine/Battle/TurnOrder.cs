﻿using UnityEngine;
using System.Collections;

public class TurnOrder
{
    int[] order;
    int currentTurn;
    int length;
    GameObject PortraitSelect;
    public TurnOrder()
    {

    }
    public  void reInstantiate()
    {
        PortraitSelect = GameObject.Find("PortraitSelect");
    }
    public void Order(CharacterList list)
    {
        length = list.getLength();
        order = new int[length];
        for (int i = 0; i<length; i++)
        {
            order[i] = i + 1;
        }
        for (int i = 0; i < length; i++)
        {
            int topSpeed = -1;
            int index = 0;
            for (int j = i; j < length; j++)
            {
                int speed = list.list[order[j]].Spd;
                if (speed > topSpeed)
                {
                    topSpeed = speed;
                    index = j;
                }
                if (speed == topSpeed)
                {
                    int num = BattleManager.instance.random.Next(0, 2);
                    if (num == 1)
                        index = j;
                }
            }
            int hold = order[i];
            order[i] = order[index];
            order[index] = hold;
            currentTurn = 0;
        }
    }
    public void displayPortraits(CharacterList list)
    {
        float x = 8.25f;
        float start = (length / 2) * .7f;
        if (length % 2 == 0)
            start -= .35f;
        for (int i = 0; i < length; i++)
        {
            placePortrait(list.list[order[i]], x, start);
            start -= 0.70f;
        }
        if (!StateInformation.instance.suicide)
            changeSelector();
        else
            hideSelector();
    }
    public void changeSelector()
    {
        float y = (length / 2) * .7f;
        float x = 8.25f; 
        if (length % 2 == 0)
            y -= .35f;
        y -= currentTurn * .70f;
        if (currentTurn == length)
            hideSelector();
        else
            PortraitSelect.transform.position = new Vector2(x,y);
    }
    public void hideSelector()
    {
        PortraitSelect.transform.position = new Vector2(999, 999);
    }
    private void placePortrait(BattleCharacter character, float x, float y)
    {
        if (!character.name.Equals(""))
        {
            character.portrait.transform.position = new Vector2(x, y);
        }
    }
    public bool nextTurn()
    {
        currentTurn++;
        changeSelector();
        return currentTurn == length;
    }
    public int getCurrentTurnIndex()
    {
        return order[currentTurn];
    }
    public void remove(int index)
    {
        int trueIndex = 0;
        int counter = 0;
        int [] newOrder = new int [length-1];
        for(int i=0;i<length;i++)
        {
            if(order[i] == index)
            {
                trueIndex = i;
            }
            else
            {
                if (order[i] > index)
                    newOrder[counter] = order[i]-1;
                else
                    newOrder[counter] = order[i];
                counter++;
            }
        }
        length--;
        order = newOrder;
        if (trueIndex < currentTurn)
            currentTurn--;
        if (StateInformation.instance.suicide)
            currentTurn--;
        displayPortraits(BattleManager.instance.characters);
    }
}
