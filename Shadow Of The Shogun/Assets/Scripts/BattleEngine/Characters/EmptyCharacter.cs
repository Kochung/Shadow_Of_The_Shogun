﻿using UnityEngine;
using System.Collections;

public class EmptyCharacter : BattleCharacter
{
    public EmptyCharacter()
    {
        name = "";
        job = "";
        Hp = 0;
        index = 0;
        goldMin = 0;
        goldMax = 0;
    }
}

