﻿using UnityEngine;
using System.Collections;

public class Wraith: BattleCharacter
{
    public Wraith(string Name)
    {
        name = Name;
        job = "Wraith";

        setBaseStat();
        loyalty = 0;
        IV.roll();

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("WraithPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Wraith"));

        for (int i = 0; i < 100; i++)
        {
            levelUp();
        }
        Mp = MaxMp;
        Hp = MaxHp;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("WraithPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Wraith"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 80;
        baseStat.Mp = 80;
        baseStat.Atk = 90;
        baseStat.Def = 83;
        baseStat.MAtk = 83;
        baseStat.MDEF = 83;
        baseStat.Spd = 78;
    }
}
