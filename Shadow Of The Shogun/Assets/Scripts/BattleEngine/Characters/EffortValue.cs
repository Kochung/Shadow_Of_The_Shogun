﻿using UnityEngine;
using System.Collections;

public class EffortValue
{
    public const int MaxTotal = 128;
    public const int MaxIndividual = 64;
    public int Hp;
    public int Mp;
    public int Atk;
    public int Def;
    public int MAtk;
    public int MDEF;
    public int Spd;
    public EffortValue()
    {
        Hp = MaxIndividual;
        Mp = MaxIndividual;
        Atk = MaxIndividual;
        Def = MaxIndividual;
        MAtk = MaxIndividual;
        MDEF = MaxIndividual;
        Spd = MaxIndividual;
    }
    public int getTotal()
    {
        return Hp + Mp + Atk + Def + MAtk + MDEF + Spd;
    }
    public void load(int[] values)
    {
        Hp = values[0];
        Mp = values[1];
        Atk = values[2];
        Def = values[3];
        MAtk = values[4];
        MDEF = values[5];
        Spd = values[6];
    }
}
