﻿using UnityEngine;
using System.Collections;
using System;

public abstract class BattleCharacter
{
    public string name;
    public string job;
    public int level;
    public int Hp;
    public int MaxHp;
    public int Mp;
    public int MaxMp;
    public int Atk;
    public int Def;
    public int MAtk;
    public int MDef;
    public int Spd;
    public int RealAtk;
    public int RealDef;
    public int RealMAtk;
    public int RealMDef;
    public int RealSpd;
    public int loyalty;
    public int [] traits;
    public int [] skills;
    public bool Hit;
    public StatusEffect[] statusEffects;
    public int goldMin;
    public int goldMax;

    public bool immune;
    public bool guarding;
    public bool cannotRecruit;
    public bool noAttack;

    public BaseStat baseStat;
    public IndividualValue IV;
    public EffortValue EV;

    public int index;
    public int flip;

    public GameObject portrait;
    public GameObject character;

    public Action attackAction;
    public Action attackSelectAction;
    public Action enemyAttackAction;

    public BattleCharacter()
    {
        baseStat = new BaseStat();
        IV = new IndividualValue();
        EV = new EffortValue();
        guarding = false;
        traits = new int[6];
        skills = new int[6];
        statusEffects = new StatusEffect[10];
        for (int i = 0; i < statusEffects.Length; i++)
        {
            statusEffects[i] = new NoStatusEffect();
        }
        Hit = false;
    }
    public void levelUpTo(int newLevel)
    {
        level = 0;
        for (int i = 0; i < newLevel; i++)
        {
            levelUp();
        }
        Hp = MaxHp;
        Mp = MaxMp;
    }
    public void levelUp()
    {
        level++;
        MaxHp = determinHpOrMp(baseStat.Hp, IV.Hp, EV.Hp);
        MaxMp = baseStat.Mp;
        Atk = determineStat(baseStat.Atk, IV.Atk, EV.Atk);
        Def = determineStat(baseStat.Def, IV.Def, EV.Def);
        MAtk = determineStat(baseStat.MAtk, IV.MAtk, EV.MAtk);
        MDef = determineStat(baseStat.MDEF, IV.MDEF, EV.MDEF);
        Spd = determineStat(baseStat.Spd, IV.Spd, EV.Spd);

        RealAtk = Atk;
        RealDef = Def;
        RealMAtk = MAtk;
        RealMDef = MDef;
        RealSpd = Spd;
    }
    public int determinHpOrMp(int baseValue,int ivValue,int evValue)
    {
        return (int)(((2 * baseValue + ivValue + evValue) * level) / 100.0) + level + 10;
    }
    public int determineStat(int baseValue, int ivValue, int evValue)
    {
        return (int)(((2 * baseValue + ivValue + evValue) * level) / 100.0) + 5;
    }
    public void face(bool right)
    {
        if(!right && flip == 0)
        {
            character.transform.localScale = new Vector3(-character.transform.localScale.x, character.transform.localScale.y, character.transform.localScale.z);
            portrait.transform.localScale = new Vector3(-portrait.transform.localScale.x, portrait.transform.localScale.y, portrait.transform.localScale.z);
            flip = 1;
        }
        else if(right && flip == 1)
        {
            character.transform.localScale = new Vector3(-character.transform.localScale.x, character.transform.localScale.y, character.transform.localScale.z);
            portrait.transform.localScale = new Vector3(-portrait.transform.localScale.x, portrait.transform.localScale.y, portrait.transform.localScale.z);
            flip = 0;
        }
    }
    public void Damage(int damage)
    {
        Hp-=damage;
        Hit = true;
        if (Hp <= 0)
        {
            Hp = 0;
        }
    }
    public void NoHitDamage(int damage)
    {
        Hp -= damage;
        if (Hp <= 0)
        {
            Hp = 0;
        }
    }
    public void Heal(int amount)
    {
        Hp += amount;
        if (Hp > MaxHp)
        {
            Hp = MaxHp;
        }
    }
    public void useMp(int amount)
    {
        Mp -= amount;
        if (Mp <= 0)
        {
            Mp = 0;
        }
    }
    public void gainMp(int amount)
    {
        Mp += amount;
        if (Mp > MaxMp)
        {
            Mp = MaxMp;
        }
    }
    public void addLoyalty(int amount)
    {
        loyalty += amount;
        if (loyalty > 100)
            loyalty = 100;
        if (loyalty < 0)
            loyalty = 0;
    }
    public void die()
    {
        BattleManager.instance.board.remove(this);
        GameObject.Destroy(portrait.gameObject);
        GameObject.Destroy(character.gameObject);

    }
    public int Capture()
    {
        if(cannotRecruit)
            return -1;
        double limit = Math.Pow(10, -(100-loyalty) / 80.0) - .1;
        double roll = BattleManager.instance.random.NextDouble();
        if (roll >= limit)
            return -1;
        Vector2 newPlace = BattleManager.instance.board.firstPlayerOpen();
        Vector2 oldPlace = BattleManager.instance.board.getPosition(index);
        if (newPlace.x != -1)
        {
            BattleManager.instance.characters.isEnemy[index] = false;
            BattleManager.instance.board.switchPlacesPlayerEnemy((int)newPlace.x, (int)newPlace.y, (int)oldPlace.x, (int)oldPlace.y);
            BattleManager.instance.board.placeCharacters(BattleManager.instance.characters);
            BattleManager.instance.party.set(this, (int)newPlace.x, (int)newPlace.y);
            BattleManager.instance.enemyParty.set(new EmptyCharacter(), (int)oldPlace.x, (int)oldPlace.y);
            BattleManager.instance.healthBars.reset();
            face(true);
            return 1;
        }
        return 0;
    }
    public int Betray()
    {
        double limit = Math.Pow(10, -loyalty / 80.0) - .1;
        double roll = BattleManager.instance.random.NextDouble();
        if (roll >= limit)
            return -1;
        Vector2 newPlace = BattleManager.instance.board.firstEnemyOpen();
        Vector2 oldPlace = BattleManager.instance.board.getPosition(index);
        if(newPlace.x != -1)
        {
            BattleManager.instance.characters.isEnemy[index] = true;
            BattleManager.instance.board.switchPlacesPlayerEnemy((int)oldPlace.x, (int)oldPlace.y, (int)newPlace.x, (int)newPlace.y);
            BattleManager.instance.board.placeCharacters(BattleManager.instance.characters);
            BattleManager.instance.enemyParty.set(this, (int)newPlace.x, (int)newPlace.y);
            BattleManager.instance.party.set(new EmptyCharacter(), (int)oldPlace.x, (int)oldPlace.y);
            BattleManager.instance.healthBars.reset();
            face(false);
            return 1;
        }
        return 0;
    }
    public int forceBetray()
    {
        Vector2 newPlace = BattleManager.instance.board.firstEnemyOpen();
        Vector2 oldPlace = BattleManager.instance.board.getPosition(index);
        if (newPlace.x != -1)
        {
            BattleManager.instance.characters.isEnemy[index] = true;
            BattleManager.instance.board.switchPlacesPlayerEnemy((int)oldPlace.x, (int)oldPlace.y, (int)newPlace.x, (int)newPlace.y);
            BattleManager.instance.board.placeCharacters(BattleManager.instance.characters);
            BattleManager.instance.enemyParty.set(this, (int)newPlace.x, (int)newPlace.y);
            BattleManager.instance.party.set(new EmptyCharacter(), (int)oldPlace.x, (int)oldPlace.y);
            BattleManager.instance.healthBars.reset();
            face(false);
            return 1;
        }
        return 0;
    }
    public string getInformationLeft()
    {
        if (job.Equals(""))
            return "";
        string info = "";
        info += name + "\n";
        info += job + "\n";
        info += "Hp:" + Hp + "/" + MaxHp + "\n";
        info += "Mp:" + Mp + "/" + MaxMp + "\n";
        return info;
    }

    public string getInformationRight()
    {
        if (job.Equals(""))
            return "";
        string info = "";
        info += "Atk:  " + Atk +"\n";
        info += "Def:  " + Def + "\n";
        info += "MAtk: " + MAtk + "\n";
        info += "MDEF: " + MDef + "\n";
        info += "Spd:  " + Spd + "\n";
        info += "Loyalty: " + loyalty + "\n";
        return info;
    }
    public string[] getInformationList()
    {
        string [] info = new string [10];
        info[0] = name;
        info[1] = job;
        info[2] = "Hp:" + Hp + "/" + MaxHp;
        info[3] = "Mp:" + Mp + "/" + MaxMp;
        info[4] = "Atk:  " + Atk;
        info[5] = "Def:  " + Def;
        info[6] = "MAtk: " + MAtk;
        info[7] = "MDEF: " + MDef;
        info[8] = "Spd:  " + Spd;
        info[9] = "Loyalty: " + loyalty;
        return info;
    }
    public double getDamage(BattleCharacter attacker, int baseAttack, bool physical, bool crit)
    {
        double damage = 0;
        if (physical)
            damage = ((2 * attacker.level + 10) / 250.0) * (attacker.Atk / (double)Def) * baseAttack + 2;
        else
            damage = ((2 * attacker.level + 10) / 250.0) * (attacker.MAtk / (double)MDef) * baseAttack + 2;
        double range = BattleManager.instance.random.Next(85,101)/100.0;
        if (crit)
            range *= 1.5;
        if (guarding)
            range *= 0.5;
        return (damage * range);
    }
    public bool getCrit()
    {
        return BattleManager.instance.random.Next(0, 16) == 0;
    }
    public virtual void reInstantiate(){}
    public virtual string attackInfo()
    {
        return "Standard Attack";
    }
    public void runStatusSkill(ref double damage, bool physical, ref bool crit, ref bool enemyGuarding)
    {
        int length = statusEffects.Length;
        for (int i = 0; i < length; i++)
        {
            if (statusEffects[i].checkType(StatusEffect.SkillType))
            {
                damage = statusEffects[i].runAttack(ref damage, physical, ref crit, ref enemyGuarding);
            }
        }
    }

    public void runStatusAttack(ref double damage, bool physical, ref bool crit, ref bool enemyGuarding)
    {
        int length = statusEffects.Length;
        for (int i = 0; i < length; i++)
        {
            if (statusEffects[i].checkType(StatusEffect.AttackType))
            {
                damage = statusEffects[i].runAttack(ref damage, physical, ref crit, ref enemyGuarding);
            }
        }
    }
    public void cleanStatusEffects()
    {
        int length = statusEffects.Length;
        StatusEffect [] newStatusEffect = new StatusEffect[length];
        int counter = 0;
        for (int i = 0; i < length; i++)
        {
            if(statusEffects[i].turnCount==0)
            {
                statusEffects[i].dropoff(this);
            }
            else
            {
                if(!statusEffects[i].name.Equals(""))
                {
                    newStatusEffect[counter] = statusEffects[i];
                    counter++;
                }
            }
        }
        for(int i=counter;i<length;i++)
        {
            newStatusEffect[i] = new NoStatusEffect();
        }
        statusEffects = newStatusEffect;
    }
    public void addStatus(StatusEffect effect)
    {
        int length = statusEffects.Length;
        for (int i = 0; i < length; i++)
        {
            if (statusEffects[i].name.Equals(effect.name))
            {
                statusEffects[i].Stack(effect);
                break;
            }
            else if (statusEffects[i].name.Equals(""))
            {
                if (effect.checkType(StatusEffect.PassiveType))
                    effect.run(this);
                statusEffects[i] = effect;
                break;
            }
        }
    }
    public int rollGold()
    {
        return BattleManager.instance.random.Next(goldMin, goldMax + 1);
    }
    public void clearStatus()
    {
        int length = statusEffects.Length;
        StatusEffect[] newStatusEffect = new StatusEffect[length];
        for (int i = 0; i < length; i++)
        {
            statusEffects[i].dropoff(this);
            newStatusEffect[i] = new NoStatusEffect();
        }
        statusEffects = newStatusEffect;
    }
    public string getStatusEffects()
    {
        string returnString = "";
        if (guarding)
            returnString += "Guarding\n";
        int length = statusEffects.Length;
        for (int i = 0; i < length; i++)
        {
            if(!statusEffects[i].name.Equals(""))
                returnString += statusEffects[i].name + " " + statusEffects[i].turnCount + " turns\n";
        }
        return returnString;
    }
    public string getStatusEffectsRight()
    {
        string returnString = "";
        int length = statusEffects.Length;
        int count = 0;
        if (guarding)
            count++;
        for (int i = 0; i < length; i++)
        {
            if (!statusEffects[i].name.Equals(""))
            {
                count++;
                if(count > 6)
                {
                    returnString += statusEffects[i].name + " " + statusEffects[i].turnCount + " turns\n";
                }
            }
        }
        return returnString;
    }
    public int getStatusEffectsFontSize()
    {
        int count = 0;
        if (guarding)
            count++;
        int length = statusEffects.Length;
        for (int i = 0; i < length; i++)
        {
            if (!statusEffects[i].name.Equals(""))
                count++;
            if (count == 5)
                return 16;
        }
        return 24;
    }
    public StatusEffect getStatusEffect(string name)
    {
        int length = statusEffects.Length;
        for (int i = 0; i < length; i++)
        {
            if (statusEffects[i].name.Equals(name))
                return statusEffects[i];
        }
        return new NoStatusEffect();
    }
    public void randomTraits()
    {
        int number = 2;
        for (int i = 0; i < number; i++)
        {
            for(int j=0;j<100;j++)
            {
                int index = BattleManager.instance.random.Next(1, 10);
                if (checkTrait(index))
                {
                    traits[i] = index;
                    break;
                }
            }
        }
    }
    public bool checkTrait(int value)
    {
        for(int i=0;i<traits.Length;i++)
        {
            if (traits[i] == value)
                return false;
        }
        return true;
    }
}
