﻿using UnityEngine;
using System.Collections;

public class IndividualValue
{
    public int Hp;
    public int Mp;
    public int Atk;
    public int Def;
    public int MAtk;
    public int MDEF;
    public int Spd;

    public void roll()
    {
        Hp = BattleManager.instance.random.Next(0,32);
        Mp = BattleManager.instance.random.Next(0, 32);
        Atk = BattleManager.instance.random.Next(0, 32);
        Def = BattleManager.instance.random.Next(0, 32);
        MAtk = BattleManager.instance.random.Next(0, 32);
        MDEF = BattleManager.instance.random.Next(0, 32);
        Spd = BattleManager.instance.random.Next(0, 32);
    }
    public void load(int [] values)
    {
        Hp = values[0];
        Mp = values[1];
        Atk = values[2];
        Def = values[3];
        MAtk = values[4];
        MDEF = values[5];
        Spd = values[6];
    }
}

