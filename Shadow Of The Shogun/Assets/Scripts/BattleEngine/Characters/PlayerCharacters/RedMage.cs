﻿using UnityEngine;
using System.Collections;

public class RedMage : BattleCharacter
{

    public RedMage(string Name)
    {
        name = Name;
        job = "RedMage";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 1;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("RedMagePortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("RedMage"));

        for (int i = 0; i < 100; i++)
        {
            levelUp();
        }
        Mp = MaxMp;
        Hp = MaxHp;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("RedMagePortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("RedMage"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 85;
        baseStat.Mp = 80;
        baseStat.Atk = 115;
        baseStat.Def = 80;
        baseStat.MAtk = 105;
        baseStat.MDEF = 80;
        baseStat.Spd = 50;
    }
}