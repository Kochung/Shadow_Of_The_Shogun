﻿using UnityEngine;
using System.Collections;

public class Shogun: BattleCharacter
{
    public Shogun(string Name)
    {
        name = Name;
        job = "Shogun";
        Hp = 10;
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("ShogunPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Shogun"));
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("ShogunPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Shogun"));
    }
}
