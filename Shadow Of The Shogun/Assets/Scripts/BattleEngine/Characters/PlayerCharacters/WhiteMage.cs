﻿using UnityEngine;
using System.Collections;

public class WhiteMage: BattleCharacter
{
    public WhiteMage(string Name)
    {
        name = Name;
        job = "WhiteMage";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 1;
        skills[1] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("WhiteMagePortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("WhiteMage"));

        for (int i = 0; i < 100; i++)
        {
            levelUp();
        }
        Mp = MaxMp;
        Hp = MaxHp;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("WhiteMagePortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("WhiteMage"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 255;
        baseStat.Mp = 150;
        baseStat.Atk = 10;
        baseStat.Def = 10;
        baseStat.MAtk = 75;
        baseStat.MDEF = 135;
        baseStat.Spd = 55;
    }
}
