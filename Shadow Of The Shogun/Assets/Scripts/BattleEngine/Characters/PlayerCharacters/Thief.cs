﻿using UnityEngine;
using System.Collections;

public class Thief : BattleCharacter
{

    public Thief(string Name)
    {
        name = Name;
        job = "Thief";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 1;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("ThiefPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Thief"));

        for (int i = 0; i < 100; i++)
        {
            levelUp();
        }
        Mp = MaxMp;
        Hp = MaxHp;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("ThiefPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Thief"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 70;
        baseStat.Mp = 50;
        baseStat.Atk = 120;
        baseStat.Def = 65;
        baseStat.MAtk = 45;
        baseStat.MDEF = 85;
        baseStat.Spd = 125;
    }
}
