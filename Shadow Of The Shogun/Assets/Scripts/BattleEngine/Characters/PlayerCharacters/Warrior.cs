﻿using UnityEngine;
using System.Collections;

public class Warrior : BattleCharacter
{
    public Warrior(string Name)
    {
        name = Name;
        job = "Warrior";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 1;
        traits[0] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("WarriorPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Warrior"));

        for(int i=0;i<100;i++)
        {
            levelUp();
        }
        Mp = MaxMp;
        Hp = MaxHp;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("WarriorPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Warrior"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 80;
        baseStat.Mp = 80;
        baseStat.Atk = 90;
        baseStat.Def = 83;
        baseStat.MAtk = 83;
        baseStat.MDEF = 83;
        baseStat.Spd = 78;
    }
}
