﻿using UnityEngine;
using System.Collections;

public class BlackMage : BattleCharacter
{
    
    public BlackMage(string Name)
    {
        name = Name;
        job = "BlackMage";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 1;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("BlackMagePortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("BlackMage"));

        for (int i = 0; i < 100; i++)
        {
            levelUp();
        }
        Mp = MaxMp;
        Hp = MaxHp;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("BlackMagePortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("BlackMage"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 75;
        baseStat.Mp = 180;
        baseStat.Atk = 70;
        baseStat.Def = 72;
        baseStat.MAtk = 114;
        baseStat.MDEF = 100;
        baseStat.Spd = 104;
    }
}
