﻿using UnityEngine;
using System.Collections;

public class Nue : BattleCharacter
{
    public Nue(string Name)
    {
        name = Name;
        job = "Nue";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 21;
        skills[1] = 22;
        skills[2] = 23;
        randomTraits();
        //        traits[0] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("NuePortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Nue"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.soundEffect = "blunt_hit.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        tempSelect.type = CursorControl.EnemyFrontSelectState;
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.soundEffect = "blunt_hit.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("NuePortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Nue"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 120;
        baseStat.Mp = 70;
        baseStat.Atk = 135;
        baseStat.Def = 70;
        baseStat.MAtk = 90;
        baseStat.MDEF = 70;
        baseStat.Spd = 75;

        goldMin = 9;
        goldMax = 12;
    }
}