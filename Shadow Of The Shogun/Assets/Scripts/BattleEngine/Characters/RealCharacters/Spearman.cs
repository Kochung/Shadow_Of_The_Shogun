﻿using UnityEngine;
using System.Collections;

public class Spearman : BattleCharacter
{
    public int color;
    public Spearman(string Name)
    {
        setUp(Name, 1);
    }
    public Spearman(string Name, int Palette)
    {
        setUp(Name, Palette);
    }
    private void setUp(string Name, int Palette)
    {

        name = Name;
        color = Palette;
        job = "Spearman";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 13;
        skills[1] = 14;
        skills[2] = 15;
        randomTraits();
        //        traits[0] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("SpearmanPortrait"));
        if (color == 2)
            character = BattleManager.instance.Instantiate(Characters.instance.get("Spearman2"));
        else
            character = BattleManager.instance.Instantiate(Characters.instance.get("Spearman"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.soundEffect = "slash.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        tempSelect.type = CursorControl.EnemyFrontSelectState;
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.soundEffect = "slash.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("SpearmanPortrait"));
        if (color == 2)
            character = BattleManager.instance.Instantiate(Characters.instance.get("Spearman2"));
        else
            character = BattleManager.instance.Instantiate(Characters.instance.get("Spearman"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 85;
        baseStat.Mp = 70;
        baseStat.Atk = 95;
        baseStat.Def = 100;
        baseStat.MAtk = 90;
        baseStat.MDEF = 90;
        baseStat.Spd = 70;

        goldMin = 8;
        goldMax = 10;
    }
}