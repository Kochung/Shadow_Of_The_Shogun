﻿using UnityEngine;
using System.Collections;

public class Bowman : BattleCharacter
{
    public int color;
    public Bowman(string Name)
    {
        setUp(Name, 1);
    }
    public Bowman(string Name, int Palette)
    {
        setUp(Name, Palette);
    }
    private void setUp(string Name, int Palette)
    {
        name = Name;
        color = Palette;
        job = "Bowman";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 6;
        skills[1] = 7;
        skills[2] = 8;
        randomTraits();
        //        traits[0] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("BowmanPortrait"));
        if (color == 2)
            character = BattleManager.instance.Instantiate(Characters.instance.get("Bowman2"));
        else
            character = BattleManager.instance.Instantiate(Characters.instance.get("Bowman"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.physical = false;
        tempAttack.soundEffect = "arrow.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.physical = false;
        tempEnemyAttack.soundEffect = "arrow.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("BowmanPortrait"));
        if (color == 2)
            character = BattleManager.instance.Instantiate(Characters.instance.get("Bowman2"));
        else
            character = BattleManager.instance.Instantiate(Characters.instance.get("Bowman"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 65;
        baseStat.Mp = 80;
        baseStat.Atk = 60;
        baseStat.Def = 60;
        baseStat.MAtk = 125;
        baseStat.MDEF = 95;
        baseStat.Spd = 100;

        goldMin = 4;
        goldMax = 6;
    }
}
