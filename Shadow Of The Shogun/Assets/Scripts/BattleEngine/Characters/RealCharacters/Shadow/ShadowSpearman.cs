﻿using UnityEngine;
using System.Collections;

public class ShadowSpearman : BattleCharacter
{
    public ShadowSpearman(string Name)
    {
        setUp(Name);
    }
    private void setUp(string Name)
    {
        name = Name;
        job = "Shadow";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 13;
        skills[1] = 14;
        skills[2] = 15;

        traits[0] = 9;
        traits[1] = 5;
        traits[2] = 8;
        traits[3] = 4;

        traits[4] = 10;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("SpearmanPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowSpearman"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.physical = true;
        tempAttack.soundEffect = "slash.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.physical = true;
        tempEnemyAttack.soundEffect = "slash.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("SpearmanPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowSpearman"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 85;
        baseStat.Mp = 70;
        baseStat.Atk = 95;
        baseStat.Def = 100;
        baseStat.MAtk = 90;
        baseStat.MDEF = 90;
        baseStat.Spd = 70;

        goldMin = 8;
        goldMax = 10;
    }
}

