﻿using UnityEngine;
using System.Collections;

public class ShadowShogun : BattleCharacter
{
    public ShadowShogun(string Name)
    {
        setUp(Name);
    }
    private void setUp(string Name)
    {
        name = Name;
        job = "Shadow";
        cannotRecruit = true;
        noAttack = true;

        setBaseStat();
        loyalty = 0; 
        IV.Hp = 31;
        IV.Mp = 0;
        IV.Atk = 0;
        IV.Def = 31;
        IV.MAtk = 0;
        IV.MDEF = 31;
        IV.Spd = 0;

        EV.Atk = 0;
        EV.MAtk = 0;
        EV.Spd = 0;

        skills[0] = 25;
        skills[1] = 24;
        skills[2] = 26;
        skills[3] = 27;

        traits[0] = 5;
        traits[1] = 10;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("ShogunPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowShogun"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.physical = false;
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.physical = false;
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("ShogunPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowShogun"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 255;
        baseStat.Mp = 0;
        baseStat.Atk = 0;
        baseStat.Def = 100;
        baseStat.MAtk = 0;
        baseStat.MDEF = 100;
        baseStat.Spd = 0;

        goldMin = 4;
        goldMax = 6;
    }
}
