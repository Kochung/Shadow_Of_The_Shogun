﻿using UnityEngine;
using System.Collections;

public class ShadowRonin : BattleCharacter
{
    int color;
    public ShadowRonin(string Name, int Color)
    {
        color = Color;
        setUp(Name);
    }
    private void setUp(string Name)
    {
        name = Name;
        job = "Shadow";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 3;
        skills[1] = 4;
        skills[2] = 5;

        traits[0] = 9;
        traits[1] = 5;
        traits[2] = 7;
        traits[3] = 4;
        traits[4] = 10;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("RoninPortrait"));
        if (color == 2)
            character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowRonin2"));
        else
            character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowRonin"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.physical = true;
        tempAttack.soundEffect = "slash.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.physical = true;
        tempEnemyAttack.soundEffect = "slash.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("RoninPortrait"));
        if (color == 2)
            character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowRonin2"));
        else
            character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowRonin"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 80;
        baseStat.Mp = 60;
        baseStat.Atk = 125;
        baseStat.Def = 90;
        baseStat.MAtk = 55;
        baseStat.MDEF = 80;
        baseStat.Spd = 90;

        goldMin = 6;
        goldMax = 8;
    }
}

