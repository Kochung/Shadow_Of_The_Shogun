﻿using UnityEngine;
using System.Collections;

public class ShadowBowman : BattleCharacter
{
    public ShadowBowman(string Name)
    {
        setUp(Name);
    }
    private void setUp(string Name)
    {
        name = Name;
        job = "Shadow";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 6;
        skills[1] = 7;
        skills[2] = 8;

        traits[0] = 9;
        traits[1] = 7;
        traits[2] = 6;

        traits[3] = 10;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("BowmanPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowBowman"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.physical = false;
        tempAttack.soundEffect = "arrow.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.physical = false;
        tempEnemyAttack.soundEffect = "arrow.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("BowmanPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("ShadowBowman"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 65;
        baseStat.Mp = 80;
        baseStat.Atk = 60;
        baseStat.Def = 60;
        baseStat.MAtk = 125;
        baseStat.MDEF = 95;
        baseStat.Spd = 100;

        goldMin = 4;
        goldMax = 6;
    }
}

