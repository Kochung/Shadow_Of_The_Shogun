﻿using UnityEngine;
using System.Collections;

public class Jorougumo : BattleCharacter
{
    public Jorougumo(string Name)
    {
        name = Name;
        job = "Jorougumo";

        setBaseStat();
        loyalty = 100;
        IV.roll();
        
        randomTraits();
        //       traits[0] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("JorougumoPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Jorougumo"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.physical = false;
        tempAttack.soundEffect = "slash.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.physical = false;
        tempEnemyAttack.soundEffect = "slash.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("JorougumoPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Jorougumo"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 70;
        baseStat.Mp = 100;
        baseStat.Atk = 60;
        baseStat.Def = 50;
        baseStat.MAtk = 115;
        baseStat.MDEF = 100;
        baseStat.Spd = 95;

        goldMin = 5;
        goldMax = 7;
    }
}