﻿using UnityEngine;
using System.Collections;

public class Oni : BattleCharacter
{
    public Oni(string Name)
    {
        name = Name;
        job = "Oni";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 16;
        skills[1] = 17;
        skills[2] = 18;
        randomTraits();
        //        traits[0] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("OniPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Oni"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.soundEffect = "blunt_hit.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        tempSelect.type = CursorControl.EnemyFrontSelectState;
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.soundEffect = "blunt_hit.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("OniPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Oni"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 100;
        baseStat.Mp = 50;
        baseStat.Atk = 110;
        baseStat.Def = 65;
        baseStat.MAtk = 60;
        baseStat.MDEF = 55;
        baseStat.Spd = 77;

        goldMin = 6;
        goldMax = 8;
    }
}