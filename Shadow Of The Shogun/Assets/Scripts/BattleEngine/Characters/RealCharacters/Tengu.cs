﻿using UnityEngine;
using System.Collections;

public class Tengu : BattleCharacter
{
    public Tengu(string Name)
    {
        name = Name;
        job = "Tengu";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 19;
        skills[1] = 20;
        randomTraits();
//        traits[0] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("TenguPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Tengu"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.soundEffect = "slash.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.soundEffect = "slash.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("TenguPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Tengu"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 75;
        baseStat.Mp = 65;
        baseStat.Atk = 100;
        baseStat.Def = 80;
        baseStat.MAtk = 90;
        baseStat.MDEF = 60;
        baseStat.Spd = 80;

        goldMin = 5;
        goldMax = 7;
    }
}
