﻿using UnityEngine;
using System.Collections;

public class Ronin : BattleCharacter
{
    public int color;
    public Ronin(string Name)
    {
        setUp(Name, 1);
    }
    public Ronin(string Name, int Palette)
    {
        setUp(Name, Palette);
    }

    private void setUp(string Name, int Palette)
    {
        name = Name;
        color = Palette;
        job = "Ronin";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 3;
        skills[1] = 4;
        skills[2] = 5;
        randomTraits();
        //        traits[0] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("RoninPortrait"));
        if (color == 2)
            character = BattleManager.instance.Instantiate(Characters.instance.get("Ronin2"));
        else
            character = BattleManager.instance.Instantiate(Characters.instance.get("Ronin"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.soundEffect = "slash.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        tempSelect.type = CursorControl.EnemyFrontSelectState;
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.soundEffect = "slash.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("RoninPortrait"));
        if (color == 2)
            character = BattleManager.instance.Instantiate(Characters.instance.get("Ronin2"));
        else
            character = BattleManager.instance.Instantiate(Characters.instance.get("Ronin"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 80;
        baseStat.Mp = 60;
        baseStat.Atk = 125;
        baseStat.Def = 90;
        baseStat.MAtk = 55;
        baseStat.MDEF = 80;
        baseStat.Spd = 90;

        goldMin = 6;
        goldMax = 8;
    }
}