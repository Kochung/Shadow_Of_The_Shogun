﻿using UnityEngine;
using System.Collections;

public class Monk : BattleCharacter
{
    public Monk(string Name)
    {
        name = Name;
        job = "Monk";

        setBaseStat();
        loyalty = 100;
        IV.roll();

        skills[0] = 12;
        skills[1] = 9;
        skills[2] = 10;
        skills[3] = 11;
        randomTraits();
        //        traits[0] = 2;

        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("MonkPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Monk"));

        Mp = MaxMp;
        Hp = MaxHp;

        AttackAction tempAttack = new AttackAction();
        tempAttack.physical = false;
        tempAttack.soundEffect = "blunt_hit.wav";
        attackAction = tempAttack;

        AttackSelectAction tempSelect = new AttackSelectAction();
        tempSelect.type = CursorControl.EnemyFrontSelectState;
        attackSelectAction = tempSelect;

        EnemyAttackAction tempEnemyAttack = new EnemyAttackAction();
        tempEnemyAttack.physical = false;
        tempEnemyAttack.soundEffect = "blunt_hit.wav";
        enemyAttackAction = tempEnemyAttack;
    }
    public override void reInstantiate()
    {
        portrait = BattleManager.instance.Instantiate(Portraits.instance.get("MonkPortrait"));
        character = BattleManager.instance.Instantiate(Characters.instance.get("Monk"));
    }
    public void setBaseStat()
    {
        baseStat.Hp = 65;
        baseStat.Mp = 100;
        baseStat.Atk = 70;
        baseStat.Def = 55;
        baseStat.MAtk = 110;
        baseStat.MDEF = 100;
        baseStat.Spd = 78;

        goldMin = 4;
        goldMax = 6;
    }
}
