﻿using UnityEngine;
using System.Collections;

public class BaseStat
{
    public int Hp;
    public int Mp;
    public int Atk;
    public int Def;
    public int MAtk;
    public int MDEF;
    public int Spd;
    public void set(int hp, int mp,int atk,int def,int matk, int mdef, int spd)
    {
        Hp = hp;
        Mp = mp;
        Atk = atk;
        Def = def;
        MAtk = matk;
        MDEF = mdef;
        Spd = spd;
    }

    public void load(int[] values)
    {
        Hp = values[0];
        Mp = values[1];
        Atk = values[2];
        Def = values[3];
        MAtk = values[4];
        MDEF = values[5];
        Spd = values[6];
    }
}
