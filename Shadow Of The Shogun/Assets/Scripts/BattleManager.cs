﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class BattleManager : MonoBehaviour
{
    public static BattleManager instance = null;
    public System.Random random;

    public BoardControl board;
    public CharacterList characters;
    public Party party;
    public EnemyParty enemyParty;
    public TurnOrder turnOrder;
    public TextBox textBox;
    public CursorControl cursor;
    public ItemBag bag;
    public Gold gold;
    public HealthBarList healthBars;
    public int floorNumber;

    public bool boss;

    public Names names;

    public Reserves reserve;
    public Recruits recruits;

    public StateInformation information;
    public SkillList skillList;
    public TraitList traitList;
    public Machine machine;
    public bool active;
    public bool escapeDungeon;
    public bool Done;
    public bool start;
    BattleCharacter shogun;
    public int winGold;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        active = false;
    }
	void Start ()
    {
        textBox = new TextBox();
        Reset();
    }
    public void Reset()
    {
        random = new System.Random();
        reserve = new Reserves();
        recruits = new Recruits();
        party = new Party();
        enemyParty = new EnemyParty();
        characters = new CharacterList();
        board = new BoardControl();
        names = new Names();
        shogun = new Shogun("Shogun");
        turnOrder = new TurnOrder();
        information = new StateInformation();
        skillList = new SkillList();
        traitList = new TraitList();
        bag = new ItemBag();
        gold = new Gold();
        bag.StartingItems();

        recruits.roll();
        active = true;
        floorNumber = 1;
    }
    public void runAfterLoad()
    {
        if(active)
        {
            gold.findText();
            gold.updateNumber();
            escapeDungeon = false;
            party.cleanStatus();
        }
    }
	public void StartBattle()
    {
        cursor = GameObject.Find("CursorControl").GetComponent<CursorControl>();
        textBox.reInstantiate();
        turnOrder.reInstantiate();
        characters.clear();

        machine = new Machine(new StartState());

        party.reInstantiate();
        shogun.reInstantiate();
        //TempStart();
        if(boss)
            getBoss();
        else
            rollEnemies();

        characters.add(shogun, false);
        party.addAll(characters);
        enemyParty.addAll(characters);
        board.setParty(party);
        board.setEnemyParty(enemyParty);

        board.placeCharacters(characters);

        healthBars = new HealthBarList();
        Done = false;
        start = true;
    }
	void Update ()
    {
        if(!Done && start)
        {
            machine.Run();
        }
        else if(start)
        {
            if (!textBox.isMainActive())
            {
                if (party.Wipe() || escapeDungeon)
                {
                    MapManager.instance.clearMap();
                    GameManager.instance.KillSave();
                    GameManager.instance.LoadTown();
                    floorNumber = 1;
                }
                else
                {
                    if (enemyParty.Wipe())
                        gold.add(winGold);
                    if(boss)
                    {
                        GameManager.instance.Load("Final","");
                        boss = false;
                    }
                    else
                        GameManager.instance.LoadDungeon();
                }
                start = false;
            }
        }
	}

    private void TempStart()
    {
        enemyParty.Possition1 = new Ronin(names.getRandomName());
        enemyParty.Possition2 = new Bowman(names.getRandomName());
        enemyParty.Possition3 = new Spearman(names.getRandomName());
        enemyParty.Possition4 = new Monk(names.getRandomName());
        enemyParty.Possition5 = new Jorougumo(names.getRandomName());
        enemyParty.Possition6 = new Tengu(names.getRandomName());
        enemyParty.Possition7 = new Nue(names.getRandomName());
        enemyParty.Possition8 = new Oni(names.getRandomName());
        enemyParty.Possition9 = new Ronin(names.getRandomName(),2);
    }
    private void getBoss()
    {
        enemyParty.clear();
        winGold = 0;
        enemyParty.Possition8 = new ShadowRonin("SwordShadow", 1);
        enemyParty.Possition4 = new ShadowSpearman("SpearShadow");
        enemyParty.Possition2 = new ShadowRonin("SwordShadow", 2);
        enemyParty.Possition9 = new ShadowBowman("BowShadow");
        enemyParty.Possition3 = new ShadowMonk("BeadShadow");
        enemyParty.Possition5 = new ShadowShogun("Shadow");


        enemyParty.Possition8.levelUpTo(100);
        enemyParty.Possition4.levelUpTo(100);
        enemyParty.Possition2.levelUpTo(100);
        enemyParty.Possition9.levelUpTo(100);
        enemyParty.Possition3.levelUpTo(100);
        enemyParty.Possition5.levelUpTo(100);

        enemyParty.Possition8.loyalty = 0;
        enemyParty.Possition4.loyalty = 0;
        enemyParty.Possition2.loyalty = 0;
        enemyParty.Possition9.loyalty = 0;
        enemyParty.Possition3.loyalty = 0;
        enemyParty.Possition5.loyalty = 0;

    }
    private void rollEnemies()
    {
        enemyParty.clear();
        winGold = 0;
        int ememyNumber = getEnemyNumber();
        for (int i = 0; i < ememyNumber; i++)
        {
            BattleCharacter randomEnemie = getRandomEnemies();
            winGold += randomEnemie.rollGold();
            randomEnemie.loyalty = 0;
            randomEnemie.levelUpTo(100 - (10 * (3-floorNumber)));
            enemyParty.setRandomFreeSlot(randomEnemie);
        }
    }
    public void addRandomEnemy(int enemyNumber)
    {
        for (int i = 0; i < enemyNumber; i++)
        {
            BattleCharacter randomEnemie = getRandomEnemies();
            winGold += randomEnemie.rollGold();
            randomEnemie.loyalty = 0;
            randomEnemie.levelUpTo(100 - (10 * (3 - floorNumber)));
            enemyParty.setRandomFreeSlot(randomEnemie);
            characters.add(randomEnemie, true);
        }

        board.setEnemyParty(enemyParty);
        board.placeCharacters(characters);
        healthBars.reset();
    }
    public int getEnemyNumber()
    {
        int number = random.Next(0, 100);
        if (number < 12)
            return 1;
        else if (number < 20)
            return 2;
        else if (number < 40)
            return 3;
        else if (number < 60)
            return 4;
        else if (number < 75)
            return 5;
        else if (number < 82)
            return 6;
        else if (number < 88)
            return 7;
        else if (number < 95)
            return 8;
        else if (number < 100)
            return 9;
        return 4;
    }
    public BattleCharacter getRandomEnemies()
    {
        switch(random.Next(0,12))
        {
            case 0:
                return new Oni(names.getRandomName());
            case 1:
                return new Tengu(names.getRandomName());
            case 2:
                return new Nue(names.getRandomName());
            case 3:
                return new Jorougumo(names.getRandomName());
            case 4:
                return new Spearman(names.getRandomName(),2);
            case 5:
                return new Bowman(names.getRandomName(),2);
            case 6:
                return new Ronin(names.getRandomName(),2);
            case 7:
                return new Monk(names.getRandomName());
            case 8:
                return new Oni(names.getRandomName());
            case 9:
                return new Tengu(names.getRandomName());
            case 10:
                return new Nue(names.getRandomName());
            case 11:
                return new Jorougumo(names.getRandomName());
        }
        return new Cobra("Cobra");
    }
    public GameObject Instantiate(GameObject thing)
    {
        if (thing == null)
            return null;
        return Instantiate(thing, new Vector3(999,999,0), Quaternion.identity) as GameObject;
    }
    public GameObject Instantiate(GameObject thing,Vector3 pos)
    {
        if (thing == null)
            return null;
        return Instantiate(thing, pos, Quaternion.identity) as GameObject;
    }
}
