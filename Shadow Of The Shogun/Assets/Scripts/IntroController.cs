﻿using UnityEngine;
using System.Collections;

public class IntroController : MonoBehaviour 
{
    SpriteRenderer Image;
	// Use this for initialization
	void Start () 
    {
        Image = GameObject.Find("Shogun01").GetComponent<SpriteRenderer>();
	}
	
	void Update () 
    {
        if(InputHandler.instance.AButton || InputHandler.instance.StartButton)
        {
            if (Image.enabled)
                Image.enabled = false;
            else
            {
                GameManager.instance.TownName = "TownMap";
                GameManager.instance.LoadTown();
            }
        }
	}
}
