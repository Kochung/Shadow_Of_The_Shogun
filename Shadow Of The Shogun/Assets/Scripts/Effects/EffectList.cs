﻿using UnityEngine;
using System.Collections;

public class EffectList : MonoBehaviour 
{
    public static EffectList instance = null;
    public GameObject[] effectList;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public GameObject Instantiate(int index, Vector3 pos)
    {
        return Instantiate(effectList[index], pos, Quaternion.identity) as GameObject;
    }
}
