﻿using UnityEngine;
using System.Collections;

public class CutsceneController : MonoBehaviour 
{
    bool activated;
    public Player player;
    public BigTextBox box;
    public Canvas canvas;
    string[] message;
    int timer = 40;
    public bool final;
	void Start () 
    {
        activated = false;
        player = GameObject.Find("Player").GetComponent<Player>();
        box = GameObject.Find("MainTextBox").GetComponent<BigTextBox>();
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        canvas.enabled = false;
        if (final)
        {
            message = new string[5];
            message[0] = "Shadow:\n*Hard Breathing* This is far from over. The people wont recognize you as the Shogun. Our forces are far from depleted.";
            message[1] = "Shogun:\nWhat do you mean \"our forces\"? How many of you are there?";
            message[2] = "Shadow:\nAs many as there are stars in the sky. You'll never defeat us all.*Body fades away*";    
            message[3] = "Shogun:\nJust what was he......? ";
            message[4] = "Shogun:\nWell for now I have this outpost back. I'll have to find out more about my enemy if I plan to take my kingom back.";
            timer = 0;
        }
        else
        {
            message = new string[6];
            message[0] = "Shogun:\nWho the hell are you?? And why do you look like me? ";
            message[1] = "Shadow:\nWho do you think? The Shogun.";
            message[2] = "Shogun:\nYOU DARE CALL YOURSELF THAT?! You've allowed the presence of monsters to dishonor this place. No Shogun would ever allow this.";
            message[3] = "Shadow:\nNow the question is how are you alive? You know what? It doesn't matter you'll be dead soon enough.";
            message[4] = "Shogun:\nBastard!!! This is my outpost! I am the rightful lord of these lands";
            message[5] = "Shadow:\n*Scoff* We'll see.";
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if(activated)
        {
            if(timer == 0)
            {
                canvas.enabled = true;
                box.SetMessage(message);
                player.stop();
                timer = -1;
                player.horizontalInput = 0;
                player.verticalInput = 0;
            }
            else if(timer == -1 && !box.isActive())
            {
                if (final)
                    GameManager.instance.Load("End", "BossEncounter");
                else
                {
                    BattleManager.instance.boss = true;
                    GameManager.instance.LoadBattle();
                }
            }
            else if(timer > 0)
            {
                timer--;
                player.horizontalInput = 0;
                player.verticalInput = 1;
            }
        }
	}
    void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.tag.Equals("Player"))
        {
            activated = true;
            player.cutscene = true;
            SoundManager.instance.PlayMusic("BossEncounter");
        }
    }
}
