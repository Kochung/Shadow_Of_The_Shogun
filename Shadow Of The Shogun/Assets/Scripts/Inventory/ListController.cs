﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class ListController : MonoBehaviour
{

    public GameObject ContentPanel;
    public GameObject ItemTemplate;
    public GameObject DialogTemplate;
    public GameObject IconTemplate;
    public Image cursor;
    public Vector2 itemOffset;
    public float spacing;
    public bool Battle;
    //	public AudioClip moveSound;
    //	public AudioClip yesSound;
    //	public AudioClip noSound;

    int selected;
    ArrayList Entries;
    GameObject currentEntry;
    GameObject[] Display;
    int displayHead;
    int displayTail;
    Image UpArrow;
    Image DownArrow;
    DialogController popup;
    bool active;
    bool forceBack;

    void Start()
    {
        forceBack = false;
        Entries = new ArrayList();
        selected = 0;
        Display = new GameObject[6];
        displayHead = 0;
        UpArrow = transform.GetChild(1).GetComponent<Image>();
        DownArrow = transform.GetChild(2).GetComponent<Image>();
        GameObject dialog = Instantiate(DialogTemplate) as GameObject;
        popup = dialog.GetComponent<DialogController>();
        popup.setImage();
        dialog.transform.SetParent(GetComponentInParent<ScrollRect>().transform, false);
        dialog.transform.localScale = Vector3.one;

        HideDialog();
        ItemBag bag = BattleManager.instance.bag;
        for (int i = 0; i < bag.size; i++)
        {
            if (bag.bag[i].quantity != 0)
            {
                GameObject newItem = Instantiate(ItemTemplate) as GameObject;
                ItemController controller = newItem.GetComponent<ItemController>();
                if(bag.bag[i].quantity != -1)
                {
                    string[] fields = { bag.bag[i].name, "" + bag.bag[i].quantity, bag.bag[i].icon };
                    controller.label.text = fields[0];
                    controller.count.text = fields[1];
                    controller.icon.sprite = IconList.instance.get(fields[2]);
                    controller.cursor.sprite = cursor.sprite;
                }
                else
                {
                    controller.label.text = "Back";
                    controller.cursor.sprite = cursor.sprite;
                }
                newItem.transform.SetParent(ContentPanel.transform, false);
                newItem.transform.position = new Vector3(0 + itemOffset.x, (-Entries.Count * spacing) + itemOffset.y, 0);
                Entries.Add(newItem);
                if (Entries.Count > 6)
                    Hide(newItem);
                else
                {
                    Display[Entries.Count - 1] = (newItem);
                    displayTail = Entries.Count - 1;
                }
            }
        }
        UpArrow.enabled = !(displayHead == 0);
        DownArrow.enabled = !(displayTail == Entries.Count - 1);
        ShowCursor(selected);
        Activate();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0)
            return;
        if (!active)
            return;
        updateDisplay();
        if (popup.noText.enabled)
        {
            if (DirectionalPadHandler.instance.Up || DirectionalPadHandler.instance.Down)
            {
                SoundManager.instance.PlaySingle("click");
                popup.yesIcon.enabled = !popup.yesIcon.enabled;
                popup.noIcon.enabled = !popup.noIcon.enabled;
            }

            if (InputHandler.instance.AButton || InputHandler.instance.StartButton)
            {
                if (popup.yesIcon.enabled)
                {
                    //					GetComponentInParent<AudioSource>().PlayOneShot(yesSound);
                    currentEntry = Entries[selected] as GameObject;
                    if(Battle)
                    {
                        runBattleAction();
                    }
                }
                else
                {
                    //					GetComponentInParent<AudioSource>().PlayOneShot(noSound);
                }

                HideDialog();
            }
            if (InputHandler.instance.BButton)
            {
                HideDialog();
            }
        }
        else
        {
            if (DirectionalPadHandler.instance.Down)
            {
                // play sound
                SoundManager.instance.PlaySingle("click");
                if (selected == (Entries.Count - 1))
                    return;
                if (selected == displayTail)
                    ScrollDown();
                HideCursor(selected++);
                ShowCursor(selected);
            }

            if (DirectionalPadHandler.instance.Up)
            {
                // play sound
                SoundManager.instance.PlaySingle("click");
                if (selected == 0)
                    return;
                if (selected == displayHead)
                    ScrollUp();
                HideCursor(selected--);
                ShowCursor(selected);
            }

            if (InputHandler.instance.AButton || InputHandler.instance.StartButton)
            {
                ShowDialog();
            }
            if (InputHandler.instance.BButton)
            {
                forceBack = true;
                Deactivate();
            }
        }
    }

    public void HideDialog()
    {
        popup.noIcon.enabled = false;
        popup.noText.enabled = false;
        popup.yesIcon.enabled = false;
        popup.yesText.enabled = false;
        popup.backgroundImage.enabled = false;
    }

    public void ShowDialog()
    {

        popup.transform.position = new Vector3(-5, ((GameObject)Entries[selected]).transform.position.y, 0);
        popup.noText.enabled = true;
        popup.yesIcon.enabled = true;
        popup.yesText.enabled = true;
        popup.backgroundImage.enabled = true;

    }

    public void HideCursor(int index)
    {
        GameObject item = Entries[index] as GameObject;
        item.GetComponent<ItemController>().cursor.enabled = false;
    }

    public void ShowCursor(int index)
    {
        GameObject item = Entries[index] as GameObject;
        item.GetComponent<ItemController>().cursor.enabled = true;
    }

    public void ScrollUp()
    {
        Hide(((GameObject)Entries[displayTail]));
        displayHead--;
        displayTail--;
        for (int i = 0; i < Display.Length; i++)
            Display[i] = ((GameObject)Entries[displayHead + i]);
        updateDisplay();
        UpArrow.enabled = !(displayHead == 0);
        DownArrow.enabled = !(displayTail == Entries.Count - 1);
    }

    public void ScrollDown()
    {
        Hide(((GameObject)Entries[displayHead]));
        displayHead++;
        displayTail++;
        for (int i = 0; i < Display.Length; i++)
            Display[i] =((GameObject)Entries[displayHead + i]);
        updateDisplay();
        UpArrow.enabled = !(displayHead == 0);
        DownArrow.enabled = !(displayTail == Entries.Count - 1);
    }
    public void displayRemove()
    {
        if (Entries.Count < 6)
        {
            Display = new GameObject[Entries.Count];
            displayTail = Entries.Count - 1;
            displayHead = 0;
        }
        else if (displayTail >= Entries.Count)
        {
            displayTail--;
            displayHead--;
        }
        for (int i = 0; i < Display.Length; i++)
            Display[i] = ((GameObject)Entries[displayHead + i]);
        updateDisplay();
        UpArrow.enabled = !(displayHead == 0);
        DownArrow.enabled = !(displayTail == Entries.Count - 1);
    }
    private void updateDisplay()
    {
        for (int i = 0; i < Display.Length; i++)
            Display[i].transform.position = new Vector3(0 + itemOffset.x, (-i * spacing) + itemOffset.y, 0);
    }
    private void Hide(GameObject item)
    {
        item.transform.position = new Vector3(99, 0, 0);
    }
    public void Activate()
    {
        active = true;
        transform.parent.GetComponent<Canvas>().enabled = true;
    }
    public void Deactivate()
    {
        active = false;
        transform.parent.GetComponent<Canvas>().enabled = false;
    }
    public bool isActive()
    {
        return active;
    }
    public void runBattleAction()
    {
        Deactivate();
    }
    public void usedItem()
    {
        if (currentEntry.GetComponent<ItemController>().UseItem())
        {
            Entries.RemoveAt(selected);
            displayRemove();
            GameObject.Destroy(currentEntry.gameObject);
            if (selected >= Entries.Count)
                selected--;
            ShowCursor(selected);
        }
    }
    public GameObject getItem()
    {
        if(forceBack)
        {
            forceBack = false;
            return ((GameObject)Entries[Entries.Count - 1]);
        }
        return ((GameObject)Entries[selected]);
    }
}
