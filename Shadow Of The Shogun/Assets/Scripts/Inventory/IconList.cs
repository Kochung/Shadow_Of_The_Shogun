﻿using UnityEngine;
using System.Collections;

public class IconList : MonoBehaviour 
{
    public static IconList instance = null;
    public Sprite[] icon;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    public Sprite get(string name)
    {
        int length = icon.Length;
        for (int i = 0; i < length; i++)
        {
            if (name.Equals(icon[i].name))
                return icon[i];
        }
        return null;
    }
}
