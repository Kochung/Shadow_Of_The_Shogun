﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemController : MonoBehaviour {
	public Image icon, cursor;
	public Text label, count;

	public bool UseItem()
	{
        BattleManager.instance.bag.useItem(label.text, 1);
		int num = int.Parse (count.text);
		if (num <= 1)
		{
			return true;
		}

		count.text = string.Format ("{0}", --num);
		return false;
	}
    public string getName()
    {
        return label.text;
    }
}
