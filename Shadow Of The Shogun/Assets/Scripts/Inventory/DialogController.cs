﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogController : MonoBehaviour {
	public Image yesIcon, noIcon;
	public Text yesText, noText;
    public Image backgroundImage;
    public void setImage()
    {
        backgroundImage = GetComponent<Image>();
    }
}