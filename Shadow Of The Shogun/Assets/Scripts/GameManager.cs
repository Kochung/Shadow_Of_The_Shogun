﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour 
{

    public static GameManager instance = null;

    public bool savePosision;
    public Vector3 playerPos;
    public Vector3 cameraPos;
    public int playerFace;
    public EnemyList enemyList;
    public System.Random random;
    public bool stopEnemies;
    public string TownName;

    private string next;
    private string nextSong;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        enemyList = new EnemyList();
    }
	void Start () 
    {
        savePosision = false;
        random = new System.Random();
        stopEnemies = true;
    }
	void Update () {}

    public void KillSave()
    {
        savePosision = false;
    }
    public void SavePos()
    {
        savePosision = true;
        playerPos = GameObject.Find("Player").transform.position;
        cameraPos = GameObject.Find("Main Camera").transform.position;
        playerFace = GameObject.Find("Player").GetComponent<Player>().getFace();
        MapManager.instance.setFogMap();
        enemyList.Hold();
    }
    public void LoadBattle()
    {
        next = "Battle";
        nextSong = "Battle";
        if (BattleManager.instance.boss)
            nextSong = "Boss";
        SceneManager.LoadScene("Loading");
    }
    public void LoadDungeon()
    {
        next = "Dungeon";
        nextSong = "Dungeon";
        SceneManager.LoadScene("Loading");
    }
    public void LoadPause()
    {
        next = "Pause";
        nextSong = "Dungeon";
        SceneManager.LoadScene("Loading");
    }
    public void LoadPartySelect()
    {
        next = "PartySelection";
        nextSong = "Town";
        SceneManager.LoadScene("Loading");
    }
    public void LoadTown()
    {
        SceneManager.LoadScene(TownName);
        SoundManager.instance.PlayMusic("Town");
    }
    public void LoadWorld()
    {
        SceneManager.LoadScene("WorldMap");
        SoundManager.instance.PlayMusic("World");
    }
    public void LoadNext()
    {
        SceneManager.LoadScene(next);
        SoundManager.instance.PlayMusic(nextSong);
    }
    public void Load(string SceneName, string songName)
    {
        next = SceneName;
        nextSong = songName;
        SceneManager.LoadScene("Loading");
    }
}
