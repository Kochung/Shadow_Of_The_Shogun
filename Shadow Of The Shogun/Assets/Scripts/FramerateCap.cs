﻿using UnityEngine;
using System.Collections;

public class FramerateCap : MonoBehaviour 
{
    void Awake()
    {
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 60;
    }
}
