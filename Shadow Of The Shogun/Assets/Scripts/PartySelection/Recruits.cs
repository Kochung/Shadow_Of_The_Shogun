﻿using UnityEngine;
using System.Collections;

public class Recruits
{
    public BattleCharacter[] list;

    public Recruits()
    {
        list = new BattleCharacter[6];
        for (int i = 0; i < 6; i++)
            list[i] = new EmptyCharacter();
    }
    public void roll()
    {
        for(int i=0;i<6;i++)
        {
            int num = BattleManager.instance.random.Next(0,4);
            switch(num)
            {
                case 0:
                    list[i] = new Ronin(BattleManager.instance.names.getRandomName());
                    break;
                case 1:
                    list[i] = new Monk(BattleManager.instance.names.getRandomName());
                    break;
                case 2:
                    list[i] = new Spearman(BattleManager.instance.names.getRandomName());
                    break;
                case 3:
                    list[i] = new Bowman(BattleManager.instance.names.getRandomName());
                    break;
            }
            list[i].levelUpTo(100);
        }
    }
    public void reInstantiate()
    {
        for (int i = 0; i < 6; i++)
        {
            if (list[i].Hp != 0)
                list[i].reInstantiate();
            else
                list[i] = new EmptyCharacter();
        }
    }
}
