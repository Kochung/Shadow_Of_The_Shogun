﻿using UnityEngine;
using System.Collections;

public class CharacterID : MonoBehaviour
{
    public BattleCharacter character;
    public void set(BattleCharacter Character)
    {
        character = Character;
        if(!character.job.Equals(""))
        {
            SpriteRenderer tempSprite = character.character.GetComponent<SpriteRenderer>();
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = tempSprite.sprite;
            transform.GetChild(0).GetComponent<SpriteRenderer>().transform.localScale = new Vector3(tempSprite.transform.localScale.x * (3 / 2), tempSprite.transform.localScale.y * (3 / 2), tempSprite.transform.localScale.z);
            transform.GetChild(1).GetComponent<TextMesh>().text = character.name;
            transform.GetChild(3).GetComponent<TextMesh>().text = character.job;
            transform.GetChild(4).GetComponent<TextMesh>().text = "Level:" + character.level;
        }
        else
        {
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = null;
            transform.GetChild(1).GetComponent<TextMesh>().text = "Empty";
            transform.GetChild(3).GetComponent<TextMesh>().text = "";
            transform.GetChild(4).GetComponent<TextMesh>().text = "";
        }
    }
}
