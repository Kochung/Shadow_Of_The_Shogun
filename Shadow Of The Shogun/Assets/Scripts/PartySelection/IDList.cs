﻿using UnityEngine;
using System.Collections;

public class IDList 
{
    CharacterID [] party;
    CharacterID[] reserve;
    CharacterID[] recruits;
    public IDList(GameObject ID, float offset)
    {
        party = new CharacterID[4];
        reserve = new CharacterID[12];
        recruits = new CharacterID[6];
        
        party[0] = (BattleManager.instance.Instantiate(ID, new Vector3(-7, 1.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        party[1] = (BattleManager.instance.Instantiate(ID, new Vector3(-7, 2.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        party[2] = (BattleManager.instance.Instantiate(ID, new Vector3(-4, 1.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        party[3] = (BattleManager.instance.Instantiate(ID, new Vector3(-4, 2.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();

        reserve[0] = (BattleManager.instance.Instantiate(ID, new Vector3(3, -.75f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[1] = (BattleManager.instance.Instantiate(ID, new Vector3(3, .25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[2] = (BattleManager.instance.Instantiate(ID, new Vector3(3, 1.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[3] = (BattleManager.instance.Instantiate(ID, new Vector3(3, 2.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[4] = (BattleManager.instance.Instantiate(ID, new Vector3(3, 3.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[5] = (BattleManager.instance.Instantiate(ID, new Vector3(3, 4.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[6] = (BattleManager.instance.Instantiate(ID, new Vector3(0, -.75f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[7] = (BattleManager.instance.Instantiate(ID, new Vector3(0, .25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[8] = (BattleManager.instance.Instantiate(ID, new Vector3(0, 1.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[9] = (BattleManager.instance.Instantiate(ID, new Vector3(0, 2.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[10] = (BattleManager.instance.Instantiate(ID, new Vector3(0, 3.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        reserve[11] = (BattleManager.instance.Instantiate(ID, new Vector3(0, 4.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();

        recruits[0] = (BattleManager.instance.Instantiate(ID, new Vector3(7, -.75f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        recruits[1] = (BattleManager.instance.Instantiate(ID, new Vector3(7, .25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        recruits[2] = (BattleManager.instance.Instantiate(ID, new Vector3(7, 1.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        recruits[3] = (BattleManager.instance.Instantiate(ID, new Vector3(7, 2.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        recruits[4] = (BattleManager.instance.Instantiate(ID, new Vector3(7, 3.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
        recruits[5] = (BattleManager.instance.Instantiate(ID, new Vector3(7, 4.25f + offset, 0)) as GameObject).GetComponent<CharacterID>();
    }
	
    public void reOrder()
    {
        party[0].set(BattleManager.instance.party.BottomLeft);
        party[1].set(BattleManager.instance.party.TopLeft);
        party[2].set(BattleManager.instance.party.BottomRight);
        party[3].set(BattleManager.instance.party.TopRight);

        for (int i = 0; i < 12; i++)
            reserve[i].set(BattleManager.instance.reserve.list[11-i]);

        for (int i = 0; i < 6; i++)
            recruits[i].set(BattleManager.instance.recruits.list[5-i]);
    }
}
