﻿using UnityEngine;
using System.Collections;

public class Reserves
{
    public BattleCharacter[] list;
    public Reserves()
    {
        list = new BattleCharacter[12];
        for (int i = 0; i < 12; i++)
            list[i] = new EmptyCharacter();
    }
    public void reInstantiate()
    {
        for (int i = 0; i < 12; i++)
        {
            if (list[i].Hp != 0)
                list[i].reInstantiate();
            else
                list[i] = new EmptyCharacter();
        }
    }
}
