﻿using UnityEngine;
using System.Collections;

public class IDSelector : MonoBehaviour
{
    public GameObject ID;
    public float offset;

    GameObject Mark;
    Position[][] map;
    SmallTextBox text;
    IDList list;

    int positionX;
    int positionY;
    bool marked;
    Position MarkPosition;
    int markedX;
    int markedY;
    private class Position
    {
        public float x;
        public float y;
        public int state;
        public Position(float X, float Y, int S)
        {
            x = X;
            y = Y;
            state = S;
        }
    }
	void Start ()
    {
        marked = false;
        Mark = GameObject.Find("IDMark");
        text = GameObject.Find("TextBox").GetComponent<SmallTextBox>();
        map = new Position[5][];
        for(int i=0;i<5; i++)
        {
            map[i] = new Position[6];
        }
        map[0][2] = new Position(-7, 2.25f + offset, 0);
        map[0][3] = new Position(-7, 1.25f + offset, 0);
        map[1][2] = new Position(-4, 2.25f + offset, 0);
        map[1][3] = new Position(-4, 1.25f + offset, 0);


        map[2][5] = new Position(0, -.75f + offset, 1);
        map[2][4] = new Position(0, .25f + offset, 1);
        map[2][3] = new Position(0, 1.25f + offset, 1);
        map[2][2] = new Position(0, 2.25f + offset, 1);
        map[2][1] = new Position(0, 3.25f + offset, 1);
        map[2][0] = new Position(0, 4.25f + offset, 1);
        map[3][5] = new Position(3, -.75f + offset, 1);
        map[3][4] = new Position(3, .25f + offset, 1);
        map[3][3] = new Position(3, 1.25f + offset, 1);
        map[3][2] = new Position(3, 2.25f + offset, 1);
        map[3][1] = new Position(3, 3.25f + offset, 1);
        map[3][0] = new Position(3, 4.25f + offset, 1);

        map[4][5] = new Position(7, -.75f + offset, 2);
        map[4][4] = new Position(7, .25f + offset, 2);
        map[4][3] = new Position(7, 1.25f + offset, 2);
        map[4][2] = new Position(7, 2.25f + offset, 2);
        map[4][1] = new Position(7, 3.25f + offset, 2);
        map[4][0] = new Position(7, 4.25f + offset, 2);

        BattleManager.instance.party.reInstantiate();
        BattleManager.instance.reserve.reInstantiate();
        BattleManager.instance.recruits.roll();

        list = new IDList(ID, offset);
        list.reOrder();
    }
	
	void Update ()
    {
        if (Time.timeScale == 0)
            return;
        int horizontal = 0;
        int vertical = 0;
        bool Abutton = InputHandler.instance.AButton;
        bool Bbutton = InputHandler.instance.BButton;
        bool StartButton = InputHandler.instance.StartButton;
        if (DirectionalPadHandler.instance.Up)
            vertical++;
        if (DirectionalPadHandler.instance.Down)
            vertical--;
        if (DirectionalPadHandler.instance.Left)
            horizontal++;
        if (DirectionalPadHandler.instance.Right)
            horizontal--;
        if (horizontal != 0 || vertical != 0)
        {
            SoundManager.instance.PlaySingle("click");
        }
        changeHorizontal(horizontal);
        changeVertical(-vertical);
        check();
        transform.position = new Vector2(map[positionX][positionY].x, map[positionX][positionY].y);

        if (Bbutton)
        {
            if (marked)
            {
                HideMark();
                marked = false;
            }
            else
            {
                GameManager.instance.LoadTown();
            }
        }
        else if (Abutton)
        {
            if (marked)
            {
                HideMark();
                marked = false;
                swap();
                list.reOrder();
            }
            else
            {
                Mark.transform.position = new Vector2(map[positionX][positionY].x, map[positionX][positionY].y);
                MarkPosition = map[positionX][positionY];
                markedX = positionX;
                markedY = positionY;
                marked = true;
            }
        }
        info();
    }
    public void info()
    {
        string left = "";
        string right = "";
        
        if (map[positionX][positionY].state == 0)
        {
            BattleCharacter temp = getPlayer(positionX, positionY - 2);
            left = temp.getInformationLeft();
            right = temp.getInformationRight();
        }
        else if (map[positionX][positionY].state == 1)
        {
            BattleCharacter temp = BattleManager.instance.reserve.list[getReserveIndex(positionX, positionY)];
            left = temp.getInformationLeft();
            right = temp.getInformationRight();
        }
        else
        {
            BattleCharacter temp = BattleManager.instance.recruits.list[positionY];
            left = temp.getInformationLeft();
            right = temp.getInformationRight();
        }
        text.SetMessage(left, -1, right, 15);
    }
    void HideMark()
    {
        Mark.transform.position = new Vector2(999,999);
    }
    void changeHorizontal(int horizontal)
    {
        positionX += horizontal;
        if (positionX < 0)
            positionX = 4;
        if (positionX > 4)
            positionX = 0;
    }
    void changeVertical(int vertical)
    {
        positionY += vertical;
        if (positionY < 0)
            positionY = 5;
        if (positionY > 5)
            positionY = 0;
        if (positionX < 2 && positionY > 3)
            positionY = 3;
        else if (positionX < 2 && positionY < 2)
            positionY = 2;
    }
    void check()
    {
        if (positionX < 2 && positionY > 3)
            positionY = 3;
        if (positionX < 2 && positionY < 2)
            positionY = 2;
    }
    void swap()
    {
        if(map[positionX][positionY].state == 0)
        {
            BattleCharacter temp;
            switch (MarkPosition.state)
            {
                case 0:
                    temp = getPlayer(positionX, positionY - 2);
                    setPlayer(getPlayer(markedX, markedY - 2), positionX, positionY - 2);
                    setPlayer(temp, markedX, markedY - 2);
                    break;
                case 1:
                    temp = getPlayer(positionX, positionY - 2);
                    setPlayer(BattleManager.instance.reserve.list[getReserveIndex(markedX, markedY)], positionX, positionY - 2);
                    BattleManager.instance.reserve.list[getReserveIndex(markedX, markedY)] = temp;
                    break;
                case 2:
                    temp = getPlayer(positionX, positionY - 2);
                    setPlayer(BattleManager.instance.recruits.list[markedY], positionX, positionY - 2);
                    BattleManager.instance.recruits.list[markedY] = temp;
                    break;
            }
        }
        else if (map[positionX][positionY].state == 1)
        {
            BattleCharacter temp;
            switch (MarkPosition.state)
            {
                case 0:
                    temp = BattleManager.instance.reserve.list[getReserveIndex(positionX, positionY)];
                    BattleManager.instance.reserve.list[getReserveIndex(positionX, positionY)] = getPlayer(markedX, markedY - 2);
                    setPlayer(temp, markedX, markedY - 2);
                    break;
                case 1:
                    temp = BattleManager.instance.reserve.list[getReserveIndex(positionX, positionY)];
                    BattleManager.instance.reserve.list[getReserveIndex(positionX, positionY)] = BattleManager.instance.reserve.list[getReserveIndex(markedX, markedY)];
                    BattleManager.instance.reserve.list[getReserveIndex(markedX, markedY)] = temp;
                    break;
                case 2:
                    temp = BattleManager.instance.reserve.list[getReserveIndex(positionX, positionY)];
                    BattleManager.instance.reserve.list[getReserveIndex(positionX, positionY)] = BattleManager.instance.recruits.list[markedY];
                    BattleManager.instance.recruits.list[markedY] = temp;
                    break;
            }
        }
        else
        {
            BattleCharacter temp;
            switch (MarkPosition.state)
            {
                case 0:
                    temp = BattleManager.instance.recruits.list[positionY];
                    BattleManager.instance.recruits.list[positionY] = getPlayer(markedX, markedY - 2);
                    setPlayer(temp, markedX, markedY - 2);
                    break;
                case 1:
                    temp = BattleManager.instance.recruits.list[positionY];
                    BattleManager.instance.recruits.list[positionY] = BattleManager.instance.reserve.list[getReserveIndex(markedX, markedY)];
                    BattleManager.instance.reserve.list[getReserveIndex(markedX, markedY)] = temp;
                    break;
                case 2:
                    temp = BattleManager.instance.recruits.list[positionY];
                    BattleManager.instance.recruits.list[positionY] = BattleManager.instance.recruits.list[markedY];
                    BattleManager.instance.recruits.list[markedY] = temp;
                    break;
            }
        }
    }
    public BattleCharacter getPlayer(int x,int y)
    {
        if (y == 0)
        {
            if (x == 0)
                return BattleManager.instance.party.TopLeft;
            if (x == 1)
                return BattleManager.instance.party.TopRight;
        }
        if (y == 1)
        {
            if (x == 0)
                return BattleManager.instance.party.BottomLeft;
            if (x == 1)
                return BattleManager.instance.party.BottomRight;
        }
        return new EmptyCharacter();
    }
    public void setPlayer(BattleCharacter C, int x, int y)
    {
        if (y == 0)
        {
            if (x == 0)
                BattleManager.instance.party.TopLeft = C;
            if (x == 1)
                BattleManager.instance.party.TopRight = C;
        }
        if (y == 1)
        {
            if (x == 0)
                BattleManager.instance.party.BottomLeft = C;
            if (x == 1)
                BattleManager.instance.party.BottomRight = C;
        }
    }
    public int getReserveIndex(int x,int y)
    {
        if(x == 2)
            return y;
        return y + 6;
    }

}
