﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    public AudioSource musicSource;
    public AudioSource sfxSource;

    public AudioClip[] backgroundMusics;
    public AudioClip[] soundEffects;

    public AudioClip buttonSelect1;
    public AudioClip buttonSelect2;
	
    public float musicVolume = 0.2f;
    public bool muteMusic = false;
    public float sfxVolume = 1f;
    public bool muteSfx = false;

    public static SoundManager instance = null;

    void Awake() 
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    void Start()
	{
		ChangeMusicVolume(musicVolume);
        ToggleMusic(muteMusic);
        ChangeSfxVolume(sfxVolume);
        ToggleSfx(muteSfx);
	}
    public void PlaySingle(string name)
    {
        if(name.Contains(".wav"))
            name = name.Substring(0, name.Length - 4);
        for (int i = 0; i < soundEffects.Length; ++i)
        {
            if (soundEffects[i].name.Equals(name))
            {
                sfxSource.clip = soundEffects[i];
                sfxSource.Play();
                break;
            }
        }
    }
    public void PlaySingle(AudioClip clip)
    {
        sfxSource.clip = clip;
        sfxSource.Play();
    }
    public void PlayMusic(string songName)
    {
        for (int i = 0; i < backgroundMusics.Length; ++i)
        {
            if (backgroundMusics[i].name.Equals(songName))
            {
                if (musicSource.clip == backgroundMusics[i])
                    break;
                musicSource.Stop();
                musicSource.clip = backgroundMusics[i];
                musicSource.Play();
                break;
            }
        }
    }
	
    public void ChangeMusicVolume(float volume)
    {
        musicSource.volume = musicVolume = volume;
    }

    public void ToggleMusic(bool mute)
    {
        musicSource.mute = muteMusic = mute;
    }

    public void ChangeSfxVolume(float volume)
    {
        sfxSource.volume = sfxVolume = volume;
    }

    public void ToggleSfx(bool mute)
    {
        sfxSource.mute = muteSfx = mute;
    }
}
